<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/includes
 * @author     Screenpartner AS <post@screenpartner.no>
 */
class SP_Bilservice {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      SP_Bilservice_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $sp_bilservice    The string used to uniquely identify this plugin.
	 */
	protected $sp_bilservice;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'SP_BILSERVICE_VERSION' ) ) {
			$this->version = SP_BILSERVICE_VERSION;
		} else {
			$this->version = '1.0.9';
		}
		$this->sp_bilservice = 'sp-bilservice';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - SP_Bilservice_Loader. Orchestrates the hooks of the plugin.
	 * - SP_Bilservice_i18n. Defines internationalization functionality.
	 * - SP_Bilservice_Admin. Defines all hooks for the admin area.
	 * - SP_Bilservice_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		if( ! class_exists('acf') ) {

			/**
			 * Define path and URL to the ACF plugin.
	     * Include the ACF plugin.
	     * Customize the url setting to fix incorrect asset URLs.
	     * (Optional) Hide the ACF admin menu item.
			 */
	    define( 'SP_BILSERVICE_ACF_PATH', plugin_dir_path( dirname( __FILE__ ) ) . '/includes/acf/' );
	    define( 'SP_BILSERVICE_ACF_URL', plugin_dir_url( dirname( __FILE__ ) ) . '/includes/acf/' );

	    include_once( SP_BILSERVICE_ACF_PATH . 'acf.php' );

	    add_filter('acf/settings/url', 'sp_bilservice_settings_url');
	    function sp_bilservice_settings_url( $url ) {
				return SP_BILSERVICE_ACF_URL;
	    }

	    add_filter('acf/settings/show_admin', 'sp_bilservice_settings_show_admin');
	    function sp_bilservice_settings_show_admin( $show_admin ) {
				//return false;
				return true;
	    }

	    add_filter('acf/settings/save_json', 'sp_bilservice_json_save_point');
	    function sp_bilservice_json_save_point( $path ) {
	      $path = plugin_dir_path( dirname( __FILE__ ) ) . '/acf-json';
	      return $path;
	    }

	    add_filter('acf/settings/load_json', 'sp_bilservice_json_load_point');
	    function sp_bilservice_json_load_point( $paths ) {
	      unset($paths[0]);
	      $paths[] = plugin_dir_path( dirname( __FILE__ ) ) . '/acf-json';
	      return $paths;
	    }

		}

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sp-bilservice-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sp-bilservice-i18n.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'libraries/action-scheduler/action-scheduler.php';

		/**
		 * The class responsible for defining the carweb rest notifications
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-rest-notification.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-sp-bilservice-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-sp-bilservice-public.php';

		$this->loader = new SP_Bilservice_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the SP_Bilservice_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new SP_Bilservice_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new SP_Bilservice_Admin( $this->get_sp_bilservice(), $this->get_version() );
		$plugin_rest = new SP_Bilservice_Rest_Notification( $this->get_sp_bilservice(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		// Ajaxify sp_bilservice_fetch_cars_ajax_callback() function
		$this->loader->add_action( 'wp_ajax_sp_bilservice_fetch_cars_ajax_callback', $plugin_admin, 'sp_bilservice_fetch_cars_ajax_callback' );

		// Ajaxify sp_bilservice_fetch_cars_ajax_callback() function
		$this->loader->add_action( 'wp_ajax_sp_bilservice_fetch_single_car_images_ajax_callback', $plugin_admin, 'sp_bilservice_fetch_single_car_images_ajax_callback' );

		// Ajaxify sp_bilservice_fetch_cars_ajax_callback() function
		$this->loader->add_action( 'wp_ajax_sp_bilservice_cancel_fetch_images_ajax_callback', $plugin_admin, 'sp_bilservice_cancel_fetch_images_ajax_callback' );

		// Hooks into acf/init hook to add custom acf settings page
		$this->loader->add_action( 'acf/init', $plugin_admin, 'sp_bilservice_add_acf_settings_page' );

		// Create Custom post types and taxonomies
		$this->loader->add_action( 'init', $plugin_admin, 'sp_bilservice_create_cpt_lead' );
		$this->loader->add_action( 'init', $plugin_admin, 'sp_bilservice_create_cpt_car' );
		$this->loader->add_action( 'init', $plugin_admin, 'sp_bilservice_create_brand_tax' );
		$this->loader->add_action( 'init', $plugin_admin, 'sp_bilservice_create_accessories_tax' );
		$this->loader->add_action( 'init', $plugin_admin, 'sp_bilservice_create_model_tax' );
		$this->loader->add_action( 'init', $plugin_admin, 'sp_bilservice_create_transmission_tax' );
		$this->loader->add_action( 'init', $plugin_admin, 'sp_bilservice_create_fuel_tax' );
		$this->loader->add_action( 'init', $plugin_admin, 'sp_bilservice_create_wheeldrive_tax' );
		$this->loader->add_action( 'init', $plugin_admin, 'sp_bilservice_create_place_tax' );
		$this->loader->add_action( 'init', $plugin_admin, 'sp_bilservice_create_cpt_contact_person' );
		$this->loader->add_action( 'init', $plugin_admin, 'add_image_sizes' );
		//$this->loader->add_action( 'shutdown', $plugin_admin, 'sp_bilservice_fetch_single_car_images_ajax_callback', 10 );
		//$this->loader->add_action( 'download_car_images', $plugin_admin, 'download_car_images_callback', 10, 1 );
		$this->loader->add_action( 'download_car_image', $plugin_admin, 'download_car_image_callback', 10, 4 );
		$this->loader->add_action( 'download_department_image', $plugin_admin, 'download_department_image_callback', 10, 4 );
		$this->loader->add_action( 'delete_car', $plugin_admin, 'delete_car_callback', 10, 3 );
		$this->loader->add_action( 'download_car_condition_evaluation_pdf', $plugin_admin, 'download_car_condition_evaluation_pdf_callback', 10, 3 );

		$this->loader->add_action( 'rest_add_car', $plugin_rest, 'rest_add_car_callback', 10, 2 );
		$this->loader->add_action( 'rest_update_car', $plugin_rest, 'rest_update_car_callback', 10, 1 );

		// Add custom meta boxes before ACF Form
		$this->loader->add_action( 'acf/input/admin_head', $plugin_admin, 'sp_bilservice_mb_before_acf', 1);

		$this->loader->add_action( 'acf/save_post', $plugin_admin, 'sp_bilservice_generate_options_css', 20 );

		// Add Settings link to the plugin
		$plugin_basename = plugin_basename( plugin_dir_path( __DIR__ ) . $this->sp_bilservice . '.php' );

		$this->loader->add_filter( 'plugin_action_links_' . $plugin_basename, $plugin_admin, 'add_action_links' );

		// Hide imported images from WP media gallery
		$this->loader->add_filter( 'ajax_query_attachments_args', $plugin_admin, 'sp_bilservice_hide_car_images_in_media_library_ajax_view' );
		$this->loader->add_action( 'pre_get_posts', $plugin_admin, 'sp_bilservice_hide_car_images_in_media_library_list_view' );

		$this->loader->add_action( 'before_delete_post', $plugin_admin, 'sp_bilservice_delete_all_attached_media' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new SP_Bilservice_Public( $this->get_sp_bilservice(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action( 'rest_api_init', $plugin_public, 'expose_acf_to_rest' );
		$this->loader->add_action( 'pre_get_posts', $plugin_public, 'search_and_filter_cars' );
		$this->loader->add_action( 'template_redirect', $plugin_public, 'override_404_no_results' );

		$plugin_rest_notification = new SP_Bilservice_Rest_Notification( $this->get_sp_bilservice(), $this->get_version() );

		// Ajaxify sp_bilservice_fetch_cars_ajax_callback() function
		$this->loader->add_action( 'wp_ajax_sp_bilservice_load_more_cars', $plugin_public, 'load_more_cars_ajax_callback' );
		$this->loader->add_action( 'wp_ajax_nopriv_sp_bilservice_load_more_cars', $plugin_public, 'load_more_cars_ajax_callback' );

		// Ajaxify sp_bilservice_fetch_cars_ajax_callback() function
		$this->loader->add_action( 'wp_ajax_sp_bilservice_get_monthly_price', $plugin_public, 'get_monthly_price_ajax_callback' );
		$this->loader->add_action( 'wp_ajax_nopriv_sp_bilservice_get_monthly_price', $plugin_public, 'get_monthly_price_ajax_callback' );

		// Ajaxify sp_bilservice_fetch_cars_ajax_callback() function
		$this->loader->add_action( 'wp_ajax_spbcarsfavorites_ajax_callback', $plugin_public, 'spbcarsfavorites_ajax_callback' );
		$this->loader->add_action( 'wp_ajax_nopriv_spbcarsfavorites_ajax_callback', $plugin_public, 'spbcarsfavorites_ajax_callback' );

		// Ajaxify sp_bilservice_fetch_cars_ajax_callback() function
		$this->loader->add_action( 'wp_ajax_spbcarsfilter_ajax_callback', $plugin_public, 'spbcarsfilter_ajax_callback' );
		$this->loader->add_action( 'wp_ajax_nopriv_spbcarsfilter_ajax_callback', $plugin_public, 'spbcarsfilter_ajax_callback' );

		// Ajaxify sp_bilservice_reset_cars_ajax_callback() function
		$this->loader->add_action( 'wp_ajax_sp_bilservice_reset_cars_ajax_callback', $plugin_public, 'sp_bilservice_reset_cars_ajax_callback' );
		$this->loader->add_action( 'wp_ajax_nopriv_sp_bilservice_reset_cars_ajax_callback', $plugin_public, 'sp_bilservice_reset_cars_ajax_callback' );

		// Ajaxify sp_bilservice_fetch_cars_ajax_callback() function
		$this->loader->add_action( 'wp_ajax_send_popup_form_ajax_callback', $plugin_public, 'send_popup_form_ajax_callback' );
		$this->loader->add_action( 'wp_ajax_nopriv_send_popup_form_ajax_callback', $plugin_public, 'send_popup_form_ajax_callback' );

		$this->loader->add_action( 'rest_api_init', $plugin_rest_notification, 'sp_bilservice_register_rest_routes' );
		$this->loader->add_filter( 'template_include', $plugin_public, 'display_correct_templates' );
		$this->loader->add_filter( 'query_vars', $plugin_public, 'add_car_archive_query_arguments' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_sp_bilservice() {
		return $this->sp_bilservice;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    SP_Bilservice_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
