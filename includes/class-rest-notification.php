<?php

/**
 * Register Rest API Notification Routes and
 * custom edpoint action
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/includes
 */

/**
 * Register Rest API Notification Routes and
 * custom edpoint action
 *
 * @since      1.0.0
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/includes
 * @author     Screenpartner AS <post@screenpartner.no>
 */
class SP_Bilservice_Rest_Notification {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $sp_bilservice    The ID of this plugin.
	 */
	private $sp_bilservice;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $sp_bilservice       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $sp_bilservice, $version ) {

		$this->sp_bilservice = $sp_bilservice;
		$this->version = $version;

	}

	public function sp_bilservice_register_rest_routes() {
		register_rest_route( $this->sp_bilservice . '/v1', '/carweb-notification', array(
	    'methods'  => 'POST',
	    'callback' => array( $this, 'sp_bilservice_carweb_notification' ),
			'permission_callback' => '__return_true'
	  ) );
	}

	public static function sp_bilservice_carweb_notification( WP_REST_Request $request ) {
		// Store Data Request object
		$data = $request->get_json_params();

		// Log incoming webhook
		self::sp_bilservice_write_to_log($data);

		// Set type of notification as variable
		$notification = $data['NotificationClass'];

		// Set the remote car id
		$remote_car_id = $data['Identification']['CarId'];

		// If no car id, exit
		if ( ! $remote_car_id ) {
			self::sp_bilservice_write_to_log( 'No Remote Car Id' );
			return;
		}

		// Strip dashes and unwanted chars from remote car id
		// this must be done because ids have different formatting
		// between systems
		$remote_car_id = preg_replace( '/[^a-zA-Z0-9]/', '', $remote_car_id );

		// We need to check if the updatedDate has changed
		// This check needs to happen even if the notification
		// is PublishedCarOnPortal
		if ( $notification == 'Marketing.PublishedCarOnPortal' ) {
			// Get car id from WP system
			$admin = new SP_Bilservice_Admin( 'sp_bilservice', '1.0.9' );
			$car_id = $admin->sp_bilservice_get_internal_car_id( $remote_car_id );

			// If car does not exists in WP
			if ( ! $car_id ) {
				self::sp_bilservice_write_to_log( 'Car does not exist in WP with remote_car_id ' . $remote_car_id );
				return;
			}

			// Update marketed date
			as_schedule_single_action( strtotime("+30 seconds"), 'rest_update_car', array( 'remote_car_id' => $remote_car_id ) );
		}

		// Add approved notification states to this array
		$approved_notification_states = array(
			'Marketing.ChangedMarketingState',
			'Marketing.ChangedAskPrice',
			'Car.Archived'
		);

		// Write to log incoming notification
		self::sp_bilservice_write_to_log( 'Notification class is: ' . $notification );

		// if not in approved notificates array, exit
		if ( ! in_array( $notification, $approved_notification_states ) ) {
			self::sp_bilservice_write_to_log( 'Car not updated. Webhook did not use an approved notification state.' );
			return;
		}

		// NB: This variable is only populated if notication class is:
		// Marketing.ChangedMarketingState
		// So we add it manually to:
		// Marketing.ChangedAskPrice
		if ( $notification == 'Marketing.ChangedAskPrice' ) {
			$marketing_state = 'FollowUp';
		} elseif ( $notification == 'Car.Archived' ) {
			$marketing_state = 'Archive';
		} else {
			$marketing_state = $data['MarketingState'];
		}

		// Add approved notification states to this array
		$approved_marketing_states = array(
			'FollowUp'
		);

		// Actions specific to these marketing states
		// 1. Add or update existing car
		// 2. Download corresponsiding images
		if ( in_array( $marketing_state, $approved_marketing_states ) ) {
			as_schedule_single_action( strtotime("+60 seconds"), 'rest_add_car', array( 'remote_car_id' => $remote_car_id, 'marketing_state' => $marketing_state ) );
		} else {
			// Get car id from WP system
			$admin = new SP_Bilservice_Admin( 'sp_bilservice', '1.0.9' );
			$car_id = $admin->sp_bilservice_get_internal_car_id( $remote_car_id );

			if ($car_id) {
				// Set status of car
				if ($marketing_state == 'Paused') {
					update_field( 'status', 'sold', $car_id );
					$admin->sp_bilservice_schedule_delete_car( $car_id );
					//$admin->sp_bilservice_change_car_status( $remote_car_id, 'draft' );
					self::sp_bilservice_write_to_log( $car_id . ' status set to "sold". Car will be deleted in 2 days.' );
				} elseif ($marketing_state == 'Sold') {
					update_field( 'status', 'sold', $car_id );
					$admin->sp_bilservice_schedule_delete_car( $car_id );
					//$admin->sp_bilservice_change_car_status( $remote_car_id, 'draft' );
					self::sp_bilservice_write_to_log( $car_id . ' status set to "sold". Car will be deleted in 2 days.' );
				} elseif ($marketing_state == 'FollowUp') {
					update_field( 'status', 'for_sale', $car_id );
					//$admin->sp_bilservice_change_car_status( $remote_car_id, 'publish' );
					self::sp_bilservice_write_to_log( $car_id . ' status set to "for sale".' );
				} elseif ($marketing_state == 'Archive') {
					$admin->sp_bilservice_schedule_delete_car( $car_id, '+60 seconds' );
					self::sp_bilservice_write_to_log( $car_id . ' (' . $remote_car_id . ') is archived. Car will be deleted in 30 seconds.' );
				}
			}
		}
	}

	public function rest_add_car_callback( $remote_car_id, $marketing_state ) {
		// Instance Admin Class
		// and fetch car
		$admin = new SP_Bilservice_Admin( 'sp_bilservice', '1.0.9' );
		$car = $admin->sp_bilservice_get_car( $remote_car_id );

		if ( ! is_array($car) ) {
			error_log( print_r('Tried to fetch CAR ID: ' . $remote_car_id . '.  This car does not exist.') );
			self::sp_bilservice_write_to_log( 'Tried to fetch CAR ID: ' . $remote_car_id . '.  This car does not exist.' );
			return;
		}

		// If no car is found, exit
		if ($car[0] == null) {
			self::sp_bilservice_write_to_log( 'Tried to fetch CAR ID: ' . $remote_car_id . '.  Server responded with an empty response.' );
			return;
		}

		$car_id = $admin->sp_bilservice_add_car( $car[0] );
		$admin->download_car_images_callback( $car_id );

		self::sp_bilservice_write_to_log( $remote_car_id . ' added / updated to internal car ID ' . $car_id . '.' );

		// Set status of car
		if ($marketing_state == 'Paused') {
			update_field( 'status', 'sold', $car_id );
			$admin->sp_bilservice_schedule_delete_car( $car_id );
			//$admin->sp_bilservice_change_car_status( $remote_car_id, 'draft' );
			self::sp_bilservice_write_to_log( $car_id . ' status set to "sold". Car will be deleted in 2 days.' );
		} elseif ($marketing_state == 'Sold') {
			update_field( 'status', 'sold', $car_id );
			$admin->sp_bilservice_schedule_delete_car( $car_id );
			//$admin->sp_bilservice_change_car_status( $remote_car_id, 'draft' );
			self::sp_bilservice_write_to_log( $car_id . ' status set to "sold". Car will be deleted in 2 days.' );
		} elseif ($marketing_state == 'FollowUp') {
			update_field( 'status', 'for_sale', $car_id );
			//$admin->sp_bilservice_change_car_status( $remote_car_id, 'publish' );
			self::sp_bilservice_write_to_log( $car_id . ' status set to "for sale".' );
		}
	}

	public function rest_update_car_callback( $remote_car_id ) {
		// Instance Admin Class
		// and fetch car
		$admin = new SP_Bilservice_Admin( 'sp_bilservice', '1.0.9' );
		$car = $admin->sp_bilservice_get_car( $remote_car_id );

		if ( ! is_array($car) ) {
			error_log( print_r('Tried to fetch CAR ID: ' . $remote_car_id . '.  This car does not exist.') );
			self::sp_bilservice_write_to_log( 'Tried to fetch CAR ID: ' . $remote_car_id . '.  This car does not exist.' );
			return;
		}

		// If no car is found, exit
		if ($car[0] == null) {
			self::sp_bilservice_write_to_log( 'Tried to fetch CAR ID: ' . $remote_car_id . '.  Server responded with an empty response.' );
			return;
		}

		$car_id = $admin->sp_bilservice_add_car( $car[0] );
		$admin->download_car_images_callback( $car_id );

		self::sp_bilservice_write_to_log( $remote_car_id . ' updated to internal car ID ' . $car_id . '.' );
	}
	
	public static function sp_bilservice_write_to_log( $log_message ) {
		$log_directory_name = wp_upload_dir()['basedir'] . '/bruktbiler-webhook-logs';
		if ( ! file_exists($log_directory_name) ) {
			// Create folder
			mkdir($log_directory_name, 0777, true);
		}

		$log_file_data = $log_directory_name . '/' . date('Y-d-m') . '.log';
		file_put_contents( $log_file_data, date('Y-d-m h:i:s') . "\n", FILE_APPEND );
		file_put_contents( $log_file_data, print_r($log_message, true) . "\n", FILE_APPEND );
	}

}
