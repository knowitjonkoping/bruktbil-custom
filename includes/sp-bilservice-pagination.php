<?php
function set_posts_per_page_for_bruktbiler_cpt( $query ) {
   if ( $query->is_main_query() && is_post_type_archive( 'car' ) ) {
      $query->set( 'posts_per_page', '19' );
   }
}
add_action( 'pre_get_posts', 'set_posts_per_page_for_bruktbiler_cpt' );


function new_cpt_archive_title($title){
   $bruktbil_archive_title = get_field('bruktbil_archive_title', 'bilservice_options');
   if ( is_post_type_archive('car') ){
       $title = $bruktbil_archive_title . ' - ' . get_bloginfo('name');
       return $title;
   }
   return $title;
} 
add_filter( 'pre_get_document_title', 'new_cpt_archive_title', 9999 );