<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://screenpartner.no
 * @since             1.0.0
 * @package           SP_Bilservice
 *
 * @wordpress-plugin
 * Plugin Name:       Bruktbil
 * Plugin URI:        https://screenpartner.no/
 * Description:       Integrates WP with Carweb/Finn.no.
 * Version:           1.0.9
 * Author:            Screenpartner AS
 * Author URI:        http://screenpartner.no/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       sp-bilservice
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'SP_BILSERVICE_VERSION', '1.0.9' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-sp-bilservice-activator.php
 */
function activate_sp_bilservice() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sp-bilservice-activator.php';
	SP_Bilservice_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-sp-bilservice-deactivator.php
 */
function deactivate_sp_bilservice() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sp-bilservice-deactivator.php';
	SP_Bilservice_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_sp_bilservice' );
register_deactivation_hook( __FILE__, 'deactivate_sp_bilservice' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-sp-bilservice.php';

/** The template tags accessible for public use */
require_once plugin_dir_path( __FILE__ ) . 'public/sp-bilservice-helper-functions.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_sp_bilservice() {

	$plugin = new SP_Bilservice();
	$plugin->run();

}
run_sp_bilservice();

/** Pagination */
require_once plugin_dir_path( __FILE__ ) . 'includes/sp-bilservice-pagination.php';