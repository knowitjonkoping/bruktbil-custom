(function( $ ) {
	'use strict';

	/**
	 * Car Actions
	 * Ajax function
	 */

	$('#fetch-cars, #delete-cars').click(function(){
		var btn = $(this);
		var car_action = $(this).attr('data-car-action');
		var carweb_user = $( 'input#acf-field_carweb_user' );
		var carweb_password = $( 'input#acf-field_carweb_password' );
		var carweb_department_id = $( 'input#acf-field_carweb_department_id' );
		var finn = $( 'input#acf-field_finn_api_key' );
		var strings = ajax_object.localized_strings;

		// Make sure you absolutely want to delete the cars
		if (car_action == 'delete-cars') {
			if ( ! confirm( strings['delete-cars-confirmation'] ) ) {
			  return false;
			}
		}
		// Make sure API values are set for either carweb or finn.no
		else if (car_action == 'fetch-cars') {
			if ( ( carweb_user.val().length <= 0 || carweb_password.val().length <= 0 ) && ( finn.val().length <= 0 ) ) {
				alert( strings['specify-api-info'] );
				return;
			}

			// Make user aware of the importance of department id
			if (carweb_department_id.val().length <= 0) {
				if ( ! confirm( strings['specify-department-id'] ) ) {
				  return false;
				}
			}
		}

		// Do the ajax action
    $.ajax({
      type: 'POST',
			url: ajax_object.ajax_url,
      data: {
        "action" : "sp_bilservice_fetch_cars_ajax_callback",
        "nonce" : ajax_object.nonce_fetch_cars,
				"car_action" : car_action
      },
			beforeSend: function () {
				if (car_action == 'fetch-cars') {
					sp_message('<p>' + strings['fetching-cars'] + '</p>', 'info');
        } else {
					sp_message('<p>' + strings['deleting-cars'] + '</p>', 'info');
				}
			},
      success: function(data) {
				$('.acf-settings-wrap').find('.notice').remove();
				console.log(data);
        if (car_action == 'fetch-cars') {
					sp_message('<p>' + strings['cars-fetched'] + '</p>', 'success');
        } else {
					sp_message('<p>' + strings['cars-deleted'] + '</p>', 'success');
				}
      },
      error: function(data) {
				sp_message('<p>' + strings['error-request'] + ' ' + data.error_message + '</p>', 'error');
      },
    });
  });

	$('#cancel-fetch-images').click(function(){
		var btn = $(this);

		// Do the ajax action
    $.ajax({
      type: 'POST',
			url: ajax_object.ajax_url,
      data: {
        "action" : "sp_bilservice_cancel_fetch_images_ajax_callback",
        "nonce" : ajax_object.nonce_cancel_fetch_images
      },
      success: function(data) {
				console.log(data);
      },
      error: function(data) {
				console.log(data);
      },
    });
  });

	/* $('#fetch-images').click(function(){
		var btn = $(this);
		var strings = ajax_object.localized_strings;
		var ids = [];
		var per_page = 20;
		var number_of_cars = ajax_object.number_of_cars;
		var pages = ~~(number_of_cars / per_page);
		if (pages == 0) {
			pages = 1;
		}

		var promises = [];

		for (var i = 0; i < pages; i++) {
			var jsonUrl = ajax_object.site_url + "/wp-json/wp/v2/car?per_page=" + per_page + "&page=" + (i + 1);
			console.log(jsonUrl);

			promises.push(
				$.getJSON( jsonUrl, function( data ) {
					$.each( data, function( key, val ) {
						ids.push(val.id);
					});
		    })
			);
		}

		// Combine all promises
		// and run a callback
		$.when.apply($, promises).then(function() {
			console.log(ids);
			// Count Ids and
			// create custom progress bar
			var total_cars = ids.length;
			sp_progress_bar_init( total_cars );

			var count_cars = 0;

			$.each( ids, function( index, id ) {
				// Do ajax request for every car
				$.ajax({
					type: 'POST',
					url: ajax_object.ajax_url,
					data: {
						"action" : "sp_bilservice_fetch_single_car_images_ajax_callback",
						"nonce" : ajax_object.nonce_fetch_images,
						"car_id" : id
					},
					success: function(data) {
						count_cars = count_cars + 1;
						sp_progress_bar_update( count_cars );

						if (count_cars + 1 == total_cars) {
							setTimeout(function() {
								sp_progress_bar_remove();
							}, 5000);
						}
					},
					error: function(data) {
						count_cars = count_cars + 1;
						sp_progress_bar_update( count_cars );
						console.log(data);
						sp_message('<p>' + strings['error-request'] + ' ' + data.status + ' - ' + data.statusText + '</p>', 'error');
					}
				});
			});

		});
  }); */

	$('#fetch-images').click(function(){
		var btn = $(this);
		var strings = ajax_object.localized_strings;

		// Do ajax request for every car
		$.ajax({
			type: 'POST',
			url: ajax_object.ajax_url,
			data: {
				"action" : "sp_bilservice_fetch_single_car_images_ajax_callback",
				"nonce" : ajax_object.nonce_fetch_images
			},
			beforeSend: function() {
				sp_message('<p>' + strings['preparing-download'] + '</p>', 'success');
			},
			success: function(data) {
				console.log(data);
				sp_message('<p>' + strings['fetching-images'] + ' ' + data.status + ' - ' + data.statusText + '</p>', 'success');
			},
			error: function(data) {
				sp_message('<p>' + strings['error-request'] + ' ' + data.status + ' - ' + data.statusText + '</p>', 'error');
			}
		});
  });

	function randomNumber(min, max) {
    var number = (Math.floor(Math.random() * (max - min + 1) + min));
    return number;
  }

	function sp_message($msg, $type) {
		var minNumber = 100;
    var maxNumber = 999;
		var unique_class = 'sp-message-' + randomNumber(minNumber, maxNumber);
		var location = $('.acf-settings-wrap form');
		var html = "<div class='notice notice-" + $type + " " + unique_class + "'>" + $msg + "</div>";
		location.prepend(html);
		location.find('.' + unique_class).fadeIn();
		console.log(unique_class);
		console.log(location.find('.' + unique_class));
		setTimeout(function() {
      location.find('.' + unique_class).fadeOut("medium");
    }, 10000);
	}

	function sp_progress_bar_init( total_cars ) {
		var location = $('.acf-settings-wrap form');
		var progress_bar_html = "<progress class='progress-bar' value='0' max='" + total_cars + "'></progress>";
		location.prepend(progress_bar_html);
		$('.progress-bar').show();
	}

	function sp_progress_bar_update( current_car ) {
		var progress_bar = $( '.progress-bar' );
		progress_bar.attr( 'value', current_car );
	}

	function sp_progress_bar_remove() {
		$('.progress-bar').remove();
	}

})( jQuery );
