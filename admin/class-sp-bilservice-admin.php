<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/admin
 * @author     Screenpartner AS <post@screenpartner.no>
 */
class SP_Bilservice_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $sp_bilservice    The ID of this plugin.
	 */
	private $sp_bilservice;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $sp_bilservice_options;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $sp_bilservice       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $sp_bilservice, $version ) {

		$this->sp_bilservice = $sp_bilservice;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in SP_Bilservice_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The SP_Bilservice_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->sp_bilservice, plugin_dir_url( __FILE__ ) . 'css/sp-bilservice-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in SP_Bilservice_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The SP_Bilservice_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->sp_bilservice, plugin_dir_url( __FILE__ ) . 'js/sp-bilservice-admin.js', array( 'jquery' ), $this->version, true );

		wp_enqueue_script( $this->sp_bilservice . '-settings-caractions', plugin_dir_url( __FILE__ ) . 'js/sp-bilservice-settings-caractions.js', array( 'jquery' ), $this->version, true );

		$translation_array = array(
			'delete-cars-confirmation' => __( 'Are you sure you want to delete all cars from the database?', 'sp-bilservice' ),
			'specify-api-info' => __( 'Please specify API info for either Carweb (User & password) or Finn.no (Api Key).', 'sp-bilservice' ),
			'specify-department-id' => __( 'You have not specified a Carweb Department ID, this means you will fetch all cars. Please confirm this action!', 'sp-bilservice' ),
			'fetching-cars' => __( 'Fetching cars... Do not navigate away from this window!', 'sp-bilservice' ),
			'deleting-cars' => __( 'Deleting cars... Do not navigate away from this window!', 'sp-bilservice' ),
			'cars-fetched' => __( 'Cars successfully fetched / updated!', 'sp-bilservice' ),
			'cars-deleted' => __( 'Cars successfully deleted!', 'sp-bilservice' ),
			'preparing-download' => __( 'Please wait, your download is being prepared.', 'sp-bilservice' ),
			'fetching-images' => sprintf( __( 'Images are now fetching in the background! This is a resource intensive process and will take some time to complete. To see the current process, visit this page: %s', 'sp-bilservice' ), '<a href="' . get_site_url() . '/wp-admin/tools.php?page=action-scheduler' . '" target="_blank">' . get_site_url() . '/wp-admin/tools.php?page=action-scheduler' . '</a>' ),
			'error-request' => __( 'There was an error in your request:', 'sp-bilservice' )
		);

		wp_localize_script( $this->sp_bilservice . '-settings-caractions', 'ajax_object', array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'nonce_fetch_cars' => wp_create_nonce( 'sp_bilservice_fetch_cars' ),
			'nonce_fetch_images' => wp_create_nonce( 'sp_bilservice_fetch_images' ),
			'nonce_cancel_fetch_images' => wp_create_nonce( 'sp_bilservice_cancel_fetch_images' ),
			'site_url' => get_site_url(),
			'localized_strings' => $translation_array,
			'number_of_cars' => wp_count_posts( 'car' )->publish
		));

	}

	/**
	 * Add settings action link to the plugins page.
	 *
	 * @since    1.0.0
	 */
	public function add_action_links( $links ) {
    $settings_link = array( '<a href="' . admin_url( 'options-general.php?page=acf-options-bruktbiler' ) . '">' . __( 'Settings', 'sp-bilservice' ) . '</a>', );
    return array_merge(  $settings_link, $links );
	}

	public function add_image_sizes() {
		add_image_size( 'cararchive', 430, 300, true ); // Archive
	}

	/**
	 * Add settings page acf php / html.
	 *
	 * @since    1.0.0
	 */
	public function sp_bilservice_add_acf_settings_page() {
		require_once plugin_dir_path( __FILE__ ). 'partials/sp-bilservice-settings-acf.php';
	}

	/**
	 * Add extra meta box.
	 *
	 * @since    1.0.0
	 */
	public function sp_bilservice_mb_before_acf() {
    $screen = get_current_screen();
    if ($screen->id == 'settings_page_acf-options-bruktbiler') {
      add_meta_box(
				'sp-bilservice-caractions',
				__('Car Actions', 'sp-bilservice'),
				array( $this, 'sp_bilservice_mb_caractions_content' ),
				'acf_options_page',
				'normal',
				'high'
			);
    }
  }

	/**
	 * Add html to the extra meta box.
	 *
	 * @since    1.0.0
	 */
  public function sp_bilservice_mb_caractions_content( $post, $args = array() ) {
    require_once plugin_dir_path( __FILE__ ). 'partials/sp-bilservice-settings-caractions.php';
  }

	private function sp_bilservice_get_authentication_headers() {
		$carweb_user = get_field('carweb_merchant_user', 'bilservice_options');
		$carweb_password = get_field('carweb_merchant_password', 'bilservice_options');

		$authentication = array(
			'headers' => array(
				'Authorization' => 'Basic ' . base64_encode( $carweb_user . ':' . $carweb_password )
			)
		);

		return $authentication;
	}

	/**
	 * Fetch Cars Ajax Function.
	 *
	 * @since    1.0.0
	 */
	public function sp_bilservice_fetch_cars_ajax_callback() {
    // Security
		$nonce = $_POST['nonce'];
		$car_action = $_POST['car_action'];

    if ( ! wp_verify_nonce( $nonce, 'sp_bilservice_fetch_cars' ) ) {
      die( 'Nonce value cannot be verified.' );
    }

		if ($car_action == 'fetch-cars') {

			// Start function
			$pull_cars_from = get_field('pull_cars_from', 'bilservice_options');

			if ( $pull_cars_from == 'carweb' ) {

				$department_id = get_field('carweb_department_id', 'bilservice_options');

				if ($department_id) {
					$numberofcars = json_decode( wp_remote_get( $this->sp_bilservice_get_base_api_url() . '/v3/departments/' . $department_id . '/cars?count=all&format=json', $this->sp_bilservice_get_authentication_headers() )['body'] )[0]->Number;
				} else {
					$numberofcars = json_decode( wp_remote_get( $this->sp_bilservice_get_base_api_url() . '/v3/cars?count=all&format=json', $this->sp_bilservice_get_authentication_headers() )['body'] )[0]->Number;
				}

				if ( ! is_numeric( $numberofcars ) ) {
					error_log( print_r( 'Did not get a numeric result from $numberofcars variable', true ) );
					error_log( print_r( 'Response: ' . $numberofcars, true ) );
					wp_die();
				}

				$per_page = 25;
				$pages = ceil( $numberofcars / $per_page );

				// Loop through all pages
				foreach ( range(1, $pages) as $page ) {
					$this->sp_bilservice_fetch_car_page( $page, $per_page );
				}

			} elseif ( $pull_cars_from == 'finn' ) {

				// Finn implementation
				$finn_api = get_field('finn_api_key', 'bilservice_options');

			}

		} elseif ( $car_action == 'delete-cars' ) {

			$all_cars = get_posts( array('post_type' => 'car', 'numberposts' => -1 ) );
			foreach ( $all_cars as $car ) {
				$attachments = get_attached_media( '', $car->ID );
				foreach ( $attachments as $attachment ) {
					wp_delete_attachment( $attachment->ID, 'true' );
				}
				wp_delete_post( $car->ID, true );
				rmdir(trailingslashit( wp_upload_dir()['basedir'] . '/cars/' . $car->ID ), true);
			}

		}

    wp_die();
	}

	public function sp_bilservice_fetch_car_page( $page, $per_page = 25 ) {
		// Carweb implementation
		$department_id = get_field('carweb_department_id', 'bilservice_options');

		$offset = ($page - 1) * $per_page;

		// Get cars from department
		if ( $department_id ) {
			$request = wp_remote_get( $this->sp_bilservice_get_base_api_url() . '/v3/departments/' . $department_id . '/cars?format=json&language=no&limit=' . $per_page . '&offset=' . $offset, $this->sp_bilservice_get_authentication_headers() );
		} else {
			$request = wp_remote_get( $this->sp_bilservice_get_base_api_url() . '/v3/cars?format=json&language=no&limit=' . $per_page . '&offset=' . $offset, $this->sp_bilservice_get_authentication_headers() );
		}

		if ( is_wp_error( $request ) ) {
			return false; // Bail
		}

		$body = wp_remote_retrieve_body( $request );
		$data = json_decode( $body );

		if ( ! empty( $data ) ) {
			// For every car
			foreach ($data as $car) {
				// Car fields
				$remote_car_id = $car->CarId;
				$department_id = $car->DepartmentId;
				$is_leasing_car = $car->IsMarketedAsLeasingCar;

				// Ignore leasing cars
				if (!$is_leasing_car) {
					$car_id = $this->sp_bilservice_add_car( $car );
				}
			}
		}
	}

	public function sp_bilservice_get_car( $remote_car_id ) {
		$request = wp_remote_get( $this->sp_bilservice_get_base_api_url() . '/v3/cars/ ' . $remote_car_id . '?format=json&language=no', $this->sp_bilservice_get_authentication_headers() );

		if ( is_wp_error( $request ) ) {
			return false; // Bail
		}

		$body = wp_remote_retrieve_body( $request );
		$data = json_decode( $body );

		return $data;
	}

	public function sp_bilservice_get_internal_car_id( $remote_car_id ) {
		$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
		$query_args = array(
		'paged' => $paged,
		'post_type' => 'car',
		'posts_per_page' => 1,
		'meta_key' => 'remote_car_id',
		'meta_value' => $remote_car_id
		);

		$car_query = new WP_Query($query_args);

		if (!$car_query->have_posts()) {
			return;
		}

		return $car_query->posts[0]->ID;
	}

	/**
	 * Fetch Single Car Images Ajax Function.
	 *
	 * @since    1.0.0
	 */
	/* public function sp_bilservice_fetch_single_car_images_ajax_callback() {
		// Security
		$nonce = $_POST['nonce'];
		$car_id = $_POST['car_id'];

    if ( ! wp_verify_nonce( $nonce, 'sp_bilservice_fetch_images' ) ) {
      die( 'Nonce value cannot be verified.' );
    }

		// Carweb implementation
		$remote_car_id = get_field('remote_car_id', $car_id);

		// Get cars from department
		$request = wp_remote_get( $this->sp_bilservice_get_base_api_url() . '/v2/cars/' . $remote_car_id . '/images?format=json', $this->sp_bilservice_get_authentication_headers() );

		if ( is_wp_error( $request ) ) {
			return false; // Bail
		}

		$body = wp_remote_retrieve_body( $request );
		$data = json_decode( $body );

		if ( ! empty( $data ) ) {

			$attachment_ids = array();

			// For every image
			foreach ($data as $image) {

				$remote_image_id = $image->Id;
				$checksum = $image->CheckSum;

				$attachment_ids[] = $this->sp_bilservice_get_attachment_id( $remote_image_id, $checksum, $car_id );

			}

			// Update images field with array of attachment ids
			update_field( 'images', $attachment_ids, $car_id );

		}

		wp_die();
	} */

	/**
	 * Fetch Single Car Images Ajax Function.
	 *
	 * @since    1.0.0
	 */
	public function sp_bilservice_fetch_single_car_images_ajax_callback() {
		// Security
		if ( ! wp_verify_nonce( $_POST['nonce'], 'sp_bilservice_fetch_images' ) ) {
      wp_die( 'Nonce value cannot be verified.' );
    }
	 	$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
		$all_cars = get_posts( array( 'fields' => 'ids', 'post_type' => 'car', 'numberposts' => -1, 'paged' => $paged, 'offset' => $paged ? ($paged - 1 ) * $number : 0 ) );

		if ( empty( $all_cars ) ) {
			echo json_encode( array( 'status' => 'error' ) );
			return false;
		}

		$message = array();

		foreach ( $all_cars as $car_id ) {
			//as_enqueue_async_action( 'download_car_images', array( 'car_id' => $car_id ) );
			//as_schedule_single_action( time(), 'download_car_images', array( 'car_id' => $car_id ) );
			$this->download_car_images_callback( $car_id );
		}

		echo json_encode( array( 'status' => 'success', 'cars' => $all_cars ) );

		wp_die();
	}


	/**
	 * Cancel Fetch Images Ajax Function.
	 *
	 * @since    1.0.0
	 */
	public function sp_bilservice_cancel_fetch_images_ajax_callback() {
		// Security
		if ( ! wp_verify_nonce( $_POST['nonce'], 'sp_bilservice_cancel_fetch_images' ) ) {
      wp_die( 'Nonce value cannot be verified.' );
    }

		as_unschedule_all_actions('download_car_image');
		as_unschedule_all_actions('download_car_images');

		wp_die();
	}

	public function download_car_images_callback( $car_id ) {
		if ( ! $car_id ) {
			return;
		}

		// Carweb implementation
		$remote_car_id = get_field('remote_car_id', $car_id);

		// Get cars from department
		$request = wp_remote_get( $this->sp_bilservice_get_base_api_url() . '/v2/cars/' . $remote_car_id . '/images?format=json', $this->sp_bilservice_get_authentication_headers() );

		if ( is_wp_error( $request ) ) {
			return false; // Bail
		}

		$body = wp_remote_retrieve_body( $request );
		$data = json_decode( $body );
		$car_images_count = 0;

		if ( ! empty( $data ) ) {
			$attachment_ids = array();

			// For every image
			foreach ($data as $image) {
				$remote_image_id = $image->Id;
				$checksum = $image->CheckSum;
				$sort_order = $image->SortOrder;
				$car_images_count++;

				as_enqueue_async_action( 'download_car_image', array( 'remote_image_id' => $remote_image_id, 'checksum' => $checksum, 'car_id' => $car_id, 'sort_order' => $sort_order ) );
			}
		}

		// Also download department images
		$department_id = get_field('department_id', $car_id);

		if ( $department_id ) {
			// Get cars from department
			$dep_request = wp_remote_get( $this->sp_bilservice_get_base_api_url() . '/departments/' . $department_id . '/fixedimages?format=json', $this->sp_bilservice_get_authentication_headers() );

			if ( is_wp_error( $dep_request ) ) {
				return false; // Bail
			}

			$dep_body = wp_remote_retrieve_body( $dep_request );
			$dep_data = json_decode( $dep_body );

			if ( ! empty( $dep_data ) ) {
				$attachment_ids = array();

				$car_images_count = $car_images_count + 50;

				// For every image
				foreach ($dep_data as $image) {
					$remote_image_id = $image->Id;
					$car_images_count++;

					as_enqueue_async_action( 'download_department_image', array( 'remote_image_id' => $remote_image_id, 'car_id' => $car_id, 'sort_order' => $car_images_count, 'department_id' => $department_id ) );
				}
			}
		}
	}

	public function download_car_image_callback( $remote_image_id, $checksum, $car_id, $sort_order ) {
		$images_field = get_field( 'images', $car_id, false );
		$attachment_id = $this->sp_bilservice_get_attachment_id( $remote_image_id, $checksum, $car_id, $sort_order );

		if ($attachment_id) {
			if ( ! is_array( $images_field ) ) {
			  $images_field = array();
			}

			// Adding attachment id to array
			$images_field[] = $attachment_id;

			// Update sort order field for specific attachment
			update_field( 'carweb_sort_order', $sort_order, $attachment_id );

			// Update images gallery field to include new attachment
			update_field( 'images', $images_field, $car_id );
		}
	}

	public function download_department_image_callback( $remote_image_id, $car_id, $sort_order, $department_id ) {
		$images_field = get_field( 'images', $car_id, false );
		$attachment_id = $this->sp_bilservice_get_department_attachment_id( $remote_image_id, $car_id, $department_id, $sort_order );

		if ($attachment_id) {
			if ( ! is_array( $images_field ) ) {
			  $images_field = array();
			}

			// Adding attachment id to array
			$images_field[] = $attachment_id;

			// Update sort order field for specific attachment
			update_field( 'carweb_sort_order', $sort_order, $attachment_id );

			// Update images gallery field to include new attachment
			update_field( 'images', $images_field, $car_id );
		}
	}

	public function delete_car_callback( $car_id ) {
		if ( ! $car_id ) {
			return;
		}

		$status = get_field( 'status', $car_id );
		if ( $status != 'sold' ) {
			return;
		}

		// Deleting car
		$attachments = get_attached_media( '', $car_id );
		foreach ( $attachments as $attachment ) {
			wp_delete_attachment( $attachment->ID, 'true' );
		}
		wp_delete_post( $car_id );
		rmdir(trailingslashit( wp_upload_dir()['basedir'] . '/cars/' . $car_id ), true);

	}

	/**
	 * Fetch Single Car Images Ajax Function.
	 *
	 * @since    1.0.0
	 */
	public function sp_bilservice_fetch_single_car_images( $car_id ) {
		// Carweb implementation
		$remote_car_id = get_field('remote_car_id', $car_id);

		// Get cars from department
		$request = wp_remote_get( $this->sp_bilservice_get_base_api_url() . '/v2/cars/' . $remote_car_id . '/images?format=json', $this->sp_bilservice_get_authentication_headers() );

		if ( is_wp_error( $request ) ) {
			return false; // Bail
		}

		$body = wp_remote_retrieve_body( $request );
		$data = json_decode( $body );

		if ( ! empty( $data ) ) {

			$attachment_ids = array();

			// For every image
			foreach ($data as $image) {

				$remote_image_id = $image->Id;
				$checksum = $image->CheckSum;
				$sort_order = $image->SortOrder;

				$attachment_ids[] = $this->sp_bilservice_get_attachment_id( $remote_image_id, $checksum, $car_id, $sort_order );

			}

			// Update images field with array of attachment ids
			update_field( 'images', $attachment_ids, $car_id );

		}
	}

	public function sp_bilservice_get_attachment_id( $remote_image_id, $checksum, $car_id, $sort_order ) {
		if ( $this->get_attachment_by_checksum( $checksum ) ) {
			// add existing attachment to array
			return $this->get_attachment_by_checksum( $checksum );
		} elseif ( $this->get_attachment_by_remote_image_id( $remote_image_id ) ) {
			// Check if checksum is same
			$attachment_checksum = get_field('carweb_checksum', $this->get_attachment_by_remote_image_id( $remote_image_id ));
			if ( $attachment_checksum != $checksum ) {
				// Delete existing attachment
				if ($this->sp_bilservice_delete_car_attachment( $remote_image_id )) {
					// create attachment and add to array
					return $this->sp_bilservice_create_car_attachment( $remote_image_id, $car_id, $checksum, $sort_order );
				}
			} else {
				// add existing attachment to array
				return $this->get_attachment_by_remote_image_id( $remote_image_id );
			}
		} else {
			// create attachment and add to array
			return $this->sp_bilservice_create_car_attachment( $remote_image_id, $car_id, $checksum, $sort_order );
		}
	}

	public function sp_bilservice_delete_car_attachment( $remote_image_id ) {
		$old_attachment_id = $this->get_attachment_by_remote_image_id( $remote_image_id );

		$try_deleting = wp_delete_attachment( $old_attachment_id, true );
		if ($try_deleting == false || $try_deleting == null) {
			return false;
		}

		return true;
	}

	public function sp_bilservice_create_car_attachment( $remote_image_id, $car_id, $checksum, $sort_order ) {
		$request = wp_remote_get( $this->sp_bilservice_get_base_api_url() . '/v1/images/' . $remote_image_id . '?imagetype=normal', $this->sp_bilservice_get_authentication_headers() );

		if ( is_wp_error( $request ) ) {
			return false; // Bail
		}

		$image = wp_remote_retrieve_body( $request ); // The image
		$upload_dir = wp_upload_dir();

		// Set correct file extension
		$ext = '.jpg';
		$type = wp_remote_retrieve_header( $request, 'content-type' );
		if ($type == 'image/jpeg') {
			$ext = '.jpg';
		} elseif ($type == 'image/png') {
			$ext = '.png';
		} elseif ($type == 'image/svg+xml') {
			$ext = '.svg';
		}

		if ( ! is_dir( trailingslashit( $upload_dir['basedir'] . '/cars/' . $car_id ) ) ) {
		  // dir doesn't exist, make it
		  wp_mkdir_p( trailingslashit( $upload_dir['basedir'] . '/cars/' . $car_id ) );
		}

		$file_path = trailingslashit( $upload_dir['basedir'] . '/cars/' . $car_id ) . '/' . $remote_image_id . $ext;

		file_put_contents( $file_path, $image );

		$att_info = array(
			'guid'           => $upload_dir['url'] . '/' . $remote_image_id,
			'post_mime_type' => $type,
			'post_title'     => $remote_image_id,
			'post_content'   => '',
			'post_status'    => 'inherit',
		);

		// Create the attachment
		$attach_id = wp_insert_attachment( $att_info, $file_path, $car_id );

		// Include image.php
		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		// Define attachment metadata
		$attach_data = wp_generate_attachment_metadata( $attach_id, $file_path );

		// Assign metadata to attachment
		wp_update_attachment_metadata( $attach_id,  $attach_data );

		update_field( 'carweb_image_id', $remote_image_id, $attach_id );
		update_field( 'carweb_checksum', $checksum, $attach_id );
		update_field( 'carweb_sort_order', $sort_order, $attach_id );

		return $attach_id;
	}

	public function sp_bilservice_create_department_attachment( $remote_image_id, $car_id, $sort_order, $department_id ) {
		$request = wp_remote_get( $this->sp_bilservice_get_base_api_url() . '/departments/' . $department_id . '/fixedimages/' . $remote_image_id, $this->sp_bilservice_get_authentication_headers() );

		if ( is_wp_error( $request ) ) {
			return false; // Bail
		}

		$image = wp_remote_retrieve_body( $request ); // The image
		$upload_dir = wp_upload_dir();

		// Set correct file extension
		$ext = '.jpg';
		$type = wp_remote_retrieve_header( $request, 'content-type' );
		if ($type == 'image/jpeg') {
			$ext = '.jpg';
		} elseif ($type == 'image/png') {
			$ext = '.png';
		} elseif ($type == 'image/svg+xml') {
			$ext = '.svg';
		}

		if ( ! is_dir( trailingslashit( $upload_dir['basedir'] . '/departments/' . $department_id ) ) ) {
		  // dir doesn't exist, make it
		  wp_mkdir_p( trailingslashit( $upload_dir['basedir'] . '/departments/' . $department_id ) );
		}

		$file_path = trailingslashit( $upload_dir['basedir'] . '/departments/' . $department_id ) . '/' . $remote_image_id . $ext;

		file_put_contents( $file_path, $image );

		$att_info = array(
			'guid'           => $upload_dir['url'] . '/' . $department_id . '/' . $remote_image_id,
			'post_mime_type' => $type,
			'post_title'     => $remote_image_id,
			'post_content'   => '',
			'post_status'    => 'inherit',
		);

		// Create the attachment
		$attach_id = wp_insert_attachment( $att_info, $file_path, $car_id );

		// Include image.php
		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		// Define attachment metadata
		$attach_data = wp_generate_attachment_metadata( $attach_id, $file_path );

		// Assign metadata to attachment
		wp_update_attachment_metadata( $attach_id,  $attach_data );

		update_field( 'carweb_image_id', $remote_image_id, $attach_id );
		update_field( 'carweb_sort_order', $sort_order, $attach_id );

		return $attach_id;
	}

	public function sp_bilservice_get_department_attachment_id( $remote_image_id, $car_id, $department_id, $sort_order ) {
		if ( $this->get_attachment_by_remote_image_id( $remote_image_id ) ) {
			return $this->get_attachment_by_remote_image_id( $remote_image_id );
		} else {
			// create attachment and add to array
			return $this->sp_bilservice_create_department_attachment( $remote_image_id, $car_id, $sort_order, $department_id );
		}
	}

	public function sp_bilservice_delete_car( $remote_car_id ) {
		// Delete car if it already exists
		$get_car_args = array(
			'post_type' => 'car',
			'offset' => $paged ? ($paged - 1 ) * $number : 0,
			'meta_key' => 'remote_car_id',
			'meta_value' => $remote_car_id
		);
		$get_car = new WP_Query( $get_car_args );

		// Check that we have query results.
		if ( $get_car->have_posts() ) {
			// Start looping over the query results.
			while ( $get_car->have_posts() ) {
				$get_car->the_post();
				global $post;

				// Deleting car
				$attachments = get_attached_media( '', $post->ID );
				foreach ( $attachments as $attachment ) {
					wp_delete_attachment( $attachment->ID, 'true' );
				}
				$deleted_car = wp_delete_post( $post->ID, true );
				rmdir(trailingslashit( wp_upload_dir()['basedir'] . '/cars/' . $post->ID ), true);
				return $deleted_car;

			}
		} else {
			return;
		}
	}

	public function sp_bilservice_change_car_status( $remote_car_id, $status = 'draft' ) {
		// Delete car if it already exists
		$get_car_args = array(
			'post_type' => 'car',
			'offset' => $paged ? ($paged - 1 ) * $number : 0,
			'meta_key' => 'remote_car_id',
			'meta_value' => $remote_car_id
		);
		$get_car = new WP_Query( $get_car_args );

		// Check that we have query results.
		if ( $get_car->have_posts() ) {
			// Start looping over the query results.
			while ( $get_car->have_posts() ) {
				$get_car->the_post();
				global $post;

				// Changing car status
				$args = array(
					'ID' => $post->ID,
					'post_status' => $status
				);
				$updated_car = wp_update_post( $args );

			}
		} else {
			return;
		}
	}

	public function sp_bilservice_schedule_delete_car( $car_id, $addstrtotime = '+3 Days' ) {
		as_schedule_single_action( strtotime($addstrtotime), 'delete_car', array( 'car_id' => $car_id ) );
	}

	public function sp_bilservice_add_car( $car ) {
		/* Remote fields */
		$remote_car_id = $car->CarId;
		$has_condition_report = $car->WarrantyHasConditionReport;

		/* Text Fields */
		$headline = $car->Headline;
		$description = $car->Description;

		// Department
		$department_id = $car->DepartmentId; // Unused

		/* Taxonomy fields */
		$brand = $car->Brand;
		$model = $car->Model;
		$transmission = $car->Transmission;
		$fuel = $car->Fuel;
		if ($fuel == 'Bensin/Elektrisitet' || $fuel == 'Diesel/Batteri') {
			$fuel = 'Hybrid';
		}
		$accessories = $car->Accessories;
		$drive_wheel = $car->DriveWheel;

		// Generate appropriate title
		if ( ! empty( $headline ) ) {
			$car_title = $headline;
		} else {
			$car_title = $brand . ' ' . $model;
		}

		$existing_posts = get_posts(array(
			'numberposts'	=> 1,
			'post_type'		=> 'car',
			'meta_key'		=> 'remote_car_id',
			'meta_compare' => '=',
			'meta_value'	=> $remote_car_id
		));

		/* echo json_encode(get_field('remote_car_id', $existing_posts[0]->ID));
		echo json_encode($remote_car_id);
		echo json_encode('newline');
		return; */

		if ( ! empty( $existing_posts ) && ! empty($existing_posts[0]) ) {
			// Update car post
			$existing_car_id = $existing_posts[0]->ID;
			$car_id = wp_update_post( array(
				'ID'							=> $existing_car_id,
			  'post_title' 			=> $car_title,
				'post_content'		=> $description,
			  'post_type'				=> 'car',
				'post_status' 		=> 'publish'
			) );
		} else {
			// Insert car post
			$car_id = wp_insert_post( array(
			  'post_title' 			=> $car_title,
				'post_content'		=> $description,
			  'post_type'				=> 'car',
				'post_status' 		=> 'publish'
			) );
		}

		if ($car_id) {
			/* Update Sales Person Info */
			$this->sp_bilservice_set_car_info( $car_id, $car );

			/* Update Department Info */
			$this->sp_bilservice_set_department_info( $car_id, $department_id );

			/* Update Sales Person Info */
			$this->sp_bilservice_set_sales_person_info( $car_id, $car );

			/* Update Sales Person Info */
			$this->sp_bilservice_upload_condition_evaluation_pdf( $car_id, $has_condition_report );

			/* Create / Add to taxonomies */
			$this->set_post_term( $car_id, $brand, 'brand' );
			$this->set_post_term( $car_id, $model, 'model' );
			$this->set_post_term( $car_id, $transmission, 'transmission' );
			$this->set_post_term( $car_id, $fuel, 'fuel' );
			$this->set_post_term( $car_id, $drive_wheel, 'wheeldrive' );
			/* $this->set_post_term( $car_id, $place, 'place' ); */
			$this->set_post_terms( $car_id, $accessories, 'accessories' );
		}

		return $car_id;
	}

	public function sp_bilservice_set_car_info( $car_id, $car ) {
		/* Number fields */
		$remote_car_id = $car->CarId;
		$registration_number = $car->RegistrationNumber;
		$chassis_number = $car->ChassisNumber;
		$year = $car->ModelYear;
		$mileage = $car->Mileage;
		$price = $car->Price;
		$registration_fee = $car->RegistrationFee;
		$towing_weight = $car->TowingWeight;
		$cargo_volume = $car->CargoVolume;
		$engine_power = $car->EnginePowerHp;
		$doors = $car->Doors;
		$engine_volume = $car->EngineVolumeLiter;
		$nominal_interest = $car->NominalInterest;
		$effective_interest = $car->EffectiveInterest;
		$warranty_string = $car->CarWarranty;
		$service_plan_followed = $car->IsServicePlanFollowed;
		if ( $service_plan_followed == true ) {
			$service_plan_followed_numeric = 1;
		} else {
			$service_plan_followed_numeric = 0;
		}

		/* Text fields */
		$variant = $car->Variant;
		$base_color = $car->BaseColorCode;
		$exterior_color = $car->Color;
		$interior_color = $car->Interior;

		/* Text / Option fields */
		$car_body = $car->CarBody;

		/* Booleans */
		$is_sold = $car->IsSold;
		$is_exempt_from_registration_fee = $car->IsCarExemptFromRegistrationFee;

		/* Date fields */
		$registration_date = $car->FirstTimeRegistered;
		$new_reg_date = str_replace( '/Date(', '', $registration_date );
		$new_reg_date = strstr( $new_reg_date, '-', true ) / 1000;
		$datetime_reg_date = date('Y-m-d H:i:s', $new_reg_date);

		$marketed_date = $car->MarketedDate;
		$marketed_date = $car->LastPublicationDate;
		$new_marketed_date = str_replace( '/Date(', '', $marketed_date );
		$new_marketed_date = strstr( $new_marketed_date, '-', true ) / 1000;
		$datetime_marketed_date = date('Y-m-d H:i:s', $new_marketed_date);

		// Instance Admin Class
		// and fetch car
		$rest = new SP_Bilservice_Rest_Notification( 'sp_bilservice', '1.0.9' );
		$rest->sp_bilservice_write_to_log( 'Car marketed_date: ' . $marketed_date . '. Converted to datetime: ' . $datetime_marketed_date );

		/* Update ACF Fields */
		update_field( 'remote_car_id', $remote_car_id, $car_id );
		update_field( 'variant', $variant, $car_id );
		update_field( 'price', $price, $car_id );
		update_field( 'registration_fee', $registration_fee, $car_id );
		update_field( 'year', $year, $car_id );
		update_field( 'registration_number', $registration_number, $car_id );
		update_field( 'chassis_number', $chassis_number, $car_id );
		update_field( 'registration_date', $datetime_reg_date, $car_id );
		update_field( 'marketed_date', $datetime_marketed_date, $car_id );
		update_field( 'car_body', $car_body, $car_id );
		update_field( 'engine_power', $engine_power, $car_id );
		update_field( 'engine_volume', $engine_volume, $car_id );
		update_field( 'doors', $doors, $car_id );
		update_field( 'cargo_volume', $cargo_volume, $car_id );
		update_field( 'towing_weight', $towing_weight, $car_id );
		update_field( 'exterior_color', $exterior_color, $car_id );
		update_field( 'interior_color', $interior_color, $car_id );
		update_field( 'mileage', $mileage, $car_id );
		update_field( 'nominal_interest', $nominal_interest, $car_id );
		update_field( 'effective_interest', $effective_interest, $car_id );
		update_field( 'warranty_string', $warranty_string, $car_id );
		update_field( 'service_plan_followed', $service_plan_followed_numeric, $car_id );

		if ( $is_sold == false ) {
			update_field( 'status', 'not_sold', $car_id );
		} elseif ( $is_sold == true ) {
			update_field( 'status', 'sold', $car_id );
		}
		
		if ( $is_exempt_from_registration_fee == false ) {
			update_field( 'exempt_from_registration_fee', 0, $car_id );
		} elseif ( $is_exempt_from_registration_fee == true ) {
			update_field( 'exempt_from_registration_fee', 1, $car_id );
		}
	}

	public function sp_bilservice_set_sales_person_info( $car_id, $car ) {
		// Get fields
		$seller_name = $car->SellerName;
		$seller_email = $car->ResponsibleSalesManEMail;
		$seller_phone = $car->ResponsibleSalesManPhone;
		$seller_mobile = $car->ResponsibleSalesManMobilePhone;

		$call_this_number = $seller_phone;
		if ( ! empty( $seller_mobile ) ) {
			$call_this_number = $seller_mobile;
		}

		// Update fields
		update_field( 'sales_person_name', $seller_name, $car_id );
		update_field( 'sales_person_phone', $call_this_number, $car_id );
		update_field( 'sales_person_email', $seller_email, $car_id );
	}

	public function sp_bilservice_set_department_info( $car_id, $department_id ) {
		$request = wp_remote_get( $this->sp_bilservice_get_base_api_url() . '/v2/departments?format=json&language=no', $this->sp_bilservice_get_authentication_headers() );

		if ( is_wp_error( $request ) ) {
			return false; // Bail
		}

		$body = wp_remote_retrieve_body( $request );
		$data = json_decode( $body );

		if ( ! empty( $data ) ) {

			$department_item = null;
			foreach ($data as $obj) {
				if ($department_id == $obj->DepartmentId) {
					$department_item = $obj;
					break;
				}
			}

			if ( $department_item == null ) {
				return;
			}

			/* Go ahead and populate Department Info */
			// Get fields
			$department_id = $department_item->DepartmentId;
			$department_name = $department_item->DepartmentName;
			$department_address = $department_item->Address1;
			$department_zip_code = $department_item->ZipCode;
			$department_city = $department_item->City;
			$department_phone = $department_item->City;
			$department_email = $department_item->Email;
			$department_logo_url = $department_item->LogoUrl;

			// Update fields
			update_field( 'department_id', $department_id, $car_id );
			update_field( 'department_name', $department_name, $car_id );
			update_field( 'department_address', $department_address, $car_id );
			update_field( 'department_zip_code', $department_zip_code, $car_id );
			update_field( 'department_city', $department_city, $car_id );
			update_field( 'department_phone', $department_phone, $car_id );
			update_field( 'department_email', $department_email, $car_id );
			update_field( 'department_logo_url', $department_logo_url, $car_id );

			$this->set_post_term( $car_id, $department_city, 'place' );
		}
	}

	public function sp_bilservice_upload_condition_evaluation_pdf( $car_id, $has_condition_report ) {
		if ( ! $car_id ) {
			return;
		}

		if ( ! $has_condition_report ) {
			return;
		}

		// If it already exists, exit
		$existing_condition_report = get_field('condition_report', $car_id);
		if ($existing_condition_report) {
			return;
		}

		// Remote Car ID
		$remote_car_id = get_field('remote_car_id', $car_id);
		as_enqueue_async_action( 'download_car_condition_evaluation_pdf', array( 'remote_car_id' => $remote_car_id, 'car_id' => $car_id ) );
	}

	public function download_car_condition_evaluation_pdf_callback( $remote_car_id, $car_id ) {
		// Get cars from department
		$request = wp_remote_get( $this->sp_bilservice_get_base_api_url() . '/cars/' . $remote_car_id . '/conditionevaluation', $this->sp_bilservice_get_authentication_headers() );

		if ( is_wp_error( $request ) ) {
			return false; // Bail
		}

		$image = wp_remote_retrieve_body( $request ); // The image
		$upload_dir = wp_upload_dir();

		// Set correct file extension
		$ext = '.pdf';
		$type = wp_remote_retrieve_header( $request, 'content-type' );

		if ( ! is_dir( trailingslashit( $upload_dir['basedir'] . '/cars/' . $car_id ) ) ) {
		  // dir doesn't exist, make it
		  wp_mkdir_p( trailingslashit( $upload_dir['basedir'] . '/cars/' . $car_id ) );
		}

		$file_path = trailingslashit( $upload_dir['basedir'] . '/cars/' . $car_id ) . 'condition-evaluation' . $ext;

		file_put_contents( $file_path, $image );

		$att_info = array(
			'guid'           => $upload_dir['url'] . '/condition-evaluation',
			'post_mime_type' => $type,
			'post_title'     => $car_id . ' - Condition Evaluation',
			'post_content'   => '',
			'post_status'    => 'inherit',
		);

		// Create the attachment
		$attach_id = wp_insert_attachment( $att_info, $file_path, $car_id );

		// Include image.php
		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		// Define attachment metadata
		$attach_data = wp_generate_attachment_metadata( $attach_id, $file_path );

		// Assign metadata to attachment
		wp_update_attachment_metadata( $attach_id,  $attach_data );

		// Set Condition Report as acf attachment
		update_field( 'condition_report', $attach_id, $car_id );
		update_field( 'carweb_image_id', 'conditionreport', $attach_id );
	}

	private function get_attachment_by_checksum( $checksum ) {
		if ( $checksum == null || empty($checksum) ) {
			return false;
		}

		$args = array(
			'post_type' => 'attachment',
			'post_status' => 'inherit',
			'posts_per_page' => 1,
			'meta_key' => 'carweb_checksum',
			'meta_value' => $checksum
		);

		$att_query = new WP_Query( $args );
		if ( ! $att_query->have_posts() ) {
			return false;
		}

		return $att_query->posts[0]->ID;
	}

	private function get_attachment_by_remote_image_id( $remote_image_id ) {
		if ( $remote_image_id == null || empty($remote_image_id) ) {
			return false;
		}

		$args = array(
			'post_type' => 'attachment',
			'post_status' => 'inherit',
			'posts_per_page' => 1,
			'meta_key' => 'carweb_image_id',
			'meta_value' => $remote_image_id
		);

		$att_query = new WP_Query( $args );
		if ( ! $att_query->have_posts() ) {
			return false;
		}

		return $att_query->posts[0]->ID;
	}

	public function sp_bilservice_get_base_api_url() {
		$test_mode_type = get_field( 'test_mode', 'bilservice_options' ) ?: 'test';
		if ( $test_mode_type == 'test' ) {
			return 'https://test-api.services.carweb.no';
		} elseif ( $test_mode_type == 'prod' ) {
			return 'https://prod-api.services.carweb.no';
		}
	}

	/**
	 * Appends the specified taxonomy term to the incoming post object. If
	 * the term doesn't already exist in the database, it will be created.
	 *
	 * @param    WP_Post    $post_id     The post_id to which we're adding the taxonomy term.
	 * @param    string     $value       The name of the taxonomy term
	 * @param    string     $taxonomy    The name of the taxonomy.
	 * @access   private
	 * @since    1.0.0
	 */
	private function set_post_term( $post_id, $value, $taxonomy ) {
		$term = term_exists( $value, $taxonomy );

		// If the taxonomy doesn't exist, then we create it
		if ( 0 === $term || null === $term ) {

			$term = wp_insert_term(
				$value,
				$taxonomy,
				array(
					'slug' => strtolower( str_ireplace( ' ', '-', $value ) )
				)
			);

		}

		// Then we can set the taxonomy
		wp_set_post_terms( $post_id, $term, $taxonomy, true );

	}

	/**
	 * Determines if the given value has multiple terms by checking to see
	 * if a semicolon exists in the value.
	 *
	 * @param    string   $value    The value to evaluate for multiple terms.
	 * @return   bool               True if there are multiple terms; otherwise, false.
	 */
	private function has_multiple_terms( $value ) {
		return 0 < strpos( $value, ';' );
	}

	/**
	 * Loops through each of the multiple terms that exist and use the
	 * set_profile_terms function to apply each value to the given post
	 *
	 * @param    WP_Post    $post        The post which we're applying the terms.
	 * @param    string     $values      The delimited list of terms.
	 * @param    string     $taxonomy    The taxonomy to which the terms belong.
	 */
	private function set_multiple_terms( $post_id, $values, $taxonomy ) {

		$terms = explode( ';', $values );
		foreach( $terms as $term ) {
			$this->set_post_terms( $post_id, $term, $taxonomy );
		}

	}

	/**
	 * Appends the specified taxonomy term to the incoming post object. If
	 * the term doesn't already exist in the database, it will be created. This
	 * function now supports multiple terms.
	 *
	 * @param    WP_Post    $post_id     The current post_id with which we're working
	 * @param    string     $value       The term value (or term name)
	 * @param    string     $taxonomy    The name of the taxonomy to which the term belongs.
	 * @access   private
	 * @since    1.0.0
	 */
	private function set_post_terms( $post_id, $value, $taxonomy ) {

		/* First check to see if there are multiple terms and,
		 * if so, then loop through the values and update each
		 * term.
		 */
		if ( $this->has_multiple_terms( $value ) ) {
			$this->set_multiple_terms( $post_id, $value, $taxonomy );
		} else {

			$term = term_exists( strtolower( $value ), $taxonomy );

			// If the taxonomy doesn't exist, then we create it
			if ( 0 === $term || null === $term ) {

				$term = wp_insert_term(
					$value,
					$taxonomy,
					array(
						'slug' => strtolower( str_ireplace( ' ', '-', $value ) )
					)
				);

			}
		}

		// Then we can set the taxonomy
		wp_set_post_terms( $post_id, $value, $taxonomy, true );

	}

	public function sp_bilservice_generate_options_css() {
		$palette_dir = plugin_dir_path( dirname( __FILE__ ) );
	  ob_start(); // Capture all output into buffer
	  require($palette_dir . 'admin/partials/sp-bilservice-color-palette.php'); // Grab the custom-style.php file
	  $css = ob_get_clean(); // Store output in a variable, then flush the buffer
	  file_put_contents($palette_dir . 'admin/css/sp-bilservice-color-palette.css', $css, LOCK_EX); // Save it as a css file
	}

	public function sp_bilservice_hide_car_images_in_media_library_list_view( $wp_query ) {
		global $pagenow;

		if ( ! in_array( $pagenow, array( 'upload.php' ) ) ) {
			return;
		}

		$wp_query->set('meta_key', 'carweb_image_id');
		$wp_query->set('meta_compare', 'NOT EXISTS');
	}

	public function sp_bilservice_hide_car_images_in_media_library_ajax_view( $args ) {
		// Bail if this is not the admin area.
		if ( ! is_admin() ) {
			return $args;
		}

		// Modify the query.
		$args['meta_key'] = 'carweb_image_id';
		$args['meta_compare'] = 'NOT EXISTS';

		return $args;
	}

	public function sp_bilservice_delete_all_attached_media( $car_id ) {
		if ( get_post_type( $car_id ) == "car" ) {
			$attachments = get_attached_media( '', $car_id );

			foreach ( $attachments as $attachment ) {
				wp_delete_attachment( $attachment->ID, 'true' );
			}
			rmdir(trailingslashit( wp_upload_dir()['basedir'] . '/cars/' . $car_id ), true);
		}
	}

	// Register Custom Post Type Car
	public function sp_bilservice_create_cpt_lead() {

		$labels = array(
			'name' => _x( 'Leads', 'Post Type General Name', 'sp-bilservice' ),
			'singular_name' => _x( 'Lead', 'Post Type Singular Name', 'sp-bilservice' ),
			'menu_name' => _x( 'Leads', 'Admin Menu text', 'sp-bilservice' ),
			'name_admin_bar' => _x( 'Lead', 'Add New on Toolbar', 'sp-bilservice' ),
			'archives' => __( 'Lead Archives', 'sp-bilservice' ),
			'attributes' => __( 'Lead Attributes', 'sp-bilservice' ),
			'parent_item_colon' => __( 'Parent Lead:', 'sp-bilservice' ),
			'all_items' => __( 'All Leads', 'sp-bilservice' ),
			'add_new_item' => __( 'Add New Lead', 'sp-bilservice' ),
			'add_new' => __( 'Add New', 'sp-bilservice' ),
			'new_item' => __( 'New Lead', 'sp-bilservice' ),
			'edit_item' => __( 'Edit Lead', 'sp-bilservice' ),
			'update_item' => __( 'Update Lead', 'sp-bilservice' ),
			'view_item' => __( 'View Lead', 'sp-bilservice' ),
			'view_items' => __( 'View Leads', 'sp-bilservice' ),
			'search_items' => __( 'Search Lead', 'sp-bilservice' ),
			'not_found' => __( 'Not found', 'sp-bilservice' ),
			'not_found_in_trash' => __( 'Not found in Trash', 'sp-bilservice' ),
			'featured_image' => __( 'Featured Image', 'sp-bilservice' ),
			'set_featured_image' => __( 'Set featured image', 'sp-bilservice' ),
			'remove_featured_image' => __( 'Remove featured image', 'sp-bilservice' ),
			'use_featured_image' => __( 'Use as featured image', 'sp-bilservice' ),
			'insert_into_item' => __( 'Insert into Lead', 'sp-bilservice' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Lead', 'sp-bilservice' ),
			'items_list' => __( 'Leads list', 'sp-bilservice' ),
			'items_list_navigation' => __( 'Leads list navigation', 'sp-bilservice' ),
			'filter_items_list' => __( 'Filter Leads list', 'sp-bilservice' ),
		);
		$args = array(
			'label' => __( 'Lead', 'sp-bilservice' ),
			'description' => __( 'Leads', 'sp-bilservice' ),
			'labels' => $labels,
			'menu_icon' => 'dashicons-format-chat',
			'supports' => array('title'),
			'taxonomies' => array(),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 25,
			'show_in_admin_bar' => true,
			'show_in_nav_menus' => true,
			'can_export' => true,
			'has_archive' => false,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'show_in_rest' => false,
			'publicly_queryable' => true,
			'capability_type' => 'post',
		);
		register_post_type( 'lead', $args );

	}

	// Register Custom Post Type Car
	public function sp_bilservice_create_cpt_car() {

		$labels = array(
			'name' => _x( 'Cars', 'Post Type General Name', 'sp-bilservice' ),
			'singular_name' => _x( 'Car', 'Post Type Singular Name', 'sp-bilservice' ),
			'menu_name' => _x( 'Cars', 'Admin Menu text', 'sp-bilservice' ),
			'name_admin_bar' => _x( 'Car', 'Add New on Toolbar', 'sp-bilservice' ),
			'archives' => __( 'Car Archives', 'sp-bilservice' ),
			'attributes' => __( 'Car Attributes', 'sp-bilservice' ),
			'parent_item_colon' => __( 'Parent Car:', 'sp-bilservice' ),
			'all_items' => __( 'All Cars', 'sp-bilservice' ),
			'add_new_item' => __( 'Add New Car', 'sp-bilservice' ),
			'add_new' => __( 'Add New', 'sp-bilservice' ),
			'new_item' => __( 'New Car', 'sp-bilservice' ),
			'edit_item' => __( 'Edit Car', 'sp-bilservice' ),
			'update_item' => __( 'Update Car', 'sp-bilservice' ),
			'view_item' => __( 'View Car', 'sp-bilservice' ),
			'view_items' => __( 'View Cars', 'sp-bilservice' ),
			'search_items' => __( 'Search Car', 'sp-bilservice' ),
			'not_found' => __( 'Not found', 'sp-bilservice' ),
			'not_found_in_trash' => __( 'Not found in Trash', 'sp-bilservice' ),
			'featured_image' => __( 'Featured Image', 'sp-bilservice' ),
			'set_featured_image' => __( 'Set featured image', 'sp-bilservice' ),
			'remove_featured_image' => __( 'Remove featured image', 'sp-bilservice' ),
			'use_featured_image' => __( 'Use as featured image', 'sp-bilservice' ),
			'insert_into_item' => __( 'Insert into Car', 'sp-bilservice' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Car', 'sp-bilservice' ),
			'items_list' => __( 'Cars list', 'sp-bilservice' ),
			'items_list_navigation' => __( 'Cars list navigation', 'sp-bilservice' ),
			'filter_items_list' => __( 'Filter Cars list', 'sp-bilservice' ),
		);
		$args = array(
			'label' => __( 'Car', 'sp-bilservice' ),
			'description' => __( 'Cars', 'sp-bilservice' ),
			'labels' => $labels,
			'menu_icon' => 'dashicons-car',
			'supports' => array('title', 'editor', 'revisions', 'author'),
			'taxonomies' => array('brand', 'model', 'fuel', 'place', 'wheeldrive', 'transmission', 'accessories'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 25,
			'show_in_admin_bar' => true,
			'show_in_nav_menus' => true,
			'rewrite' => array(
				'slug' => 'bruktbiler'
			),
			'can_export' => true,
			'has_archive' => true,
			'hierarchical' => false,
			'exclude_from_search' => false,
			'show_in_rest' => true,
			'publicly_queryable' => true,
			'capability_type' => 'post',
		);
		register_post_type( 'car', $args );

	}

	// Register Taxonomy Car Brand
	public function sp_bilservice_create_brand_tax() {

		$labels = array(
			'name'              => _x( 'Brands', 'taxonomy general name', 'sp-bilservice' ),
			'singular_name'     => _x( 'Brand', 'taxonomy singular name', 'sp-bilservice' ),
			'search_items'      => __( 'Search Brands', 'sp-bilservice' ),
			'all_items'         => __( 'All Brands', 'sp-bilservice' ),
			'parent_item'       => __( 'Parent Brand', 'sp-bilservice' ),
			'parent_item_colon' => __( 'Parent Brand:', 'sp-bilservice' ),
			'edit_item'         => __( 'Edit Brand', 'sp-bilservice' ),
			'update_item'       => __( 'Update Brand', 'sp-bilservice' ),
			'add_new_item'      => __( 'Add New Brand', 'sp-bilservice' ),
			'new_item_name'     => __( 'New Brand Name', 'sp-bilservice' ),
			'menu_name'         => __( 'Brand', 'sp-bilservice' ),
		);
		$args = array(
			'labels' => $labels,
			'description' => __( 'Brand of the car', 'sp-bilservice' ),
			'hierarchical' => true,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'show_in_quick_edit' => true,
			'show_admin_column' => true,
			'show_in_rest' => true,
		);
		register_taxonomy( 'brand', array('car'), $args );

	}

	// Register Taxonomy Car Brand
	public function sp_bilservice_create_model_tax() {

		$labels = array(
			'name'              => _x( 'Model', 'taxonomy general name', 'sp-bilservice' ),
			'singular_name'     => _x( 'Model', 'taxonomy singular name', 'sp-bilservice' ),
			'search_items'      => __( 'Search Model', 'sp-bilservice' ),
			'all_items'         => __( 'All Model', 'sp-bilservice' ),
			'parent_item'       => __( 'Parent Model', 'sp-bilservice' ),
			'parent_item_colon' => __( 'Parent Model:', 'sp-bilservice' ),
			'edit_item'         => __( 'Edit Model', 'sp-bilservice' ),
			'update_item'       => __( 'Update Model', 'sp-bilservice' ),
			'add_new_item'      => __( 'Add New Model', 'sp-bilservice' ),
			'new_item_name'     => __( 'New Model Name', 'sp-bilservice' ),
			'menu_name'         => __( 'Model', 'sp-bilservice' ),
		);
		$args = array(
			'labels' => $labels,
			'description' => __( 'Model of the car', 'sp-bilservice' ),
			'hierarchical' => true,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'show_in_quick_edit' => true,
			'show_admin_column' => true,
			'show_in_rest' => true,
		);
		register_taxonomy( 'model', array('car'), $args );

	}

	// Register Taxonomy Car Brand
	public function sp_bilservice_create_transmission_tax() {

		$labels = array(
			'name'              => _x( 'Transmission', 'taxonomy general name', 'sp-bilservice' ),
			'singular_name'     => _x( 'Transmission', 'taxonomy singular name', 'sp-bilservice' ),
			'search_items'      => __( 'Search Transmission', 'sp-bilservice' ),
			'all_items'         => __( 'All Transmissions', 'sp-bilservice' ),
			'parent_item'       => __( 'Parent Transmission', 'sp-bilservice' ),
			'parent_item_colon' => __( 'Parent Transmission:', 'sp-bilservice' ),
			'edit_item'         => __( 'Edit Transmission', 'sp-bilservice' ),
			'update_item'       => __( 'Update Transmission', 'sp-bilservice' ),
			'add_new_item'      => __( 'Add New Transmission', 'sp-bilservice' ),
			'new_item_name'     => __( 'New Transmission Name', 'sp-bilservice' ),
			'menu_name'         => __( 'Transmission', 'sp-bilservice' ),
		);
		$args = array(
			'labels' => $labels,
			'description' => __( 'Transmission of the car', 'sp-bilservice' ),
			'hierarchical' => true,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'show_in_quick_edit' => true,
			'show_admin_column' => false,
			'show_in_rest' => true,
		);
		register_taxonomy( 'transmission', array('car'), $args );

	}

	// Register Taxonomy Car Fuel
	public function sp_bilservice_create_fuel_tax() {

		$labels = array(
			'name'              => _x( 'Fuel', 'taxonomy general name', 'sp-bilservice' ),
			'singular_name'     => _x( 'Fuel', 'taxonomy singular name', 'sp-bilservice' ),
			'search_items'      => __( 'Search Fuel', 'sp-bilservice' ),
			'all_items'         => __( 'All Fuel', 'sp-bilservice' ),
			'parent_item'       => __( 'Parent Fuel', 'sp-bilservice' ),
			'parent_item_colon' => __( 'Parent Fuel:', 'sp-bilservice' ),
			'edit_item'         => __( 'Edit Fuel', 'sp-bilservice' ),
			'update_item'       => __( 'Update Fuel', 'sp-bilservice' ),
			'add_new_item'      => __( 'Add New Fuel', 'sp-bilservice' ),
			'new_item_name'     => __( 'New Fuel Name', 'sp-bilservice' ),
			'menu_name'         => __( 'Fuel', 'sp-bilservice' ),
		);
		$args = array(
			'labels' => $labels,
			'description' => __( 'Fuel of the car', 'sp-bilservice' ),
			'hierarchical' => true,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'show_in_quick_edit' => true,
			'show_admin_column' => false,
			'show_in_rest' => true,
		);
		register_taxonomy( 'fuel', array('car'), $args );

	}

	// Register Taxonomy Wheel Drive
	public function sp_bilservice_create_wheeldrive_tax() {

		$labels = array(
			'name'              => _x( 'Wheel Drive', 'taxonomy general name', 'sp-bilservice' ),
			'singular_name'     => _x( 'Wheel Drive', 'taxonomy singular name', 'sp-bilservice' ),
			'search_items'      => __( 'Search Wheel Drive', 'sp-bilservice' ),
			'all_items'         => __( 'All Wheel Drive', 'sp-bilservice' ),
			'parent_item'       => __( 'Parent Wheel Drive', 'sp-bilservice' ),
			'parent_item_colon' => __( 'Parent Wheel Drive:', 'sp-bilservice' ),
			'edit_item'         => __( 'Edit Wheel Drive', 'sp-bilservice' ),
			'update_item'       => __( 'Update Wheel Drive', 'sp-bilservice' ),
			'add_new_item'      => __( 'Add New Wheel Drive', 'sp-bilservice' ),
			'new_item_name'     => __( 'New Wheel Drive Name', 'sp-bilservice' ),
			'menu_name'         => __( 'Wheel Drive', 'sp-bilservice' ),
		);
		$args = array(
			'labels' => $labels,
			'description' => __( 'Wheel Drive of the car', 'sp-bilservice' ),
			'hierarchical' => true,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'show_in_quick_edit' => true,
			'show_admin_column' => false,
			'show_in_rest' => true,
		);
		register_taxonomy( 'wheeldrive', array('car'), $args );

	}

	// Register Taxonomy Wheel Drive
	public function sp_bilservice_create_place_tax() {

		$labels = array(
			'name'              => _x( 'Places', 'taxonomy general name', 'sp-bilservice' ),
			'singular_name'     => _x( 'Place', 'taxonomy singular name', 'sp-bilservice' ),
			'search_items'      => __( 'Search Places', 'sp-bilservice' ),
			'all_items'         => __( 'All Places', 'sp-bilservice' ),
			'parent_item'       => __( 'Parent Place', 'sp-bilservice' ),
			'parent_item_colon' => __( 'Parent Place:', 'sp-bilservice' ),
			'edit_item'         => __( 'Edit Place', 'sp-bilservice' ),
			'update_item'       => __( 'Update Place', 'sp-bilservice' ),
			'add_new_item'      => __( 'Add New Place', 'sp-bilservice' ),
			'new_item_name'     => __( 'New Place Name', 'sp-bilservice' ),
			'menu_name'         => __( 'Places', 'sp-bilservice' ),
		);
		$args = array(
			'labels' => $labels,
			'description' => __( 'Car Dealership Places', 'sp-bilservice' ),
			'hierarchical' => true,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'show_in_quick_edit' => true,
			'show_admin_column' => true,
			'show_in_rest' => true,
		);
		register_taxonomy( 'place', array('car'), $args );

	}

	// Register Taxonomy Car Brand
	public function sp_bilservice_create_accessories_tax() {

		$labels = array(
			'name'              => _x( 'Accessories', 'taxonomy general name', 'sp-bilservice' ),
			'singular_name'     => _x( 'Accessory', 'taxonomy singular name', 'sp-bilservice' ),
			'search_items'      => __( 'Search Accessories', 'sp-bilservice' ),
			'all_items'         => __( 'All Accessories', 'sp-bilservice' ),
			'parent_item'       => __( 'Parent Accessory', 'sp-bilservice' ),
			'parent_item_colon' => __( 'Parent Accessory:', 'sp-bilservice' ),
			'edit_item'         => __( 'Edit Accessory', 'sp-bilservice' ),
			'update_item'       => __( 'Update Accessory', 'sp-bilservice' ),
			'add_new_item'      => __( 'Add New Accessory', 'sp-bilservice' ),
			'new_item_name'     => __( 'New Accessory Name', 'sp-bilservice' ),
			'menu_name'         => __( 'Accessories', 'sp-bilservice' ),
		);
		$args = array(
			'labels' => $labels,
			'description' => __( 'Car accessories', 'sp-bilservice' ),
			'hierarchical' => false,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'show_in_quick_edit' => true,
			'show_admin_column' => false,
			'show_in_rest' => true,
		);
		register_taxonomy( 'accessories', array('car'), $args );

	}


	// Register Custom Post Type Car
	public function sp_bilservice_create_cpt_contact_person() {

		$labels = array(
			'name' => _x( 'Contact Persons', 'Post Type General Name', 'sp-bilservice' ),
			'singular_name' => _x( 'Contact Person', 'Post Type Singular Name', 'sp-bilservice' ),
			'menu_name' => _x( 'Contact Persons', 'Admin Menu text', 'sp-bilservice' ),
			'name_admin_bar' => _x( 'Contact Persons', 'Add New on Toolbar', 'sp-bilservice' ),
			'archives' => __( 'Contact Persons Archives', 'sp-bilservice' ),
			'attributes' => __( 'Contact Persons Attributes', 'sp-bilservice' ),
			'parent_item_colon' => __( 'Parent Contact Person:', 'sp-bilservice' ),
			'all_items' => __( 'All Contact Persons', 'sp-bilservice' ),
			'add_new_item' => __( 'Add New Contact Person', 'sp-bilservice' ),
			'add_new' => __( 'Add New', 'sp-bilservice' ),
			'new_item' => __( 'New Contact Person', 'sp-bilservice' ),
			'edit_item' => __( 'Edit Contact Person', 'sp-bilservice' ),
			'update_item' => __( 'Update Contact Person', 'sp-bilservice' ),
			'view_item' => __( 'View Contact Person', 'sp-bilservice' ),
			'view_items' => __( 'View Contact Persons', 'sp-bilservice' ),
			'search_items' => __( 'Search Contact Person', 'sp-bilservice' ),
			'not_found' => __( 'Not found', 'sp-bilservice' ),
			'not_found_in_trash' => __( 'Not found in Trash', 'sp-bilservice' ),
			'featured_image' => __( 'Profile Image', 'sp-bilservice' ),
			'set_featured_image' => __( 'Set profile image', 'sp-bilservice' ),
			'remove_featured_image' => __( 'Remove profile image', 'sp-bilservice' ),
			'use_featured_image' => __( 'Use as profile image', 'sp-bilservice' ),
			'insert_into_item' => __( 'Insert into Contact Person', 'sp-bilservice' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Contact Person', 'sp-bilservice' ),
			'items_list' => __( 'Contact Persons list', 'sp-bilservice' ),
			'items_list_navigation' => __( 'Contact Persons list navigation', 'sp-bilservice' ),
			'filter_items_list' => __( 'Filter Contact Persons list', 'sp-bilservice' ),
		);
		$args = array(
			'label' => __( 'Contact Person', 'sp-bilservice' ),
			'description' => __( 'Contact Persons', 'sp-bilservice' ),
			'labels' => $labels,
			'menu_icon' => 'dashicons-businessperson',
			'supports' => array('title', 'revisions', 'author', 'thumbnail'),
			'taxonomies' => array(),
			'public' => true,
			'show_ui' => true,
			/* 'show_in_menu' => 'edit.php?post_type=car', */
			'show_in_menu' => true,
			'menu_position' => 28,
			'show_in_admin_bar' => true,
			'show_in_nav_menus' => true,
			'can_export' => true,
			'has_archive' => false,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'show_in_rest' => true,
			'publicly_queryable' => true,
			'capability_type' => 'post',
		);
		register_post_type( 'contactperson', $args );

	}
}
