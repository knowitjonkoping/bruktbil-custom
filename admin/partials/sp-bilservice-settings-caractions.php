<?php

/**
 * Reset Cars Meta Box
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/admin/partials
 */
?>

<p><?php _e('Please use the buttons below to fetch or delete cars.', 'sp-bilservice'); ?></p>

<p>
  <button type="button" class="button-primary" id="fetch-cars" data-car-action="fetch-cars">
    <span><?php echo __('Fetch Cars', 'sp-bilservice'); ?></span>
  </button>

  <button type="button" class="button-secondary" id="fetch-images" data-car-action="fetch-images">
    <span><?php echo __('Download Car Images', 'sp-bilservice'); ?></span>
  </button>

  <button type="button" class="button-secondary button-link-delete" id="cancel-fetch-images" data-car-action="cancel-fetch-images">
    <span><?php echo __('Cancel Download of Car Images', 'sp-bilservice'); ?></span>
  </button>

  <button type="button" class="button-secondary button-link-delete" id="delete-cars" data-car-action="delete-cars">
    <span><?php echo __('Delete Cars', 'sp-bilservice'); ?></span>
  </button>
</p>
