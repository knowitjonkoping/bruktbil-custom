<?php
/**
* Template tag for displaying the filters form
* @return html object
*/

function sp_bilservice_format_mileage( $mileage ) {
  return SP_Bilservice_Public::format_mileage( $mileage );
}

function sp_bilservice_format_price( $price ) {
  return SP_Bilservice_Public::format_price( $price );
}

function sp_bilservice_get_main_image( $mileage ) {
  return SP_Bilservice_Public::get_main_image( $mileage );
}

function sp_bilservice_get_monthly_price( $price, $cash, $payback_time ) {
  return SP_Bilservice_Public::get_monthly_price( $price, $cash, $payback_time );
}

function sp_bilservice_get_payment_share_percent( $price, $cash ) {
  return SP_Bilservice_Public::get_payment_share_percent( $price, $cash );
}

function sp_bilservice_get_interest_rate( $percent ) {
  return SP_Bilservice_Public::get_interest_rate( $percent );
}

function sp_bilservice_get_fallback_interest_rate( $percent ) {
  return SP_Bilservice_Public::get_fallback_interest_rate( $percent );
}

function sp_bilservice_get_loan_url( $price, $cash, $payback_time, $registration_number, $brand, $model, $year, $branch_id ) {
  return SP_Bilservice_Public::get_loan_url( $price, $cash, $payback_time, $registration_number, $brand, $model, $year, $branch_id );
}

function sp_bilservice_download_car_images( $car_id, $nonce ) {
  return SP_Bilservice_Admin::download_car_images( $car_id, $nonce );
}

function sp_bilservice_sort_images_by_carweb_sort_order( $images ) {
  return SP_Bilservice_Public::sort_images_by_carweb_sort_order( $images );
}
