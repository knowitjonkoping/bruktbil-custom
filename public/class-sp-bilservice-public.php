<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public
 * @author     Screenpartner AS <post@screenpartner.no>
 */
class SP_Bilservice_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $sp_bilservice    The ID of this plugin.
	 */
	private $sp_bilservice;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $sp_bilservice       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $sp_bilservice, $version ) {

		$this->sp_bilservice = $sp_bilservice;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in SP_Bilservice_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The SP_Bilservice_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->sp_bilservice . '-jquery-ui-css', plugin_dir_url( __FILE__ ) . 'plugins/jquery-ui/jquery-ui.min.css', array(), $this->version, 'all' );

		if ( is_singular( 'car' ) ) {
			// wp_enqueue_style( $this->sp_bilservice . '-flickity', plugin_dir_url( __FILE__ ) . 'plugins/flickity/flickity.min.css', array(), $this->version, 'all' );
			// wp_enqueue_style( $this->sp_bilservice . '-flickity-fullscreen', plugin_dir_url( __FILE__ ) . 'plugins/flickity/flickity-fullscreen.css', array(), $this->version, 'all' );

		}
		if( is_post_type_archive( 'car' ) || is_singular( 'car' ) ) {
			wp_enqueue_style( $this->sp_bilservice, plugin_dir_url( __FILE__ ) . 'css/sp-bilservice-public.css', array(), $this->version, 'all' );
		}

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in SP_Bilservice_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The SP_Bilservice_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		global $wp_query;

		wp_enqueue_script( $this->sp_bilservice . '-jquery-ui', plugin_dir_url( __FILE__ ) . 'plugins/jquery-ui/jquery-ui.min.js', array( 'jquery' ), '', false );
		wp_enqueue_script( $this->sp_bilservice . '-jquery-ui-touch-punch', plugin_dir_url( __FILE__ ) . 'plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js', array( 'jquery' ), '', false );

		wp_enqueue_script( $this->sp_bilservice, plugin_dir_url( __FILE__ ) . 'js/sp-bilservice-public.js', array( 'jquery' ), $this->version, false );

		if ( is_post_type_archive( 'car' ) ) {
			wp_enqueue_script( $this->sp_bilservice . '-car-archives', plugin_dir_url( __FILE__ ) . 'js/sp-bilservice-public-car-archives.js', array( 'jquery' ), $this->version, true );
		}

		if ( is_singular( 'car' ) ) {
			// wp_enqueue_script( $this->sp_bilservice . '-flickity', plugin_dir_url( __FILE__ ) . 'plugins/flickity/flickity.min.js', array( 'jquery' ), $this->version, true );
			// wp_enqueue_script( $this->sp_bilservice . '-flickity-fullscreen', plugin_dir_url( __FILE__ ) . 'plugins/flickity/flickity-fullscreen.js', array( 'jquery' ), $this->version, true );
			wp_enqueue_script( $this->sp_bilservice . '-car-single', plugin_dir_url( __FILE__ ) . 'js/sp-bilservice-public-car-single.js', array( 'jquery' ), $this->version, true );
		}

		$translation_array = array(
			'years' => __( 'years', 'sp-bilservice' ),
			'loading' => __( 'Loading', 'sp-bilservice' ),
			'load_more' => __('Load more', 'sp-bilservice' ),
			'form_success' => __('Thank you for your interest! We will contact you shortly to schedule your meeting!', 'sp-bilservice' ),
			'form_error' => __('Something went wrong with this request, please try again!', 'sp-bilservice' )
		);

		// Calculate monthly prices & percentages
		$cash_post = isset( $_POST['cash_amount'] ) ? $_POST['cash_amount'] : 0;
		$cash_get = get_query_var('cash_amount');
		$cash_cookie = isset( $_COOKIE['spb_cash_amount'] ) ? $_COOKIE['spb_cash_amount'] : 5000;
		$payback_time_post = isset( $_POST['payback_time'] ) ? $_POST['payback_time'] : 0;
		$payback_time_get = get_query_var('payback_time');
		$payback_time_cookie = isset( $_COOKIE['spb_payback_time'] ) ? $_COOKIE['spb_payback_time'] : 7;
		if ($cash_post) {
		  $cash = $cash_post;
		} elseif ($cash_get) {
		  $cash = $cash_get;
		} else {
		  $cash = $cash_cookie;
		}
		if ($payback_time_post) {
		  $payback_time = $payback_time_post;
		} elseif ($payback_time_get) {
		  $payback_time = $payback_time_get;
		} else {
		  $payback_time = $payback_time_cookie;
		}

		wp_localize_script( $this->sp_bilservice . '-car-archives', 'car_archives_ajax_object', array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'localized_strings' => $translation_array,
			'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
			'current_page' => $wp_query->query_vars['paged'] ? $wp_query->query_vars['paged'] : 1,
			'max_page' => $wp_query->max_num_pages,
			'first_page' => get_pagenum_link(1),
			'highest_price' => get_field('highest_price', 'bilservice_options'),
			'highest_mileage' => get_field('highest_mileage', 'bilservice_options'),
			'lowest_year' => get_field('lowest_year', 'bilservice_options'),
			'current_year' => date('Y'),
			'payback_time' => $payback_time,
			'cash_amount' => $cash,
			'total_cars' => $wp_query->found_posts
		));

		$place = get_the_terms( get_the_ID(), 'place' );
		$branch_id = false;
		if ($place) {
			$branch_id = get_field('branch_id', $place[0]);
		}

		wp_localize_script( $this->sp_bilservice . '-car-single', 'car_single_ajax_object', array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'localized_strings' => $translation_array,
			'car_id' => get_the_ID(),
			'payback_time' => $payback_time,
			'cash_amount' => $cash,
			'price' => get_field('price', get_the_ID()),
			'branch_id' => $branch_id
		));
	}

	public function override_404_no_results() {
		global $wp_query, $post;
    if ( $wp_query->post_count == 0 && $wp_query->query_vars['post_type'] == 'car' ) {
      $wp_query->is_404 = false;
      $wp_query->is_archive = true;
      $wp_query->is_post_type_archive = true;
      $post = new stdClass();
      $post->post_type = $wp_query->query['post_type'];

			// Make sure HTTP status is good
			status_header( '200' );
    }
	}

	public function expose_acf_to_rest() {
		$postypes_to_exclude = ['acf-field-group','acf-field'];
    $extra_postypes_to_include = ["car"];
    $post_types = array_diff(get_post_types(["_builtin" => false], 'names'),$postypes_to_exclude);

    array_push($post_types, $extra_postypes_to_include);

    foreach ($post_types as $post_type) {
      register_rest_field( $post_type, 'sp_bilservice_rest_fields', [
        'get_callback'    => array($this, 'expose_ACF_fields'),
        'schema'          => null,
			]
     );
    }
	}

	/* Helper function */
	public function expose_ACF_fields( $object ) {
		$id = $object['id'];
		return get_fields( $id );
	}

	public function display_correct_templates( $template ) {
		global $post;

		if($template == get_template_directory()."/taxonomy-bilmerke-taxonomy.php"){
			return $template;
		}

		if ( is_singular( 'car' ) ) {
			$template = plugin_dir_path( __FILE__ ). 'templates/single-car.php';
		} elseif ( is_archive( 'car' ) ) {
			$template = plugin_dir_path( __FILE__ ). 'templates/archive-car.php';
		}

		return $template;
	}

	public function add_car_archive_query_arguments( $qvars ) {
		// Calculator vars
		$qvars[] = 'payback_time';
		$qvars[] = 'cash_amount';

		// Cars vars
		$qvars[] = 'brand';
		$qvars[] = 'model';
		$qvars[] = 'fuel';
		$qvars[] = 'place';
		$qvars[] = 'transmission';
		$qvars[] = 'wheeldrive';
		$qvars[] = 'min_price';
		$qvars[] = 'max_price';
		$qvars[] = 'min_mileage';
		$qvars[] = 'max_mileage';
		$qvars[] = 'min_year';
		$qvars[] = 'max_year';
		$qvars[] = 'favorites';
		$qvars[] = 'sortby';

		return $qvars;
	}

	public function search_and_filter_cars( $query ) {
		if (is_admin()) {
			return;
		}

    if ( $query->is_main_query() && is_archive( 'car' ) ) {
			$ppp = get_option( 'posts_per_page' ) - 1;
			// Minus 1 posts per page
			$query->set( 'posts_per_page', $ppp );

			// Filters
			$s = get_query_var('s');
			$brand = explode( ',', get_query_var('brand') );
			$model = explode( ',', get_query_var('model') );
			$fuel = explode( ',', get_query_var('fuel') );
			$transmission = explode( ',', get_query_var('transmission') );
			$place = explode( ',', get_query_var('place') );
			$wheeldrive = explode( ',', get_query_var('wheeldrive') );
			$min_price = str_replace( ' ', '', get_query_var('min_price') );
			$max_price = str_replace( ' ', '', get_query_var('max_price') );
			$min_mileage = str_replace( ' ', '', get_query_var('min_mileage') );
			$max_mileage = str_replace( ' ', '', get_query_var('max_mileage') );
			$min_year = get_query_var( 'min_year' );
			$max_year = get_query_var( 'max_year' );

			// Favorites
			$show_favorites = get_query_var('favorites');
			$favorites = isset( $_COOKIE['spb_favorites'] ) ? json_decode( html_entity_decode( stripslashes ($_COOKIE['spb_favorites']))) : array();

			// Sort by
			$sortby = get_query_var('sortby');

			// Favorites filter
			if ( $show_favorites == 'false' ) {
				$query->set( 'post__in', $favorites );
			}

			if ($sortby == 'price-low') {
				$query->set( 'meta_key', 'price' );
				$query->set( 'orderby', 'meta_value_num' );
				$query->set( 'order', 'ASC' );
			} elseif ($sortby == 'price-high') {
				$query->set( 'meta_key', 'price' );
				$query->set( 'orderby', 'meta_value_num' );
				$query->set( 'order', 'DESC' );
			} elseif ($sortby == 'distance-low') {
				$query->set( 'meta_key', 'mileage' );
				$query->set( 'orderby', 'meta_value_num' );
				$query->set( 'order', 'ASC' );
			} elseif ($sortby == 'distance-high') {
				$query->set( 'meta_key', 'mileage' );
				$query->set( 'orderby', 'meta_value_num' );
				$query->set( 'order', 'DESC' );
			} elseif ($sortby == 'newest') {
				$query->set( 'meta_key', 'year' );
				$query->set( 'orderby', 'meta_value_num' );
				$query->set( 'order', 'DESC' );
			} elseif ($sortby == 'oldest') {
				$query->set( 'meta_key', 'year' );
				$query->set( 'orderby', 'meta_value_num' );
				$query->set( 'order', 'ASC' );
			} else {
				$query->set( 'meta_key', 'marketed_date' );
				$query->set( 'orderby', 'meta_value' );
				$query->set( 'order', 'DESC' );
			}

			// Search
			if ( ! empty( $s ) ) {
				$query->set('s', $s);
			}

			// Tax Queries
			// BRAND
			$brand_array = array(
				'taxonomy' => 'brand',
				'field' => 'slug',
				'terms'=> $brand
			);

			// MODEL
			$model_array = array(
				'taxonomy' => 'model',
				'field' => 'slug',
				'terms'=> $model
			);

			// FUEL
			$fuel_array = array(
				'taxonomy' => 'fuel',
				'field' => 'slug',
				'terms'=> $fuel
			);

			// TRANSMISSION
			$transmission_array = array(
				'taxonomy' => 'transmission',
				'field' => 'slug',
				'terms'=> $transmission
			);

			// TRANSMISSION
			$place_array = array(
				'taxonomy' => 'place',
				'field' => 'slug',
				'terms'=> $place
			);

			// WHEEL DRIVE
			$wheeldrive_array = array(
				'taxonomy' => 'wheeldrive',
				'field' => 'slug',
				'terms'=> $wheeldrive
			);

			// Tax Queries
			$tax_queries = array();

			if ( ! empty( get_query_var('brand') ) || ! empty ( get_query_var('model') ) || ! empty ( get_query_var('fuel') ) || ! empty ( get_query_var('transmission') ) || ! empty ( get_query_var('place') ) ) {

				if ( ! empty( get_query_var('brand') ) ) {
					array_push( $tax_queries, $brand_array );
				}

				if ( ! empty( get_query_var('model') ) ) {
					array_push( $tax_queries, $model_array );
				}

				if ( ! empty( get_query_var('fuel') ) ) {
					array_push( $tax_queries, $fuel_array );
				}

				if ( ! empty( get_query_var('transmission') ) ) {
					array_push( $tax_queries, $transmission_array );
				}

				if ( ! empty( get_query_var('place') ) ) {
					array_push( $tax_queries, $place_array );
				}

				if ( ! empty( get_query_var('wheeldrive') ) ) {
					array_push( $tax_queries, $wheeldrive_array );
				}

				$query->set( 'tax_query', $tax_queries );
			}

			// Meta Queries
			$meta_queries = array();

			// Price filter
			if ( $min_price || $max_price ) {

				$price_between_array = array(
					'key'     => 'price',
          'value'   => array($min_price, $max_price),
          'compare' => 'BETWEEN',
          'type'    => 'NUMERIC',
				);

				array_push( $meta_queries, $price_between_array );
			}

			// Mileage filter
			if ( $min_mileage || $max_mileage ) {

				$mileage_between_array = array(
					'key'     => 'mileage',
          'value'   => array($min_mileage, $max_mileage),
          'compare' => 'BETWEEN',
          'type'    => 'NUMERIC',
				);

				array_push( $meta_queries, $mileage_between_array );
			}

			// Year filter
			if ( $min_year || $max_year ) {

				$year_between_array = array(
					'key'     => 'year',
          'value'   => array($min_year, $max_year),
          'compare' => 'BETWEEN',
          'type'    => 'NUMERIC',
				);

				array_push( $meta_queries, $year_between_array );
			}

			if ( ! empty( get_query_var('favorites') ) ) {
				$query->set('post__in', $favorites );
			}

			// Set the meta queries
			$query->set( 'meta_query', $meta_queries );
    }
	}

	public static function format_price( $price ) {
		if ( ! $price ) {
			return $price;
		}

		$new_price = number_format( $price, 0, ',', ' ' );
		return $new_price . ' kr';
	}

	public static function format_mileage( $mileage ) {
		if ( ! $mileage ) {
			return $mileage;
		}

		$new_mileage = number_format( $mileage, 0, ',', ' ' );
		return $new_mileage . ' km';
	}

	/**
	 * @param float $interest - Interest rate.
	 * @param integer $payback_time - Loan length in years.
	 * @param float $total_loan - The loan amount.
	 *
	 * PMT is an excel function that is used by Nordea
	 * to calculate payment for a loan based on constant payments
	 * and a constant interest rate
	**/
	public static function calPMT( $interest, $payback_time, $total_loan ) {
	  $payback_time = $payback_time * 12;
	  $interest = $interest / 1200;
		$amount = $interest * - $total_loan * pow( ( 1 + $interest ), $payback_time ) / ( 1 - pow( ( 1 + $interest ), $payback_time ) );
	  return round( $amount );
	}

	public static function get_monthly_price( $price, $cash, $payback_time ) {
		$startup_fee = 2990;
		$car_registration_fee = 1051;
		$montly_loan_fee = 85;

		$percent = sp_bilservice_get_payment_share_percent( $price, $cash );
		$interest = sp_bilservice_get_interest_rate( $percent );

		$total_loan_fees = $price + $startup_fee + $car_registration_fee;
		$total_loan = $total_loan_fees - $cash;
		if ($total_loan_fees <= $cash) {
			$total_loan = 0;
		}

		return sp_bilservice_format_price( SP_Bilservice_Public::calPMT( $interest, $payback_time, $total_loan ) + $montly_loan_fee );
	}

	public static function get_interest_rate( $percent ) {
		global $post;
		$nominal_interest = get_field( 'nominal_interest', $post->ID );

		if ( ! $nominal_interest ) {
			return sp_bilservice_get_fallback_interest_rate( $percent );
		}

		return floatval($nominal_interest);
	}

	public static function get_fallback_interest_rate( $percent ) {
		$low = get_field('low_interest_rate', 'bilservice_options');
		$mid = get_field('medium_interest_rate', 'bilservice_options');
		$high = get_field('high_interest_rate', 'bilservice_options');

		if ( floatval($percent) > floatval(0) && floatval($percent) < floatval(19.99) ) {
			return floatval($high);
		} elseif ( floatval($percent) > floatval(20) && floatval($percent) < floatval(34.99) ) {
			return floatval($mid);
		} elseif ( floatval($percent) > floatval(35) ) {
			return floatval($low);
		} else {
			return floatval($high);
		}
	}

	public static function get_payment_share_percent( $price, $cash ) {
		$percent = ( $cash / $price ) * 100;
		return $percent;
	}

	public function get_monthly_price_ajax_callback() {
		$car_id = $_POST['car_id'];
		$payback_time = $_POST['payback_time'];
		$price = $_POST['price'];
		$cash = $_POST['cash_amount'];
		$registration_number = get_field('registration_number', $car_id);
		$year = get_field('year', $car_id);
		$brand = get_the_terms( $car_id, 'brand' );
		$brand_first = array_pop( $brand );
		$model = get_the_terms( $car_id, 'model' );
		$model_first = array_pop( $model );
		$branch_id = $_POST['branch_id'];

		$startup_fee = 2990;
		$car_registration_fee = 1051;
		$montly_loan_fee = 85;

		$percent = sp_bilservice_get_payment_share_percent( $price, $cash );
		$interest = sp_bilservice_get_interest_rate( $percent );

		$total_loan_fees = $price + $startup_fee + $car_registration_fee;
		$total_loan = $total_loan_fees - $cash;
		if ($total_loan_fees <= $cash) {
			$total_loan = 0;
		}

		$new_montly_price = sp_bilservice_format_price( SP_Bilservice_Public::calPMT( $interest, $payback_time, $total_loan ) + $montly_loan_fee );

		$new_percent = number_format( $percent, 0, ',', ' ' );

		echo json_encode( array(
			'monthly_price' => $new_montly_price,
			'cash_amount' => $cash,
			'price' => $price,
			'payback_time' => $payback_time,
			'percent' => $new_percent,
			'car_id' => $car_id,
			'branch_id' => $branch_id,
			'loan_url' => sp_bilservice_get_loan_url( $price, $cash, $payback_time, $registration_number, $brand_first->name, $model_first->name, $year, $branch_id )
		) );

		die();
	}

	public function send_popup_form_ajax_callback() {
		$name = sanitize_text_field( $_POST['name'] );
		$email = sanitize_text_field( $_POST['email'] );
		$admin_email = get_field( 'contact_person_email', 'bilservice_options' ) ?: get_option( 'admin_email' );
		$phone = preg_replace( '/[^0-9]/', '', $_POST['phone'] );
		$showing_type = sanitize_text_field( $_POST['showing_type'] );
		$connected_car = sanitize_text_field( $_POST['connected_car'] );
		$car_registration_number = get_field( 'registration_number', $connected_car );
		$terms = $_POST['terms_and_conditions'];

		if ( ! $terms ) {
			echo json_encode( array(
				'status' => 'terms_not_checked'
			) );
			die();
		}

		$place = get_the_terms( $connected_car, 'place' );
		$place_first = array_pop( $place );

		$site_name = get_bloginfo('name');
		$email_formatted = '<a href="mailto:' . $email . '">' . $email . '</a>';

		$subject = sprintf( __('A new request has been made for %s', 'sp-bilservice'), get_the_title($connected_car) );
		$body = '';
		$body .= '<p>' . sprintf( __('Congratulations, a customer has requested a %s showing. See specified info below:', 'sp-bilservice' ), $showing_type ) . '</p>';
		$body .= '<p>' . sprintf( __('Link to car: %s', 'sp-bilservice' ), '<a href="' . get_permalink($connected_car) . '">' . get_permalink($connected_car) . '</a>' ) . '</p>';
		$body .= '<p>' . sprintf( __('Place: %s', 'sp-bilservice' ), $place_first->name ) . '</p>';
		$body .= '<p>' . sprintf( __('Name: %s', 'sp-bilservice' ), $name ) . '</p>';
		$body .= '<p>' . sprintf( __('Email: %s', 'sp-bilservice' ), $email_formatted ) . '</p>';
		$body .= '<p>' . sprintf( __('Phone: %s', 'sp-bilservice' ), $phone ) . '</p>';
		if ( $car_registration_number ) {
			$body .= '<p>' . sprintf( __('Registration Number: %s', 'sp-bilservice' ), $car_registration_number ) . '</p>';
		}
		$body .= '<p>' . __( 'This information is also stored in the Wordpress Admin as a lead', 'sp-bilservice' ) . '</p>';

		$headers[] = 'From: ' . $site_name . ' <wordpress@mysite.com>';
		$headers[] = 'Content-Type: text/html; charset=UTF-8';

		$mail = wp_mail( $admin_email, $subject, $body, $headers );

		if ($mail == true) {
			$lead_args = array(
				'post_title' => $name . ' – ' . get_the_title($connected_car) . ' (' . $car_registration_number . ')',
				'post_status' => 'publish',
				'post_type' => 'lead'
			);
			$lead_id = wp_insert_post( $lead_args );

			update_field( 'name', $name, $lead_id );
			update_field( 'email', $email, $lead_id );
			update_field( 'phone', $phone, $lead_id );
			update_field( 'connected_car', $connected_car, $lead_id );
			update_field( 'car_registration_number', $car_registration_number, $lead_id );
		}

		echo json_encode( array(
			'email' => $email,
			'subject' => $subject,
			'body' => $body,
			'terms' => $terms,
			'mail_status' => $mail,
			'connected_car' => $connected_car,
			'showing_type' => $showing_type
		) );

		die();
	}

	public function load_more_cars_ajax_callback() {
		// prepare our arguments for the query
		$params = json_decode( stripslashes( $_POST['query'] ), true ); // query_posts() takes care of the necessary sanitization
		$params['paged'] = intval($_POST['page']) + 1; // we need next page to be loaded
		$params['offset'] = ( intval($params['paged']) - 1 ) * $params['posts_per_page'];
		$params['post_status'] = 'publish';

		// it is always better to use WP_Query but not here
		query_posts( $params );

		if( have_posts() ) :
			$count = 0;
			while ( have_posts() ) :
				the_post();

				$count++;
				$car_id = get_the_ID();
				
				include( plugin_dir_path( __FILE__ ) . '/templates/card-car.php');

				if (($count - 5) % 10 == 0) {
					include( plugin_dir_path( __FILE__ ) . '/templates/card-cta.php');
				}

			endwhile;
		endif;
		die; // here we exit the script and even no wp_reset_query() required!
	}

	public function spbcarsfilter_ajax_callback() {
		$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
		$params = array(
			'paged' => $paged,
			'offset' => $paged ? ($paged - 1 ) * $number : 0,
			'post_type' => 'car',
			'orderby' => 'meta_value',
			'meta_key' => 'marketed_date',
			'order'	=> 'DESC',
			'post_status' => 'publish'
		);

		$params['tax_query'] = array( 'relation' => 'AND' );
		$params['meta_query'] = array();

		$sortby = isset( $_POST['sortby'] ) ? $_POST['sortby'] : 'date';
		if ($sortby == 'price-low') {
			$params['meta_key'] = 'price';
			$params['orderby'] = 'meta_value_num';
			$params['order'] = 'ASC';
		} elseif ($sortby == 'price-high') {
			$params['meta_key'] = 'price';
			$params['orderby'] = 'meta_value_num';
			$params['order'] = 'DESC';
		} elseif ($sortby == 'distance-low') {
			$params['meta_key'] = 'mileage';
			$params['orderby'] = 'meta_value_num';
			$params['order'] = 'ASC';
		} elseif ($sortby == 'distance-high') {
			$params['meta_key'] = 'mileage';
			$params['orderby'] = 'meta_value_num';
			$params['order'] = 'DESC';
		} elseif ($sortby == 'newest') {
			$params['meta_key'] = 'year';
			$params['orderby'] = 'meta_value_num';
			$params['order'] = 'DESC';
		} elseif ($sortby == 'oldest') {
			$params['meta_key'] = 'year';
			$params['orderby'] = 'meta_value_num';
			$params['order'] = 'ASC';
		}

		// Tax Queries
		// Brands
		$brand_array = $this->add_taxonomy_array_to_tax_query( 'brand' );
		if ( count( $brand_array['terms'] ) > 0 ) {
			array_push($params['tax_query'], $brand_array);
		}

		// Models
		$model_array = $this->add_taxonomy_array_to_tax_query( 'model' );
		if ( count( $model_array['terms'] ) > 0 && count( $brand_array['terms'] ) > 0 ) {
			array_push($params['tax_query'], $model_array);
		}

		// Fuel
		$fuel_array = $this->add_taxonomy_array_to_tax_query( 'fuel' );
		if ( count( $fuel_array['terms'] ) > 0 ) {
			array_push($params['tax_query'], $fuel_array);
		}

		// Transmission
		$transmission_array = $this->add_taxonomy_array_to_tax_query( 'transmission' );
		if ( count( $transmission_array['terms'] ) > 0 ) {
			array_push($params['tax_query'], $transmission_array);
		}

		// Place
		$place_array = $this->add_taxonomy_array_to_tax_query( 'place' );
		if ( count( $place_array['terms'] ) > 0 ) {
			array_push($params['tax_query'], $place_array);
		}

		// Place
		$wheeldrive_array = $this->add_taxonomy_array_to_tax_query( 'wheeldrive' );
		if ( count( $wheeldrive_array['terms'] ) > 0 ) {
			array_push($params['tax_query'], $wheeldrive_array);
		}

		// Price filter
		if ( isset( $_POST['min_price'] ) || isset( $_POST['max_price'] ) ) {
			$min_price = $_POST['min_price'];
			$max_price = $_POST['max_price'];

			$price_between_array = array(
				'key'     => 'price',
				'value'   => array( intval($min_price), intval($max_price)),
				'compare' => 'BETWEEN',
				'type'    => 'NUMERIC'
			);

			array_push($params['meta_query'], $price_between_array);
		}

		// Mileage filter
		if ( isset( $_POST['min_mileage'] ) || isset( $_POST['max_mileage'] ) ) {
			$min_mileage = $_POST['min_mileage'];
			$max_mileage = $_POST['max_mileage'];

			$mileage_between_array = array(
				'key'     => 'mileage',
				'value'   => array( intval($min_mileage), intval($max_mileage)),
				'compare' => 'BETWEEN',
				'type'    => 'NUMERIC'
			);

			array_push($params['meta_query'], $mileage_between_array);
		}

		// Year filter
		if ( isset( $_POST['min_year'] ) || isset( $_POST['max_year'] ) ) {
			$min_year = $_POST['min_year'];
			$max_year = $_POST['max_year'];

			$year_between_array = array(
				'key'     => 'year',
				'value'   => array( intval($min_year), intval($max_year)),
				'compare' => 'BETWEEN',
				'type'    => 'NUMERIC'
			);

			array_push($params['meta_query'], $year_between_array);
		}

		// Favorites filter
		if ( isset( $_POST['show_favorites'] ) ) {
			$params['post__in'] = array(json_decode( stripslashes (isset( $_COOKIE['spb_favorites'] ) ? $_COOKIE['spb_favorites'] : array() ) ));
		}

		// Search filter
		if ( isset( $_POST['s'] ) ) {
			$params['s'] = $_POST['s'];
		}

		query_posts( $params );

		global $wp_query;

		if( have_posts() ) :
			ob_start(); // start buffering because we do not need to print the posts now

			//echo print_r($params['tax_query']);

			while( have_posts() ): the_post();

				$car_id = get_the_ID();
				include( plugin_dir_path( __FILE__ ) . '/templates/card-car.php');

			endwhile;

			$posts_html = ob_get_contents(); // we pass the posts to variable
			ob_end_clean(); // clear the buffer
		else:
			$posts_html = '<h3>' . __("Sorry, no cars matched this search", "sp-bilservice") . '</h3>';
		endif;

	 	echo json_encode( array(
			'posts' => json_encode( $wp_query->query_vars ),
			'max_page' => $wp_query->max_num_pages,
			'found_posts' => $wp_query->found_posts,
			'content' => $posts_html,
			'params' => $params
		) );

		die();
	}

	public function add_taxonomy_array_to_tax_query( $taxonomy ) {
		if ( $terms = get_terms( array( 'taxonomy' => $taxonomy ) ) ) {
			$selected_terms = array();

			// Terms
			foreach( $terms as $term ) {
				if ( isset( $_POST[$taxonomy . '_' . $term->slug ] ) && $_POST[$taxonomy . '_' . $term->slug] == $term->slug ) {
					$selected_terms[] = $term->slug;
				}
			}

			$taxonomy_array = array(
				'taxonomy' => $taxonomy,
				'field' => 'slug',
				'terms'=> $selected_terms
			);

			return $taxonomy_array;
		}
	}

	public function spbcarsfavorites_ajax_callback() {
		$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
		$params = array(
			'paged' => $paged,
			'offset' => $paged ? ($paged - 1 ) * $number : 0,
			'post_type' => 'car',
			'orderby' => 'date',
			'order'	=> 'DESC',
			'post_status' => 'publish'
		);

		$show_favorites = isset( $_POST['show_favorites'] ) ? $_POST['show_favorites'] : false;
		$favorites = isset( $_POST['show_favorites'] ) ? $_COOKIE['spb_favorites'] : null;

		// Favorites filter
		if ( isset( $_POST['show_favorites'] ) && $_POST['show_favorites'] == 'false' ) {
			$params['post__in'] = json_decode(html_entity_decode( stripslashes ($favorites)));
		}

		query_posts( $params );

		global $wp_query;

		if( have_posts() ) :
			ob_start(); // start buffering because we do not need to print the posts now

			while( have_posts() ): the_post();

				$car_id = get_the_ID();
				include( plugin_dir_path( __FILE__ ) . '/templates/card-car.php');

			endwhile;

			$posts_html = ob_get_contents(); // we pass the posts to variable
			ob_end_clean(); // clear the buffer
		else:
			$posts_html = '<h3>' . __("Sorry, no cars matched this search", "sp-bilservice") . '</h3>';
		endif;

	 	echo json_encode( array(
			'posts' => json_encode( $wp_query->query_vars ),
			'max_page' => $wp_query->max_num_pages,
			'found_posts' => $wp_query->found_posts,
			'content' => $posts_html,
			'params' => $params
		) );

		die();
	}

	public function sp_bilservice_reset_cars_ajax_callback() {
		
		$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
		$params = array(
			'paged' => $paged,
			'offset' => $paged ? ($paged - 1 ) * $number : 0,
			'post_type' => 'car',
			'orderby' => 'date',
			'order'	=> 'DESC',
			'post_status' => 'publish'
		);

		query_posts( $params );

		global $wp_query;

		if( have_posts() ) :
			ob_start(); // start buffering because we do not need to print the posts now

			while( have_posts() ): the_post();

				$car_id = get_the_ID();
				include( plugin_dir_path( __FILE__ ) . '/templates/card-car.php');

			endwhile;

			$posts_html = ob_get_contents(); // we pass the posts to variable
			ob_end_clean(); // clear the buffer
		else:
			$posts_html = '<h3>' . __("Sorry, no cars matched this search", "sp-bilservice") . '</h3>';
		endif;

	 	echo json_encode( array(
			'posts' => json_encode( $wp_query->query_vars ),
			'max_page' => $wp_query->max_num_pages,
			'found_posts' => $wp_query->found_posts,
			'content' => $posts_html,
			'params' => $params
		) );

		die();
	}

	public static function get_loan_url( $price, $cash, $payback_time, $registration_number, $brand, $model, $year, $branch_id ) {
		$domain = 'https://billanforhandler.nfn.nordea.no?';
		$low = get_field('low_interest_rate', 'bilservice_options');
		$mid = get_field('medium_interest_rate', 'bilservice_options');
		$high = get_field('high_interest_rate', 'bilservice_options');

		$url_query = array(
			'pris' => $price,
			'egenkapital' => $cash,
			'nedbetalingstid' => $payback_time,
			'nomrente1' => $low,
			'nomrente2' => $mid,
			'nomrente3' => $high
		);

		if ( ! empty( $registration_number ) ) {
			$url_query['registreringsnummer'] = $registration_number;
		} else {
			$url_query['merke'] = $brand;
			$url_query['modell'] = $model;
			$url_query['arsmodell'] = $year;
		}

		if ( ! empty( $branch_id ) ) {
			$url_query['branchId'] = $branch_id;
		}

		return $domain . http_build_query($url_query);
	}

	public static function get_main_image( $car_id ) {
		$images = get_field('images', $car_id );

		if ( ! $images ) {
			return null;
		}

		foreach ( $images as $image ) {
			$sort_order = intval(get_field( 'carweb_sort_order', $image['ID'] ));

			if ( $sort_order === 1 ) {
				return $image;
			}
		}

		// Return first image
		return $images[0];
	}

	public static function sort_images_by_carweb_sort_order( $images ) {
		if ( ! $images ) {
			return;
		}

		$count = 0;

		foreach ($images as $image) {
			$carweb_sort_order = get_field('carweb_sort_order', $image['ID']);

			if ($carweb_sort_order) {
				$images[$count]['sort_order'] = intval($carweb_sort_order);
			} else {
				$images[$count]['sort_order'] = 99;
			}

			$count++;
		}

		usort( $images, function( $a, $b ) {
			return $a['sort_order'] <=> $b['sort_order'];
		});

		return $images;
	}
}
