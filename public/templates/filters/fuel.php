<?php
/**
 * Car Archive Filter - Fuel
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/filters
 */
$fuel = get_query_var( 'fuel' );
$fuel_array = explode(",", $fuel);

if( $terms = get_terms( array( 'taxonomy' => 'fuel', 'orderby' => 'name' ) ) ) : ?>

	<div class="spb-filterbox" id="fuel">

		<header class="spb-filterbox-header">
			<p class="spb-toggle-box"><?php echo __('Fuel', 'sp-bilservice'); ?></p>
			<img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) .  '../images/keyboard-arrow-up.svg'; ?>" alt="<?php echo __('Keyboard Arrow Up Icon', 'sp-bilservice'); ?>" class="spb-toggle-box closed">
		</header>

		<div class="spb-filterbox-content filter-terms-hidden">
			<?php foreach ( $terms as $term ) : ?>

				<div class="spb-form-check">
					<input type="checkbox" id="fuel_<?php echo $term->slug; ?>" name="fuel_<?php echo $term->slug; ?>" value="<?php echo $term->slug; ?>" <?php echo in_array($term->slug, $fuel_array) ? 'checked' : ''; ?> />
					<label for="fuel_<?php echo $term->slug; ?>"><?php echo $term->name; ?></label>
				</div>

			<?php endforeach; ?>
		</div>

	</div>
<?php endif; ?>
