<?php
/**
 * Car Archive Filter - Wheel Drive
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/filters
 */
$wheeldrive = get_query_var( 'wheeldrive' );
$wheeldrive_array = explode(",", $wheeldrive);

if( $terms = get_terms( array( 'taxonomy' => 'wheeldrive', 'orderby' => 'name' ) ) ) : ?>

	<div class="spb-filterbox" id="wheeldrive">

		<header class="spb-filterbox-header">
			<p class="spb-toggle-box"><?php echo __('Wheel Drive', 'sp-bilservice'); ?></p>
			<img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) .  '../images/keyboard-arrow-up.svg'; ?>" alt="<?php echo __('Keyboard Arrow Up Icon', 'sp-bilservice'); ?>" class="spb-toggle-box closed">
		</header>

		<div class="spb-filterbox-content filter-terms-hidden">
			<?php foreach ( $terms as $term ) : ?>

				<div class="spb-form-check">
					<input type="checkbox" id="wheeldrive_<?php echo $term->slug; ?>" name="wheeldrive_<?php echo $term->slug; ?>" value="<?php echo $term->slug; ?>" <?php echo in_array($term->slug, $wheeldrive_array) ? 'checked' : ''; ?> />
					<label for="wheeldrive_<?php echo $term->slug; ?>"><?php echo $term->name; ?></label>
				</div>

			<?php endforeach; ?>
		</div>

	</div>
<?php endif; ?>
