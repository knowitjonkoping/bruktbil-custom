<?php
/**
 * Car Archive Filter - Brand
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/filters
 */

$brand = "Volvo";
$model = get_query_var( 'model' );
$brand_array = explode(",", $brand);
$model_array = explode(",", $model);
$brand_count = 0;
$main_car_brands = get_field('main_car_brands', 'bilservice_options') ?: array();

$main_terms_args = array(
	'orderby' => 'include',
	'include' => $main_car_brands
);
$main_terms = get_terms( 'brand', $main_terms_args );

$secondary_terms_args = array(
	'orderby' => 'name',
	'exclude' => $main_car_brands
);
$secondary_terms = get_terms( 'brand', $secondary_terms_args );
?>

<div class="spb-filterbox" id="brand">

	<header class="spb-filterbox-header open">
		<p class="spb-toggle-box"><?php echo __('Brand', 'sp-bilservice'); ?></p>
		<img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) .  '../images/keyboard-arrow-up.svg'; ?>" alt="<?php echo __('Keyboard Arrow Up Icon', 'sp-bilservice'); ?>" class="spb-toggle-box closed">
	</header>

	<div class="spb-filterbox-content">

		<?php if ( $main_terms ) : ?>

			<div class="spb-main-terms">

				<?php foreach ( $main_terms as $term ) :
					$brand_count++;
					$post_type  = 'car';
					$taxonomy_a = 'model';
					$taxonomy_b = 'brand';
					$term_b_id  = $term->term_id;

					$model_query = $wpdb->prepare(
					    "SELECT DISTINCT
					        terms.*
					    FROM
					        `wp_terms` terms
					    INNER JOIN
					        `wp_term_taxonomy` tt1 ON
					            tt1.term_id = terms.term_id
					    INNER JOIN
					        `wp_term_relationships` tr1 ON
					            tr1.term_taxonomy_id = tt1.term_taxonomy_id
					    INNER JOIN
					        `wp_posts` p ON
					            p.ID = tr1.object_id
					    INNER JOIN
					        `wp_term_relationships` tr2 ON
					            tr2.object_ID = p.ID
					    INNER JOIN
					        `wp_term_taxonomy` tt2 ON
					            tt2.term_taxonomy_id = tr2.term_taxonomy_id
					    WHERE
					        p.post_type = %s AND
					        p.post_status = 'publish' AND
					        tt1.taxonomy = %s AND
					        tt2.taxonomy = %s AND
					        tt2.term_id = %d",
					    [
					        $post_type,
					        $taxonomy_a,
					        $taxonomy_b,
					        $term_b_id,
					    ]
					);

					$model_results = $wpdb->get_results( $model_query );
					?>
	
					<div class="spb-form-check" data-brand-count="<?php echo $brand_count; ?>">
						<input type="checkbox" id="brand_<?php echo $term->slug; ?>" name="brand_<?php echo $term->slug; ?>" value="<?php echo $term->slug; ?>" <?php echo in_array($term->slug, $brand_array) ? 'checked' : ''; ?> />
						<label for="brand_<?php echo $term->slug; ?>"><?php echo $term->name; ?> <span class="counter">(<?php echo $term->count; ?>)</span></label>
					</div>

					<?php if ( $model_results ) {
						$model_terms = array_map( 'get_term', $model_results );

						foreach ( $model_terms as $model ) : ?>
							<div class="spb-form-check spb-second-level <?php echo in_array($term->slug, $brand_array) ? 'active' : ''; ?>" data-brand-count="<?php echo $brand_count; ?>">
								<input type="checkbox" id="model_<?php echo $model->slug; ?>" name="model_<?php echo $model->slug; ?>" value="<?php echo $model->slug; ?>" <?php echo in_array($model->slug, $model_array) ? 'checked' : ''; ?> />
								<label for="model_<?php echo $model->slug; ?>"><?php echo $model->name; ?> <span class="counter">(<?php echo nardo_bruktbiler_model_count($model->term_id); ?>)</span></label>
							</div>
						<?php
						endforeach;
					} ?>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>

		<?php if ( $secondary_terms ) : ?>
			<p class="spb-terms-title alle-merker"><?php echo __('Alle merker', 'sp-bilservice'); ?></p>
			<div class="spb-secondary-terms">
				<?php foreach ( $secondary_terms as $term ) :
					$brand_count++;
					$post_type  = 'car';
					$taxonomy_a = 'model';
					$taxonomy_b = 'brand';
					$term_b_id  = $term->term_id;

					$model_query = $wpdb->prepare(
					    "SELECT DISTINCT
					        terms.*
					    FROM
					        `wp_terms` terms
					    INNER JOIN
					        `wp_term_taxonomy` tt1 ON
					            tt1.term_id = terms.term_id
					    INNER JOIN
					        `wp_term_relationships` tr1 ON
					            tr1.term_taxonomy_id = tt1.term_taxonomy_id
					    INNER JOIN
					        `wp_posts` p ON
					            p.ID = tr1.object_id
					    INNER JOIN
					        `wp_term_relationships` tr2 ON
					            tr2.object_ID = p.ID
					    INNER JOIN
					        `wp_term_taxonomy` tt2 ON
					            tt2.term_taxonomy_id = tr2.term_taxonomy_id
					    WHERE
					        p.post_type = %s AND
					        p.post_status = 'publish' AND
					        tt1.taxonomy = %s AND
					        tt2.taxonomy = %s AND
					        tt2.term_id = %d",
					    [
					        $post_type,
					        $taxonomy_a,
					        $taxonomy_b,
					        $term_b_id,
					    ]
					);

					$model_results = $wpdb->get_results( $model_query );
					?>

					<div class="spb-form-check" data-brand-count="<?php echo $brand_count; ?>">
						<input type="checkbox" id="brand_<?php echo $term->slug; ?>" name="brand_<?php echo $term->slug; ?>" value="<?php echo $term->slug; ?>" <?php echo in_array($term->slug, $brand_array) ? 'checked' : ''; ?> />
						<label for="brand_<?php echo $term->slug; ?>"><?php echo $term->name; ?> <span class="counter">(<?php echo $term->count; ?>)</span></label>
					</div>

					<?php if ( $model_results ) {
						$model_terms = array_map( 'get_term', $model_results );

						foreach ( $model_terms as $model ) : ?>
							<div class="spb-form-check spb-second-level <?php echo in_array($term->slug, $brand_array) ? 'active' : ''; ?>" data-brand-count="<?php echo $brand_count; ?>">
								<input type="checkbox" id="model_<?php echo $model->slug; ?>" name="model_<?php echo $model->slug; ?>" value="<?php echo $model->slug; ?>" <?php echo in_array($model->slug, $model_array) ? 'checked' : ''; ?> />
								<label for="model_<?php echo $model->slug; ?>"><?php echo $model->name; ?> <span class="counter">(<?php echo nardo_bruktbiler_model_count($model->term_id); ?>)</span></label>
							</div>
						<?php endforeach;
					} ?>
				<?php endforeach; ?>
				</div>
		<?php endif; ?>
	</div>
	<script>
	$('.alle-merker').click(function() {
		$('.spb-secondary-terms').toggleClass('spb-secondary-terms-active');
	})
</script>
</div>
