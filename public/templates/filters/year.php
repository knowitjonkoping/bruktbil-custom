<?php
/**
 * Car Archive Filter - Year
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/filters
 */

$min_year = get_query_var( 'min_year' ) ?: get_field('lowest_year', 'bilservice_options');
$max_year = get_query_var( 'max_year' ) ?: date('Y');
?>

<div class="spb-filterbox" id="year">

  <header class="spb-filterbox-header">
    <p class="spb-toggle-box"><?php echo __('Year', 'sp-bilservice'); ?></p>
    <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . '../images/keyboard-arrow-up.svg'; ?>" alt="<?php echo __('Keyboard Arrow Up Icon', 'sp-bilservice'); ?>" class="spb-toggle-box closed">
  </header>

  <div class="spb-filterbox-content filter-terms-hidden">
    <div class="spb-slider-header">
      <span class="spb-left-column"><?php echo __('From', 'sp-bilservice'); ?></span>
      <span class="spb-right-column"><?php echo __('To', 'sp-bilservice'); ?></span>
    </div>
    <div id="spb-year-slider"></div>
    <div class="spb-slider-footer">
      <span id="min_year_formatted"><?php echo $min_year; ?></span>
      <span id="max_year_formatted"><?php echo $max_year; ?></span>
      <input type="hidden" name="min_year" id="min_year" value="<?php echo $min_year; ?>" />
      <input type="hidden" name="max_year" id="max_year" value="<?php echo $max_year; ?>" />
    </div>
  </div>

</div>
