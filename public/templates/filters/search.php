<?php
/**
 * Car Archive Filter - Search
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/filters
 */

$s = isset( $_GET['s'] ) ? $_GET['s'] : '';
?>

<div class="spb-search-filter">
  <input type="search" name="s" class="searchInput" value="<?php echo get_search_query(); ?>" placeholder="<?php echo __('Søk', 'sp-bilservice'); ?>" aria-label="<?php echo __('Søk', 'sp-bilservice'); ?>"/>
</div>
