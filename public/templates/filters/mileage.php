<?php
/**
 * Car Archive Filter - Mileage
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/filters
 */

$min_mileage = get_query_var( 'min_mileage' ) ?: '0';
$max_mileage = get_query_var( 'max_mileage' ) ?: get_field('highest_mileage', 'bilservice_options');
?>

<div class="spb-filterbox" id="mileage">

  <header class="spb-filterbox-header">
    <p class="spb-toggle-box"><?php echo __('Mileage', 'sp-bilservice'); ?></p>
    <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . '../images/keyboard-arrow-up.svg'; ?>" alt="<?php echo __('Keyboard Arrow Up Icon', 'sp-bilservice'); ?>" class="spb-toggle-box closed">
  </header>

  <div class="spb-filterbox-content filter-terms-hidden">
    <div class="spb-slider-header">
      <span class="spb-left-column"><?php echo __('From', 'sp-bilservice'); ?></span>
      <span class="spb-right-column"><?php echo __('To', 'sp-bilservice'); ?></span>
    </div>
    <div id="spb-mileage-slider"></div>
    <div class="spb-slider-footer">
      <span id="min_mileage_formatted"><?php echo number_format($min_mileage, 0, ',', ' '); ?></span>
      <span id="max_mileage_formatted"><?php echo number_format($max_mileage, 0, ',', ' '); ?></span>
      <input type="hidden" name="min_mileage" id="min_mileage" value="<?php echo $min_mileage; ?>" />
      <input type="hidden" name="max_mileage" id="max_mileage" value="<?php echo $max_mileage; ?>" />
    </div>
  </div>

</div>
