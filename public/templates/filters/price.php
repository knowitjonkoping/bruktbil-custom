<?php
/**
 * Car Archive Filter - Price
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/filters
 */

$min_price = get_query_var( 'min_price' ) ?: '0';
$max_price = get_query_var( 'max_price' ) ?: get_field('highest_price', 'bilservice_options');
?>

<div class="spb-filterbox" id="price">

  <header class="spb-filterbox-header">
    <p class="spb-toggle-box"><?php echo __('Price', 'sp-bilservice'); ?></p>
    <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . '../images/keyboard-arrow-up.svg'; ?>" alt="<?php echo __('Keyboard Arrow Up Icon', 'sp-bilservice'); ?>" class="spb-toggle-box closed">
  </header>

  <div class="spb-filterbox-content filter-terms-hidden">
    <div class="spb-slider-header">
      <span class="spb-left-column"><?php echo __('From', 'sp-bilservice'); ?></span>
      <span class="spb-right-column"><?php echo __('To', 'sp-bilservice'); ?></span>
    </div>
    <div id="spb-price-slider"></div>
    <div class="spb-slider-footer">
      <span id="min_price_formatted"><?php echo number_format($min_price, 0, ',', ' '); ?> kr</span>
      <span id="max_price_formatted"><?php echo number_format($max_price, 0, ',', ' '); ?> kr</span>
      <input type="hidden" name="min_price" id="min_price" value="<?php echo $min_price; ?>" />
      <input type="hidden" name="max_price" id="max_price" value="<?php echo $max_price; ?>" />
    </div>
  </div>

</div>
