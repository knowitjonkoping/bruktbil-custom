<?php
/**
 * Car Archive Filter - Transmission
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/filters
 */
$transmission = get_query_var( 'transmission' );
$transmission_array = explode( ",", $transmission );

if( $terms = get_terms( array( 'taxonomy' => 'transmission', 'orderby' => 'name' ) ) ) : ?>

	<div class="spb-filterbox" id="transmission">

		<header class="spb-filterbox-header">
			<p class="spb-toggle-box"><?php echo __('Transmission', 'sp-bilservice'); ?></p>
			<img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) .  '../images/keyboard-arrow-up.svg'; ?>" alt="<?php echo __('Keyboard Arrow Up Icon', 'sp-bilservice'); ?>" class="spb-toggle-box closed">
		</header>

		<div class="spb-filterbox-content filter-terms-hidden">
			<?php foreach ( $terms as $term ) : ?>

				<div class="spb-form-check">
					<input type="checkbox" id="transmission_<?php echo $term->slug; ?>" name="transmission_<?php echo $term->slug; ?>" value="<?php echo $term->slug; ?>" <?php echo in_array($term->slug, $transmission_array) ? 'checked' : ''; ?> />
					<label for="transmission_<?php echo $term->slug; ?>"><?php echo $term->name; ?></label>
				</div>

			<?php endforeach; ?>
		</div>

	</div>
<?php endif; ?>
