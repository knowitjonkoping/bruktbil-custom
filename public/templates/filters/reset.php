<?php
/**
 * Car Archive Filter - Reset
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/filters
 */
?>

<div class="spb-reset-filter">
	<p><a id="spb-reset-filters" href="#" title="<?php echo __('Reset all selections', 'sp-bilservice'); ?>"><?php echo __('Reset all', 'sp-bilservice'); ?></a></p>
</div>
