<?php
/**
 * Car Archive Filter - Sort by
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/filters
 */

$sortby = isset( $_GET['sortby'] ) ? $_GET['sortby'] : 'date';
?>

<div class="spb-sortby-filter">
  <p><?php echo __('Sorter på:', 'sp-bilservice'); ?></p>
  <select style="width: 205px !important;" class="spb-sortby-select" name="sortby">
    <option value="date" <?php echo ($sortby == 'date') ? 'selected' : ''; ?>><?php echo __('Published Date', 'sp-bilservice'); ?></option>
    <option value="price-low" <?php echo ($sortby == 'price-low') ? 'selected' : ''; ?>><?php echo __('Price low-high', 'sp-bilservice'); ?></option>
    <option value="price-high" <?php echo ($sortby == 'price-high') ? 'selected' : ''; ?>><?php echo __('Price high-low', 'sp-bilservice'); ?></option>
    <option value="distance-low" <?php echo ($sortby == 'distance-low') ? 'selected' : ''; ?>><?php echo __('Distance low-high', 'sp-bilservice'); ?></option>
    <option value="distance-high" <?php echo ($sortby == 'distance-high') ? 'selected' : ''; ?>><?php echo __('Distance high-low', 'sp-bilservice'); ?></option>
    <option value="newest" <?php echo ($sortby == 'newest') ? 'selected' : ''; ?>><?php echo __('New first', 'sp-bilservice'); ?></option>
    <option value="oldest" <?php echo ($sortby == 'oldest') ? 'selected' : ''; ?>><?php echo __('Old first', 'sp-bilservice'); ?></option>
  </select>
</div>
