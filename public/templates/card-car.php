<?php
/**
 * The template for the car card
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates
 */

// Card fields
$variant = get_field('variant', $car_id);
$price = get_field('price', $car_id);
$images = get_field('images', $car_id);
$main_image = sp_bilservice_get_main_image( $car_id );

// Car status
$status = get_field('status', $car_id) ?: 'for_sale';
$place=  get_the_terms( $car_id , 'place')[0]->name;

// Info bar
$year = get_field('year', $car_id);
$mileage = get_field('mileage', $car_id);
$transmission = get_the_terms($car_id, 'transmission');
$fuel = get_the_terms($car_id, 'fuel');

// Calculate monthly prices & percentages
$cash_post = isset( $_POST['cash_amount'] ) ? $_POST['cash_amount'] : null;
$cash_get = get_query_var('cash_amount');
$cash_cookie = isset( $_COOKIE['spb_cash_amount'] ) ? $_COOKIE['spb_cash_amount'] : 5000;
$payback_time_post = isset( $_POST['payback_time'] ) ? $_POST['payback_time'] : null;
$payback_time_get = get_query_var('payback_time');
$payback_time_cookie = isset( $_COOKIE['spb_payback_time'] ) ? $_COOKIE['spb_payback_time'] : 7;
if ($cash_post) {
  $cash = $cash_post;
} elseif ($cash_get) {
  $cash = $cash_get;
} else {
  $cash = $cash_cookie;
}
if ($payback_time_post) {
  $payback_time = $payback_time_post;
} elseif ($payback_time_get) {
  $payback_time = $payback_time_get;
} else {
  $payback_time = $payback_time_cookie;
}

$monthly_price = sp_bilservice_get_monthly_price( $price, $cash, $payback_time );
$payment_share_percent = sp_bilservice_get_payment_share_percent( $price, $cash );

// Favorites
$favorites = isset( $_COOKIE['spb_favorites'] ) ? json_decode( html_entity_decode( stripslashes ($_COOKIE['spb_favorites']))) : array();;
?>
<article id="post-<?php echo $car_id; ?>" <?php post_class( 'spb-card car-status-' . $status ); ?>>
  <a class="spb-card-link" href="<?php echo get_the_permalink($car_id); ?>" title="<?php the_title_attribute(); ?>">
    <div class="spb-thumbnail">
    <?php if ( ! empty( $main_image ) ) { ?>
        <img class="spb-thumbnail-img"  src="<?php echo $main_image['sizes']['cararchive']; ?>" alt="<?php echo $images[0]['alt']; ?>">
        <?php if ($status == 'sold') { ?>
            <div class="soldtrue">Solgt</div>
        <?php } ?>
      <?php } else { ?>
        <img class="spb-card-car-placeholder" src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) .  'images/car-placeholder.svg'; ?>" alt="<?php echo __('Car Placeholder Image', 'sp-bilservice'); ?>">
      <?php } ?>
    </div>
    <div class="spb-card-main-info">
      <div class="spb-card-left-column spb-remove-margins">
        <p class="spb-card-place"><?php echo $place; ?></p>
        <h3 class="spb-card-title spb-text-medium">
        <?php echo get_the_title(); ?>
      </h3>
        <p class="spb-card-subtitle spb-text-base"><?php echo $variant; ?></p>
        <div class="spb-card-details">
          <?php if ($year) { ?>
            <div class="year">
              <p><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) .  'images/year.svg'; ?>"><?php echo $year; ?></p>
            </div>
          <?php } ?>
          <?php if ($mileage) { ?>
            <div class="mileage">
              <p><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) .  'images/mileage.svg'; ?>"> <?php echo sp_bilservice_format_mileage( $mileage ); ?></p>
            </div>
          <?php } ?>
          <?php if ($price) { ?>
            <div class="price">
              <p><?php echo str_replace("kr"," kr", sp_bilservice_format_price( $price )); ?></p>
            </div>
            <?php } ?>
        </div>
        
      </div>

    </div>
  </a>
</article><!-- #post-<?php the_ID(); ?> -->
