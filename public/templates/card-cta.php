<?php
/**
 * The template for the cta card
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates
 */

// CTA fields
$cta_title = get_field('cta_title', 'bilservice_options');
$cta_text = get_field('cta_text', 'bilservice_options');
$cta_button_text = get_field('cta_button_text', 'bilservice_options');
$cta_button_url = get_field('cta_button_url', 'bilservice_options');

?>
<article class="spb-cta-new spb-remove-margins">
  <div class="cta-inner">
    <?php if ($cta_title) { ?>
      <h3><?php echo $cta_title; ?></h3>
    <?php } ?>

    <?php if ($cta_text) { ?>
      <?php echo wpautop($cta_text); ?>
    <?php } ?>

    <?php if ($cta_button_text && $cta_button_url) { ?>
      <a href="<?php echo $cta_button_url; ?>" class="btn"><?php echo $cta_button_text; ?></a>
    <?php } ?>
  </div>
</article>
