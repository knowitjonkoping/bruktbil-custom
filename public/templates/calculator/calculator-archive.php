<?php
/**
 * Car Archive Calculator
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/filters/calculator
 */

global $wp_query;

// Calculate monthly prices & percentages
// Calculate monthly prices & percentages
$cash_post = isset( $_POST['cash_amount'] ) ? $_POST['cash_amount'] : 0;
$cash_get = get_query_var('cash_amount');
$cash_cookie = isset( $_COOKIE['spb_cash_amount'] ) ? $_COOKIE['spb_cash_amount'] : 5000;
$payback_time_post = isset( $_POST['payback_time'] ) ? $_POST['payback_time'] : 0;
$payback_time_get = get_query_var('payback_time');
$payback_time_cookie = isset( $_COOKIE['spb_payback_time'] ) ? $_COOKIE['spb_payback_time'] : 7;
if ($cash_post) {
  $cash = $cash_post;
} elseif ($cash_get) {
  $cash = $cash_get;
} else {
  $cash = $cash_cookie;
}
if ($payback_time_post) {
  $payback_time = $payback_time_post;
} elseif ($payback_time_get) {
  $payback_time = $payback_time_get;
} else {
  $payback_time = $payback_time_cookie;
}
?>

<div class="spb-filterbox-content" id="spb-cash-amount">
  <div class="spb-slider-header">
    <span class="spb-left-column"><?php echo __('Cash Amount', 'sp-bilservice'); ?></span>
  </div>
  <div id="spb-cash-amount-slider"></div>
  <div class="spb-slider-footer">
    <span id="cash_amount_formatted"><?php echo number_format($cash, 0, ',', ' '); ?> kr</span>
  </div>
</div>

<div class="spb-filterbox-content" id="spb-payment-time">
  <div class="spb-slider-header">
    <span class="spb-left-column"><?php echo __('Payback Time', 'sp-bilservice'); ?></span>
  </div>
  <div id="spb-payback-time-slider"></div>
  <div class="spb-slider-footer">
    <span id="payback_time_formatted"><?php echo sprintf( __('%d years', 'sp-bilservice'), $payback_time ); ?></span>
  </div>
</div>
