<?php
/**
 * The template for displaying all single cars
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/partials
 */

get_header();

// Options
$archive_title = get_field('archive_title', 'bilservice_options') ?: 'display';
$calculator_financing_url = get_field('calculator_financing_url', 'bilservice_options');

// Favorites
$favorites = isset( $_COOKIE['spb_favorites'] ) ? json_decode( stripslashes( $_COOKIE['spb_favorites'] ) ) : array();

$total_cars = isset( $_GET['total_cars'] ) ? $_GET['total_cars'] : $wp_query->found_posts;

//echo print_r($wp_query->query_vars);
//echo print_r($wp_query);

// Number Pagination Function 
function bruktbiler_number_pagination() {
	// Get total number of pages
	global $wp_query;
	$total = $wp_query->max_num_pages;
	// Only paginate if we have more than one page
	if ( $total > 1 )  {
		// Get the current page
		if ( !$current_page = get_query_var('paged') )
			$current_page = 1;
		// Structure of “format” depends on whether we’re using pretty permalinks
		$format = empty( get_option('permalink_structure') ) ? '&page=%#%' : '&page=%#%';
		$pl_args = array(
			'base' => get_pagenum_link(1) . '%_%',
			'format' => $format,
			'current' => $current_page,
			'total' => $total,
			'mid_size' => 3,
			'show_all' => false,
			'type' => 'list',
			'prev_next' => true,
			'prev_text' => '<svg width="11" height="16" viewBox="0 0 11 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M0.453125 14.12L6.55979 8L0.453124 1.88L2.33312 -8.21774e-08L10.3331 8L2.33312 16L0.453125 14.12Z" fill="#0D1A25"/> </svg>',
			'next_text' => '<svg width="11" height="16" viewBox="0 0 11 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M0.453125 14.12L6.55979 8L0.453124 1.88L2.33312 -8.21774e-08L10.3331 8L2.33312 16L0.453125 14.12Z" fill="#0D1A25"/> </svg>'
		);
		echo paginate_links($pl_args);
	}
}

?>

<div class="spb-wrap-wide nardo-grid">

<!-- <div class="breadcrumb">Hjem - Bruktbil</div> -->
	<section id="spb-content" class="cf">
	<?php if ( $archive_title == 'display' ) { ?>
				<header class="spb-page-header mobile">
					<h1 class="spb-page-title">Våre bruktbiler</h1>
				</header><!-- .page-header -->
			<?php } ?>

		<aside class="spb-filters">
			<?php if ( $archive_title == 'display' ) { ?>
				<header class="spb-page-header desktop">
					<h1 class="spb-page-title">Våre bruktbiler</h1>
				</header><!-- .page-header -->
			<?php } ?>
			<div class="filter-header inside">
					<a class="spb-toggle-filters inside" style="border: none !important;">
						<span>
							<svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M14 1.41L12.59 0L7 5.59L1.41 0L0 1.41L5.59 7L0 12.59L1.41 14L7 8.41L12.59 14L14 12.59L8.41 7L14 1.41Z" fill="black"/>
							</svg>
						</span>
					</a>
					<a class="inside">Filter</a>
					<a id="spb-reset-filters" class="inside" href="#" title="Reset all selections">Nullstill</a></a>
			</div>

			<form action="#" id="spb-filters" autocomplete="on">
				<?php
					//include( plugin_dir_path( __FILE__ ) . '/filters/reset.php');
					include( plugin_dir_path( __FILE__ ) . '/filters/search.php');
					include( plugin_dir_path( __FILE__ ) . '/filters/brand.php');
					include( plugin_dir_path( __FILE__ ) . '/filters/price.php');
					include( plugin_dir_path( __FILE__ ) . '/filters/year.php');
					include( plugin_dir_path( __FILE__ ) . '/filters/mileage.php');
					include( plugin_dir_path( __FILE__ ) . '/filters/fuel.php');
					include( plugin_dir_path( __FILE__ ) . '/filters/transmission.php');
					include( plugin_dir_path( __FILE__ ) . '/filters/wheeldrive.php');
					include( plugin_dir_path( __FILE__ ) . '/filters/place.php');
				?>

				<!-- other hidden fields -->
				<?php
				$cash_amount = isset( $_GET['cash_amount'] ) ? $_GET['cash_amount'] : 50000;
				$payback_time = isset( $_GET['payback_time'] ) ? $_GET['payback_time'] : 7;
				$sortby = isset( $_GET['sortby'] ) ? $_GET['sortby'] : 'date';
				?>
				<!-- Calculator -->
				<input type="hidden" name="cash_amount" id="cash_amount" value="<?php echo $cash_amount; ?>" />
				<input type="hidden" name="payback_time" id="payback_time" value="<?php echo $payback_time; ?>" />

				<!-- Sortby -->
				<input type="hidden" name="sortby" id="sortby" value="<?php echo $sortby; ?>" />

				<!-- required hidden field for admin-ajax.php -->
				<input type="hidden" name="action" value="spbcarsfilter_ajax_callback" />
			</form>
	  </aside>

	  <div class="renteKampanje">
			<?php
				$image = get_field('image', 'bilservice_options');
				$title = get_field('title', 'bilservice_options');
				$content = get_field('content', 'bilservice_options');
				$button_text = get_field('button_text', 'bilservice_options');
				$button_link = get_field('button_link', 'bilservice_options');
			?>
			<div class="renteKampanjeImage">
				<img src="<?php echo $image['url']; ?>" style="height: 210px; width: 270px">
			</div>
			<div class="renteKampanjeContent">
				<div>
					<h2><?php echo $title; ?></h2>
					<p><?php echo $content; ?></p>
					<?php if(!empty($button_link['url'])){ ?>
						<a href="<?php echo $button_link['url']; ?>"><?php echo $button_text; ?></a>
					<?php } ?>
				</div>
			</div>
	  </div>

		<div class="spb-extra-car-filters">
			<div class="spb-extra-filters-top">
				<?php include( plugin_dir_path( __FILE__ ) . '/filters/sortby.php'); ?>
			</div>

			<div class="spb-form-header">
				<a class="spb-toggle-filters">
				<span><?php echo __('Filter', 'sp-bilservice'); ?></span>
				<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M0.5 12.1667V13.8333H5.5V12.1667H0.5ZM0.5 2.16667V3.83333H8.83333V2.16667H0.5ZM8.83333 15.5V13.8333H15.5V12.1667H8.83333V10.5H7.16667V15.5H8.83333ZM3.83333 5.5V7.16667H0.5V8.83333H3.83333V10.5H5.5V5.5H3.83333ZM15.5 8.83333V7.16667H7.16667V8.83333H15.5ZM10.5 5.5H12.1667V3.83333H15.5V2.16667H12.1667V0.5H10.5V5.5Z" fill="black"/>
				</svg>
				</a>
			</div>
		</div>

		<?php if ( have_posts() ) : ?>

		  <section class="spb-cars">
		    <?php
				$count = 0;
				while ( have_posts() ) :
		  		the_post();

					$count++;
					$car_id = get_the_ID();

					include( plugin_dir_path( __FILE__ ) . '/card-car.php');

					if (($count - 5) % 10 == 0) {
						include( plugin_dir_path( __FILE__ ) . '/card-cta.php');
					}

		  	endwhile;
				?>
		  </section>
		  <section class="spb-pagination">

			<button id="spb-loadmore">Last flere</button>
		  		<?php // bruktbiler_number_pagination(); ?>
		  </section>
		<?php else : ?>

			<section class="spb-cars">
		    <h3><?php echo __('Sorry, no cars matched this search', 'sp-bilservice'); ?></h3>
		  </section>

		<?php endif; ?>

	</section>
</div>

<?php get_footer(); ?>
