
<?php
/**
 * The template for displaying all single cars
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/partials
 */

get_header();

    the_post();

    global $wp_query;

    // Calculate monthly prices & percentages
    // Calculate monthly prices & percentages
    $cash_post = isset($_POST['cash_amount']) ? $_POST['cash_amount'] : null;
    $cash_get = get_query_var('cash_amount');
    $cash_cookie = isset($_COOKIE['spb_cash_amount'])
        ? $_COOKIE['spb_cash_amount']
        : 5000;
    $payback_time_post = isset($_POST['payback_time'])
        ? $_POST['payback_time']
        : null;
    $payback_time_get = get_query_var('payback_time');
    $payback_time_cookie = isset($_COOKIE['spb_payback_time'])
        ? $_COOKIE['spb_payback_time']
        : 7;
    if ($cash_post) {
        $cash = $cash_post;
    } elseif ($cash_get) {
        $cash = $cash_get;
    } else {
        $cash = $cash_cookie;
    }
    if ($payback_time_post) {
        $payback_time = $payback_time_post;
    } elseif ($payback_time_get) {
        $payback_time = $payback_time_get;
    } else {
        $payback_time = $payback_time_cookie;
    }
    // Car id
    $car_id = get_the_ID();
    $GLOBALS['carid'] = $car_id;

    $car_reg = get_field('registration_number', $car_id);
    $subtext = get_field('calculator_subtext', 'bilservice_options');

    $place = get_the_terms( get_the_ID(), 'place' );
		$branch_id = false;
		if ($place) {
			$branch_id = get_field('branch_id', $place[0]);
		}
   
    // Car status
    $status = get_field('status', $car_id) ?: 'for_sale';
    // Fields
    $variant = get_field('variant', $car_id);
    $price = get_field('price', $car_id);
    $monthly_price = sp_bilservice_get_monthly_price(
        $price,
        $cash,
        $payback_time
    );
    $payment_share_percent = sp_bilservice_get_payment_share_percent(
        $price,
        $cash
    );
    ?>
<div class="spb-wrap-wide-singlecar">
  <!-- <div class="breadcrumb">Bruktbil - <?php // the_title(); ?></div> -->

  <div class="bruktbiler">
    <!-- Component: Hero -->
    <div class="bruktbilHero">
      <div class="g-12">
        <div class="gs12 gs12-m gs6-l first-col sizeTwo">
          <?php include plugin_dir_path(__FILE__) . '/car/car-images.php'; ?>
        </div>
        <div class="gs12 gs12-m gs6-l second-col sizeTwo">
          <div class="content-image_and_text">
          <?php if ($status == 'sold') { ?>
            <div class="soldtruesingle">Solgt</div>
            <?php } ?>
          <p class="city"><?= get_the_terms( $car_id , 'place')[0]->name; ?></p>
            <h2><?php the_title(); ?>
            <?php 
            if ($status == 'sold') 
            { 
              echo " - Solgt";
            }
             ?>
          </h2>
            <p><?php echo $variant; ?></p>
            <!-- Prices -->
            <div class="prices">
              <p><?php echo sp_bilservice_format_price($price); ?></p>
              <a href="#lanekalkulator" class="price_from_link">
              <p class="spb-text-base spb-monthly-price per-mande">eller  <span>3197</span> kr per mnd</p>
            </a>
            </div>
            <?php if ($status == 'sold') { ?>
             <!-- <a href="#flerebilertittel" class="btn btn-square btn-blue arrow-right btn-margin-right">Denne bilen er solgt, se tilsvarende biler</a> -->
            <?php } else {?>  
              
            <!-- Buttons -->
            <a href="<?=get_field('sale_form_page', 'options') ? get_field('sale_form_page', 'options') . '?carId='. $car_id : '#' ;?>" class="btn btn-square btn-blue arrow-right btn-margin-right">Kjøp denne bilen</a>
            <a href="#" id="spb-schedule-physical-showing" class="btn btn-black-border">Prøvekjør</a>
            <div class="contantInfoSimple">
              <p><a href="#selgere" class="trengerdukjopshjelp">Trenger du kjøpshjelp?</a></p>
            </div>
              <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="spb-wrap-left-column">
  <div class="spb-single-wrap">
    <?php include plugin_dir_path(__FILE__) . '/car/car-anchor-links.php'; ?>
  </div>
</div>
<div class="spb-wrap">
  <section class="">
    <?php include plugin_dir_path(__FILE__) . '/car/car-info-grid.php'; ?>
  </section>
</div>
<div class="spb-wrap-left-column">
  <div class="spb-single-wrap">
    <section class="spb-wrap-left-column">
    <?php include plugin_dir_path(__FILE__) . '/car/car-description.php'; ?>
    </section>
  </div>
</div>

<!-- SPECS -->
<div class="car-specs">
  <?php include plugin_dir_path(__FILE__) . '/car/car-specs.php'; ?>
</div>
<!-- SPECS END -->

<div class="spb-wrap tilstandsrapport">
  	<div class="spb-single-wrap">
		<section class="spb-wrap-left-column">
				<?php include plugin_dir_path(__FILE__) . '/car/car-report.php'; ?>
		</section>
	</div>
</div>

<div class="bordertop"></div>

<div class="spb-wrap-wide nardo-grid">
  	<section class="g-24 nardo-grid">
		<aside class="spb-wrap-right-column gs24">
			<?php include plugin_dir_path(__FILE__) . '/car/car-department.php'; ?>
		</aside>
  	</section>
</div>

<!-- Fordeler -->
<?php include( plugin_dir_path(__FILE__) . '/car/car-benefit-cards.php'); ?>

<!-- calculator custom -->
<?php include( plugin_dir_path(__FILE__) . '/car/car-single-calculator.php'); ?>

<!-- More cars -->
<?php include( plugin_dir_path(__FILE__) . '/car/car-morecars.php'); ?>

<!-- sales people -->
<?php include plugin_dir_path(__FILE__) . '/car/car-salesperson.php'; ?>

<!-- Pop up -->
<?php include plugin_dir_path(__FILE__) . '/car/car-popup.php'; ?>

<?php

wp_reset_postdata();

wp_reset_query();

get_footer();
