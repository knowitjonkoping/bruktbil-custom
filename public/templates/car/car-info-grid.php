<?php
/**
 * Car Info Grid
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/car/car-info-grid
 */

// Variables 
// Car id
$car_id = get_the_ID();
$year = get_field('year', $car_id);
$registration_date = get_field('registration_date', $car_id);
$mileage = get_field('mileage', $car_id);
$registration_number = get_field('registration_number', $car_id);
$transmission = get_the_terms($car_id, 'transmission');
$fuel = get_the_terms($car_id, 'fuel');
$engine_power = get_field('engine_power', $car_id);
$engine_volume = get_field('engine_volume', $car_id);
$doors = get_field('doors', $car_id);
$cargo_volume = get_field('cargo_volume', $car_id);
$drive_wheel = get_the_terms($car_id, 'wheeldrive');
$car_body = get_field('car_body', $car_id);
$towing_weight = get_field('towing_weight', $car_id);
$exterior_color = get_field('exterior_color', $car_id);
$interior_color = get_field('interior_color', $car_id);
?>

<div class="spb-info-box spb-info-grid spb-car-info-grid">

  <div class="spb-info-line">
    <div class="icon">
    <svg width="20" height="17" viewBox="0 0 20 17" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M18.3792 4.5711L17.1492 6.4211C17.7424 7.60417 18.0328 8.91593 17.9944 10.2388C17.956 11.5617 17.59 12.8544 16.9292 14.0011H3.0692C2.21036 12.5112 1.85449 10.7842 2.05434 9.07617C2.25418 7.36811 2.99911 5.76992 4.17867 4.51853C5.35824 3.26713 6.90966 2.42914 8.60293 2.12879C10.2962 1.82845 12.0412 2.08173 13.5792 2.8511L15.4292 1.6211C13.5457 0.413314 11.3115 -0.127897 9.08408 0.0840341C6.85665 0.295966 4.76464 1.24879 3.14269 2.79011C1.52074 4.33142 0.462557 6.37217 0.137413 8.58591C-0.187731 10.7997 0.238946 13.0585 1.3492 15.0011C1.52371 15.3034 1.77428 15.5547 2.07603 15.7301C2.37777 15.9056 2.72017 15.999 3.0692 16.0011H16.9192C17.2716 16.0025 17.6181 15.9107 17.9237 15.7351C18.2293 15.5595 18.483 15.3063 18.6592 15.0011C19.5806 13.405 20.043 11.5854 19.9953 9.74305C19.9477 7.90073 19.3918 6.10742 18.3892 4.5611L18.3792 4.5711Z" fill="#0D1A25"/>
      <path d="M8.59048 11.4093C8.77623 11.5952 8.9968 11.7428 9.2396 11.8434C9.4824 11.944 9.74265 11.9959 10.0055 11.9959C10.2683 11.9959 10.5286 11.944 10.7714 11.8434C11.0142 11.7428 11.2347 11.5952 11.4205 11.4093L17.0805 2.91928L8.59048 8.57928C8.40453 8.76503 8.25701 8.9856 8.15636 9.2284C8.05571 9.4712 8.00391 9.73145 8.00391 9.99428C8.00391 10.2571 8.05571 10.5174 8.15636 10.7602C8.25701 11.003 8.40453 11.2235 8.59048 11.4093Z" fill="#0D1A25"/>
    </svg>

    </div>
    <div class="info-large">
      <p class="spb-text-mini spb-color-dark-gray spb-info-mini"><?php echo __('Kilometer', 'sp-bilservice'); ?></p>
      <p class="spb-text-base spb-info-large"><?php echo number_format($mileage, 0, '', ' '); ?></p>
    </div>
  </div>

  <div class="spb-info-line">
    <div class="icon">
    <svg width="17" height="18" viewBox="0 0 17 18" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M15.77 4.23L15.78 4.22L12.06 0.5L11 1.56L13.11 3.67C12.17 4.03 11.5 4.93 11.5 6C11.5 7.38 12.62 8.5 14 8.5C14.36 8.5 14.69 8.42 15 8.29V15.5C15 16.05 14.55 16.5 14 16.5C13.45 16.5 13 16.05 13 15.5V11C13 9.9 12.1 9 11 9H10V2C10 0.9 9.1 0 8 0H2C0.9 0 0 0.9 0 2V18H10V10.5H11.5V15.5C11.5 16.88 12.62 18 14 18C15.38 18 16.5 16.88 16.5 15.5V6C16.5 5.31 16.22 4.68 15.77 4.23ZM8 8V16H2V2H8V8ZM14 7C13.45 7 13 6.55 13 6C13 5.45 13.45 5 14 5C14.55 5 15 5.45 15 6C15 6.55 14.55 7 14 7ZM6 3L2 10.5H4V15L8 8H6V3Z" fill="#0D1A25"/>
    </svg>

    </div>
    <div class="info-large">
    <?php
    $fuel_names = array();
    foreach ($fuel as $term) {
      $fuel_names[] = $term->name;
    } ?>
      <p class="spb-text-mini spb-color-dark-gray spb-info-mini"><?php echo __('Drivstoff', 'sp-bilservice'); ?></p>
      <p class="spb-text-base spb-info-large"><?php echo implode( ', ', $fuel_names ); ?></p>
    </div>
  </div>

  <div class="spb-info-line">
    <div class="icon">
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M17.4289 10.98C17.4689 10.66 17.4989 10.34 17.4989 10C17.4989 9.66 17.4689 9.34 17.4289 9.02L19.5389 7.37C19.7289 7.22 19.7789 6.95 19.6589 6.73L17.6589 3.27C17.5689 3.11 17.3989 3.02 17.2189 3.02C17.1589 3.02 17.0989 3.03 17.0489 3.05L14.5589 4.05C14.0389 3.65 13.4789 3.32 12.8689 3.07L12.4889 0.42C12.4589 0.18 12.2489 0 11.9989 0H7.99886C7.74886 0 7.53886 0.18 7.50886 0.42L7.12886 3.07C6.51886 3.32 5.95886 3.66 5.43886 4.05L2.94886 3.05C2.88886 3.03 2.82886 3.02 2.76886 3.02C2.59886 3.02 2.42886 3.11 2.33886 3.27L0.338863 6.73C0.208863 6.95 0.268863 7.22 0.458863 7.37L2.56886 9.02C2.52886 9.34 2.49886 9.67 2.49886 10C2.49886 10.33 2.52886 10.66 2.56886 10.98L0.458863 12.63C0.268863 12.78 0.218863 13.05 0.338863 13.27L2.33886 16.73C2.42886 16.89 2.59886 16.98 2.77886 16.98C2.83886 16.98 2.89886 16.97 2.94886 16.95L5.43886 15.95C5.95886 16.35 6.51886 16.68 7.12886 16.93L7.50886 19.58C7.53886 19.82 7.74886 20 7.99886 20H11.9989C12.2489 20 12.4589 19.82 12.4889 19.58L12.8689 16.93C13.4789 16.68 14.0389 16.34 14.5589 15.95L17.0489 16.95C17.1089 16.97 17.1689 16.98 17.2289 16.98C17.3989 16.98 17.5689 16.89 17.6589 16.73L19.6589 13.27C19.7789 13.05 19.7289 12.78 19.5389 12.63L17.4289 10.98ZM15.4489 9.27C15.4889 9.58 15.4989 9.79 15.4989 10C15.4989 10.21 15.4789 10.43 15.4489 10.73L15.3089 11.86L16.1989 12.56L17.2789 13.4L16.5789 14.61L15.3089 14.1L14.2689 13.68L13.3689 14.36C12.9389 14.68 12.5289 14.92 12.1189 15.09L11.0589 15.52L10.8989 16.65L10.6989 18H9.29886L9.10886 16.65L8.94886 15.52L7.88886 15.09C7.45886 14.91 7.05886 14.68 6.65886 14.38L5.74886 13.68L4.68886 14.11L3.41886 14.62L2.71886 13.41L3.79886 12.57L4.68886 11.87L4.54886 10.74C4.51886 10.43 4.49886 10.2 4.49886 10C4.49886 9.8 4.51886 9.57 4.54886 9.27L4.68886 8.14L3.79886 7.44L2.71886 6.6L3.41886 5.39L4.68886 5.9L5.72886 6.32L6.62886 5.64C7.05886 5.32 7.46886 5.08 7.87886 4.91L8.93886 4.48L9.09886 3.35L9.29886 2H10.6889L10.8789 3.35L11.0389 4.48L12.0989 4.91C12.5289 5.09 12.9289 5.32 13.3289 5.62L14.2389 6.32L15.2989 5.89L16.5689 5.38L17.2689 6.59L16.1989 7.44L15.3089 8.14L15.4489 9.27ZM9.99886 6C7.78886 6 5.99886 7.79 5.99886 10C5.99886 12.21 7.78886 14 9.99886 14C12.2089 14 13.9989 12.21 13.9989 10C13.9989 7.79 12.2089 6 9.99886 6ZM9.99886 12C8.89886 12 7.99886 11.1 7.99886 10C7.99886 8.9 8.89886 8 9.99886 8C11.0989 8 11.9989 8.9 11.9989 10C11.9989 11.1 11.0989 12 9.99886 12Z" fill="#0D1A25"/>
    </svg>

    </div>
    <div class="info-large">
    <?php
    $transmission_names = array();
    foreach ($transmission as $term) {
      $transmission_names[] = $term->name;
    } ?>
      <p class="spb-text-mini spb-color-dark-gray spb-info-mini"><?php echo __('Gir', 'sp-bilservice'); ?></p>
      <p class="spb-text-base spb-info-large"><?php echo implode( ', ', $transmission_names ); ?></p>
    </div>
  </div>

  <div class="spb-info-line">
    <div class="icon">
    <svg width="20" height="22" viewBox="0 0 20 22" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M18 2H17V0H15V2H5V0H3V2H2C0.9 2 0 2.9 0 4V20C0 21.1 0.9 22 2 22H18C19.1 22 20 21.1 20 20V4C20 2.9 19.1 2 18 2ZM18 20H2V9H18V20ZM18 7H2V4H18V7Z" fill="#0D1A25"/>
    </svg>

    </div>
    <div class="info-large">
      <p class="spb-text-mini spb-color-dark-gray spb-info-mini"><?php echo __('Modellår', 'sp-bilservice'); ?></p>
      <p class="spb-text-base spb-info-large"><?php echo $year; ?></p>
    </div>
  </div>

  <div class="spb-info-line">
    <div class="icon">
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M12.536 11.64H13.992V12.784C13.992 12.8907 13.9573 12.984 13.888 13.064C13.8187 13.1387 13.7173 13.176 13.584 13.176H12.536V16H10.808V13.176H5.928C5.79467 13.176 5.67733 13.136 5.576 13.056C5.47467 12.9707 5.41067 12.8667 5.384 12.744L5.184 11.744L10.664 4.424H12.536V11.64ZM10.808 7.856C10.808 7.68533 10.8133 7.50133 10.824 7.304C10.8347 7.10667 10.8533 6.90133 10.88 6.688L7.288 11.64H10.808V7.856Z" fill="#0D1A25"/>
      <rect x="0.5" y="0.5" width="19" height="19" rx="3.5" stroke="#0D1A25"/>
    </svg>

    </div>
    <div class="info-large">
    <?php
    $wheel_names = array();
    foreach ($drive_wheel as $term) {
      $wheel_names[] = $term->name;
    } ?>
      <p class="spb-text-mini spb-color-dark-gray spb-info-mini"><?php echo __('Hjuldrift', 'sp-bilservice'); ?></p>
      <p class="spb-text-base spb-info-large"><?php echo implode( ', ', $wheel_names ); ?></p>
    </div>
  </div>
</div>
