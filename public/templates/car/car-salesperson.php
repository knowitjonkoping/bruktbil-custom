<?php
    $carid = $GLOBALS['carid'];
    $place = get_the_terms( $carid, 'place' );
	$avdeling_id = get_field('avdeling', $place[0]);
    $avdelning_name = get_the_terms( $carid , 'place')[0]->name;
    $avdelning_slug = get_the_terms( $carid , 'place')[0]->slug;
?>
<div class="contact">

    <div class="g-24 nardo-grid">
        <h2 id="selgere" class="gs22-m gs24">Er det noe du lurer på så ta kontakt med en av våre selgere i 
            <?php if($avdelning_slug == 'ranheim') { echo 'Trondheim - '; } ?> <?= $avdelning_name; ?></h2>
        <div class="swiper nardoSwiper  g-24 gs24 swiper-initialized swiper-horizontal swiper-pointer-events">
            <div class="swiper-wrapper card_container g-24 gs24" id="contact-card-container" style="transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
                <?php
                $args = array(
                    'posts_per_page'	=> -1,
                    'post_type'		    => 'kontakt',
                    'orderby'           => 'rand',
                );
                $the_query = new WP_Query( $args );
                if( $the_query->have_posts() ) {
                    while( $the_query->have_posts() ) {
                        $the_query->the_post();
                        $wp_avdelning = get_field('avdeling');
                        $wp_task = get_field('task');
                        if($wp_avdelning->post_name == $avdelning_slug && $wp_task->slug == 'bilsalg'){?>
                            <div class="card swiper-slide swiper-slide-active">
                                <img src="<?= get_field('image')?>" alt="Man">
                                <h3><?= get_field('name')?></h3></p>
                                <p class="section"><?= get_field('description')?></p>
                                <p class="tel"><a href="tel:<?= get_field('phone')?>"><?= get_field('phone')?></a></p>
                                <p class="email"><a href="mailto:<?= get_field('email')?>">Send epost</a></p>
                            </div>
                        <?php
                        }
                    }
                    wp_reset_postdata(); 
                }
                ?>
            </div>
        </div>
    </div>
    <div class="loader">
        <div class="lds-ripple"><div></div><div></div></div>
    </div>
</div>
