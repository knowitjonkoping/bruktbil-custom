<?php
/**
 * Car Description
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/car/car-info-grid
 */

// Variables
// Car id
$car_id = get_the_ID();
?>

<div class="spb-info-box spb-description-container">
  <div class="spb-description">
    <h3><?php echo __('Beskrivelse', 'sp-bilservice'); ?></h3>
    <div class="firstContent closed">
    <?php
        the_content();
        ?>
    </div>
    <div><a id="visAlt" class="btn btn-square btn-black-border btn-visAlt">Vis alt</a>
</div>
  </div>
</div>
