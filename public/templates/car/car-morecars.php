<?php
// WP_Query arguments
$args = array(
  'post_type' => 'car',
  'orderby' => 'meta_value',
  'meta_key' => 'marketed_date',
  'order'  => 'DESC',
  'post_status' => 'publish',
  'posts_per_page' => 3
);

// The Query
$query = new WP_Query($args);

// The Loop
if ($query->have_posts()) {
?>
  <div class="moreusedcars nardo-grid">
    <div>
      <h2 id="flerebilertittel" class="flerebilertittel">Andre biler som kan være av interesse</h2>
      <div class="scrolling-wrapper-flexbox-bruktbil">
        <?php
        while ($query->have_posts()) {
          $query->the_post();
          // do something
          $car_id = get_the_ID();
          // Card fields
          $variant = get_field('variant', $car_id);
          $price = get_field('price', $car_id);
          $images = get_field('images', $car_id);

          // Car status
          $status = get_field('status', $car_id) ?: 'for_sale';

          // Info bar
          $year = get_field('year', $car_id);
          $mileage = get_field('mileage', $car_id);
          $transmission = get_the_terms($car_id, 'transmission');
          $fuel = get_the_terms($car_id, 'fuel');

        ?>
          <div class="card-bruktbil">
            <div class="desktop">
              <article id="post-<?php echo $car_id; ?>" <?php post_class('spb-card car-status-' . $status); ?>>
                <a class="spb-card-link" href="<?php echo get_the_permalink($car_id); ?>" title="<?php the_title_attribute(); ?>">
                  <div class="spb-thumbnail">
                    <?php if (!empty($images[0])) { ?>
                      <img src="<?php echo $images[0]['sizes']['cararchive']; ?>" alt="<?php echo $images[0]['alt']; ?>">
                    <?php } else { ?>
                      <img class="spb-card-car-placeholder" src="<?php echo plugin_dir_url(dirname(__FILE__)) .  '../images/car-placeholder.svg'; ?>" alt="<?php echo __('Car Placeholder Image', 'sp-bilservice'); ?>">
                    <?php } ?>
                  </div>
                  <div class="spb-card-main-info">
                    <div class="spb-card-left-column spb-remove-margins">
                      <?php if ($status == 'sold') { ?>
                        <p class="car-status-banner"><?php echo __('Sold', 'sp-bilservice'); ?></p>
                      <?php } ?>
                      <h3 class="spb-card-title spb-text-medium"><?php echo get_the_title(); ?></h3>
                      <p class="spb-card-subtitle spb-text-base"><?php echo $variant; ?></p>
                      <div class="spb-card-details">
                        <?php if ($year) { ?>
                          <div class="year">
                            <p>
                            <p><img src="<?php echo plugin_dir_url(dirname(__FILE__)) .  '../images/year.svg'; ?>"><?php echo $year; ?></p>
                          </div>
                        <?php } ?>
                        <?php if ($mileage) { ?>
                          <div class="mileage">
                            <p><img src="<?php echo plugin_dir_url(dirname(__FILE__)) .  '../images/mileage.svg'; ?>"> <?php echo sp_bilservice_format_mileage($mileage); ?></p>
                          </div>
                        <?php } ?>
                        <?php if ($price) { ?>
                          <div class="price">
                            <p><?php echo sp_bilservice_format_price($price); ?></p>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </a>
              </article>
              <!-- #post-<?php the_ID(); ?> -->
            </div>

            <div class="mobile">
              <article>
                <a href="<?php echo get_the_permalink($car_id); ?>">
                  <div>
                    <?php if (!empty($images[0])) { ?>
                      <img class="mobileimagethumb" src="<?php echo $images[0]['sizes']['cararchive']; ?>" alt="<?php echo $images[0]['alt']; ?>">
                    <?php } else { ?>
                      <img class="mobileimagethumb" class="spb-card-car-placeholder" src="<?php echo plugin_dir_url(dirname(__FILE__)) .  'images/car-placeholder.svg'; ?>" alt="<?php echo __('Car Placeholder Image', 'sp-bilservice'); ?>">
                    <?php } ?>
                  </div>

                  <div class="spb-card-main-info">
                    <div class="spb-card-left-column spb-remove-margins">
                      <?php if ($status == 'sold') { ?>
                        <p class="car-status-banner"><?php echo __('Sold', 'sp-bilservice'); ?></p>
                      <?php } ?>
                      <h3 class="spb-card-title spb-text-medium"><?php echo get_the_title(); ?></h3>
                      <p class="spb-card-subtitle spb-text-base"><?php echo $variant; ?></p>
                      <div class="spb-card-details">
                        <?php if ($year) { ?>
                          <div class="year">
                            <p class="mobiledetailsmorecars"><img src="<?php echo plugin_dir_url(dirname(__FILE__)) .  '../images/year.svg'; ?>"><?php echo $year; ?></p>
                          </div>
                        <?php } ?>
                        <?php if ($mileage) { ?>
                          <div class="mileage">
                            <p class="mobiledetailsmorecars"><img src="<?php echo plugin_dir_url(dirname(__FILE__)) .  '../images/mileage.svg'; ?>"> <?php echo sp_bilservice_format_mileage($mileage); ?></p>
                          </div>
                        <?php } ?>
                        <?php if ($price) { ?>
                          <div class="price">
                            <p class="mobiledetailsmorecars"><?php echo sp_bilservice_format_price($price); ?></p>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </a>
              </article>
            </div>
          </div>
        <?php }
        ?>
      </div>

      <div class="showAllBruktBiler">
        <a href="<?= get_post_type_archive_link('car') ?>">
          <h3>Se alle bruktbiler <i class="arrow-right"></i></h3>
        </a>
      </div>

      <!-- <div class="linktoallusedcars">
        <h3><span class="arrow-right-dark">
            <a href="/bruktbiler">Se alle bruktbiler </a></span>
        </h3>
      </div> -->

    </div>
  </div>
<?php

} else {
  // no posts found
}

// Restore original Post Data
wp_reset_postdata();
