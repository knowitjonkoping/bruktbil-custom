<?php
/**
 * Anchor links
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/car/anchor-links
 */

 ?> 

<div class="anchor-links">
  <div class="anchor-inner">
    <div class="cardancor"><a href="#spesifikasjoner">Spesifikasjoner</a></div>
    <?php if(get_field('condition_report', $car_id)) { ?>
      <div class="cardancor" > <a href="#tilstandsrapport">Tilstandsrapport</a></div>
    <?php } ?>
    <div class="cardancor"><a href="#kontakt">Prøvekjør</a></div>
    <div class="cardancor"><a href="#fordeler">Fordeler hos Nardo Bil</a></div>
    <div class="cardancor"><a href="#lanekalkulator">Finansiering</a></div>
    <div class="cardancor"><a href="#selgere">Kontakt</a></div>
  </div>
</div>