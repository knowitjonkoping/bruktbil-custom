<?php
/**
 * Car Header Title
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/car/car-title
 */

$financing_module = get_field('financing_module', 'bilservice_options') ?: 'nordea';

// Car id
$car_id = get_the_ID();

// Car status
$status = get_field('status', $car_id) ?: 'for_sale';

// Fields
$variant = get_field('variant', $car_id);
$price = get_field('price', $car_id);
$monthly_price = sp_bilservice_get_monthly_price( $price, $cash, $payback_time );
$payment_share_percent = sp_bilservice_get_payment_share_percent( $price, $cash );
?>

<div class="spb-info-box spb-title-section">
  <?php if ($status == 'sold') { ?>
    <p class="car-status-banner"><?php echo __('Sold', 'sp-bilservice'); ?></p>
  <?php } ?>
  <h1><?php the_title(); ?></h1>
  <h2><?php echo $variant; ?></h2>
</div>

<div class="spb-info-box spb-price-section">
  <div class="spb-info-box-left-column spb-single-price">
    <p class="spb-margin-bottom"><?php echo __('Price', 'sp-bilservice'); ?></p>
    <p class="spb-single-price-value spb-text-large"><?php echo sp_bilservice_format_price( $price ); ?></p>
  </div>

  <?php if ($financing_module == 'nordea') { ?>
    <div class="spb-info-box-right-column spb-single-calculated-price">
      <p class="spb-text-small spb-color-dark-gray spb-margin-bottom"><?php echo __( 'Monthly Price', 'sp-bilservice' ); ?></p>
      <p class="spb-text-base spb-monthly-price"><?php echo $monthly_price; ?></p>
      <p class="spb-text-mini spb-cash-percent spb-color-dark-gray"><?php echo sprintf( __( 'Payment Share (<span class="spb-percent-number">%d</span> %%)', 'sp-bilservice' ), $payment_share_percent ); ?></p>
    </div>
  <?php } ?>
</div>
