<!-- Component: Price Calculator start -->
<div class="price-calculator g-24" id="lanekalkulator">
    <div class="gs18-l gs24-m gs24 go3-l narrowcalc">
        <h2>Lånekalkulator</h2>
        <div>
            <div class="money slidercontainer ">
                <h3>Kontantbeløp </h3>

                <div class="slider cash-down"></div>
                <div class="text-block">
                    <input type="text" name="cash" id="cash" value="<?= number_format(round(str_replace(' ', '', $price) * 0.3), 0, ',', ' ') ?> kr">
                    <input type="text" name="percent" id="percent" value="30%">

                </div>
            </div>
            <div class="time slidercontainer ">
                <h3>Nedbetalingstid</h3>
                <div class="slider payment-time"></div>
                <div class="text-block">
                    <input type="text" name="years" id="years" value="7 år">
                </div>
            </div>          

            <h3 class='total'><strong><?= str_replace(" kr","", $monthly_price);?></strong>,- pr måned i <span>7</span> år</h3>
            <a href='#' class='financing-search-button' target='_blank'>Søk nå</a>
            <p class="sub"><?= $subtext ? $subtext : 'Månedlig kostnad: Kr 1 590 per måned i 3 år. Deretter kr 1 735 per måned i 8 år. Bilpris kr 349 000, Kontantinnsats kr 209 950, Lånebeløp kr 139 050 inkludert etableringsgebyr/tinglysingsgebyr kr 2 990, 8 års nedbetaling, Nominell flytende rente 0,25 % f.t. første 3 år, deretter nominell flytende rente 3,99 % f.t., Effektiv rente 3,56 % f.t., Termingebyr kr 95 inkludert i månedsbeløp, Kostnad kr 22 290, Totalt kr 161 340.' ?></p>
        </div>
    </div>
</div>
<script>
    var car_price = '<?=$price?>'
    var branch = '<?= $branch_id ?>'
    var reg =  '<?= $car_reg; ?>'
    var low_interest = '<?= get_field('low_interest_rate', 'bilservice_options'); ?>'
    var mid_interest = '<?= get_field('medium_interest_rate', 'bilservice_options');?>'
    var high_interest = '<?= get_field('high_interest_rate', 'bilservice_options');?>'
</script>
<!-- Component: Price Calculator end -->
