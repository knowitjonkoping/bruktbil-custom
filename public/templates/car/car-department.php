<?php
/**
 * Car Department
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/car/car-info-grid
 */

// Variables
// Car id
$car_id = get_the_ID();
$department_name = get_field('department_name', $car_id);
$department_address = get_field('department_address', $car_id);
$department_zip_code = get_field('department_zip_code', $car_id);
$department_city = get_field('department_city', $car_id);
$address = $department_address . ', ' . $department_zip_code . ' ' . $department_city . ', Norway';

?>

<div class="spb-info-box spb-department" id="kontakt">
  <div class="kontakt-container"> 
    <p class="spb-center-icon-text">
      <h3>Prøvekjør bilen i <?=$department_city;?></h3>
      <p>Vi kan også komme hjem til deg for en uforpliktende prøvekjøring*</p>
      <p><a href="#" class="btn btn-blue btn-square" id="spb-schedule-physical-showing-section">Bestill prøvekjøring</a></p>
    </p>
  </div>

  <div>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
    <iframe style="width:536px;float:right;max-width:100%;height:283px;" frameborder="0" id="cusmap" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?q=<?php echo $address; ?>&output=embed"></iframe>
  </div>
</div>
