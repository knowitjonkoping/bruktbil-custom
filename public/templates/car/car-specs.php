<?php
/**
 * Car Specs
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/car/car-specs
 */

// Variables
// Car id
$car_id = get_the_ID();
$accessories = get_the_terms($car_id, 'accessories');

$body_type = get_field('car_body', $car_id);
$registration_date = get_field('registration_date', $car_id);
$engine_power = get_field('engine_power', $car_id);
$engine_volume = get_field('engine_volume', $car_id);
$towing_weight = get_field('towing_weight', $car_id);
$exterior_color = get_field('exterior_color', $car_id);
$interior_color = get_field('interior_color', $car_id);


?>

  <?php if ($accessories) { ?>
      <div class="specifications" id="spesifikasjoner">
         <div class="spb-wrap">
            <div class="specs"><h3>Spesifikasjoner og utstyr</h3></div>
               <div class="lists bruktbilspec"> 
               <ul class="g-12">
                  <!-- Row 1 -->

                  <li class="gs6 gs6-m gs4-l">
                     <span class="bruktbil-specTitle">Avgiftsklasse</span>
                     <span class="bruktbil-specItem"><?=$body_type?></span>
                  </li>

                  <li class="gs6 gs6-m gs4-l">
                     <span class="bruktbil-specTitle">Farge</span>
                     <span class="bruktbil-specItem"><?=$exterior_color?></span>
                  </li>

                  <li class="gs6 gs6-m gs4-l">
                     <span class="bruktbil-specTitle">Interiørfarge</span>
                     <span class="bruktbil-specItem"><?=$interior_color?></span>
                  </li>

                  <!-- Row 2 -->

                  <li class="gs6 gs6-m gs4-l">
                     <span class="bruktbil-specTitle">Sylindervolum</span>
                     <span class="bruktbil-specItem"><?=$engine_volume?> l</span>
                  </li>

                  <li class="gs6 gs6-m gs4-l">
                     <span class="bruktbil-specTitle">Tilhengervekt</span>
                     <span class="bruktbil-specItem"><?=$towing_weight?> kg</span>
                  </li>

                  <li class="gs6 gs6-m gs4-l">
                     <span class="bruktbil-specTitle">1. gang registert</span>
                     <span class="bruktbil-specItem"><?=$registration_date?></span>
                  </li>

               </ul>
               <br>
               <ul class="g-12 secondContentSpecs">
               <?php foreach ($accessories as $acc) { ?>
                  <li class="gs6 gs6-m gs4-l secondContentSpecsLi"><?php echo $acc->name; ?></li>
               <?php }?>
               </ul>
               <div><a id="visAltSpecs" class="btn btn-square btn-black-border btn-visAlt btn-visAltMargin">Vis alt</a>
            </div>
         </div>
      </div>
   </div>
<?php } ?>