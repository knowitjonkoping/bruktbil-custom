<?php
/**
 * Car Images
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/car/car-images
 */

// Car id
$car_id = get_the_ID();
$images = sp_bilservice_sort_images_by_carweb_sort_order( get_field('images') );


// Favorites
// $favorites = isset( $_COOKIE['spb_favorites'] ) ? json_decode( html_entity_decode( stripslashes ($_COOKIE['spb_favorites']))) : array();;

if ($images) { ?>
<!--
  <div class="spb-car-images">
    <div class="swiper nardoBruktbilSlider">
      <div class="swiper-wrapper">
      </div>
      <div class="nardoBruktbilSlider-swiper-pagination"></div>
      <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div>
    </div>

  </div>
      -->      
<?php } ?>

<?php

if ($images) { ?>

  <div class="spb-car-images swiper usercarSwiper" id="bruktbiler-gallery">
    <div class="swiper-wrapper" itemscope itemtype="http://schema.org/ImageGallery">
      <?php foreach ($images as $bilde) { ?>
        <div class="swiper-slide">
          <a
            itemprop="contentUrl"
            href="<?php echo $bilde['url']; ?>"
            data-pswp-width="1000"
            data-pswp-height="667"
            target="_blank"
          >
            <img
              src="<?php echo esc_url($bilde['sizes']['medium_large']); ?>"
              alt="<?php echo $bilde['alt']; ?>"
              itemprop="thumbnail" alt="<?php echo $bilde['alt']; ?>"
            />
        </a>
      </div>
      <?php } ?>
      </div>
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
  </div>

  <script src="//unpkg.com/tippy.js@3/dist/tippy.all.min.js"></script>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/photoswipe/5.2.2/photoswipe.min.css"/>
  <link rel="stylesheet" href="//unpkg.com/swiper@8/swiper-bundle.min.css" />
  <script type="module">

  import Swiper from '//unpkg.com/swiper@8/swiper-bundle.esm.browser.min.js'

  const global_swiper_photoswipe_loop_setting = false;

  var mySwiper = new Swiper(".usercarSwiper", {
    slidesPerView: 1,
    spaceBetween: 10,
    centeredSlides: true,
    grabCursor: true,
    // If we need pagination
    loop: global_swiper_photoswipe_loop_setting,
    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    // keyboard control
    keyboard: {
      enabled: true,
    }
  });

  import PhotoSwipeLightbox from 'https://unpkg.com/photoswipe@5.2.4/dist/photoswipe-lightbox.esm.js';
  import PhotoSwipe from 'https://unpkg.com/photoswipe@5.2.4/dist/photoswipe.esm.js';

  const photo_swipe_options = {
    gallery: '#bruktbiler-gallery',
    pswpModule: PhotoSwipe,
    children: 'a',
    loop: global_swiper_photoswipe_loop_setting,
    showHideAnimationType: 'zoom',
    zoom: false,
    close: true,
    counter: !global_swiper_photoswipe_loop_setting,
    arrowKeys: true,
    bgOpacity: 0.9,
    wheelToZoom: true,
  };

  const lightbox = new PhotoSwipeLightbox(photo_swipe_options);

  lightbox.init();

  lightbox.on('change', () => {
    const { pswp } = lightbox;
    mySwiper.slideTo(pswp.currIndex, 0, false);
  });

  lightbox.on('afterInit', () => {
    const { pswp } = lightbox;
    if(mySwiper.params.autoplay.enabled){
      mySwiper.autoplay.stop();
    };
    console.log('afterInit');
  });


  lightbox.on('closingAnimationStart', () => {
    const { pswp } = lightbox;
    mySwiper.slideTo(pswp.currIndex, 0, false);
    if(mySwiper.params.autoplay.enabled){
      mySwiper.autoplay.start();
    }
  });

  tippy('.swiper-button-prev', { 
    content: "Prev",
    theme: "light",
    arrow: true,
  })

  tippy('.swiper-button-next', { 
    content: "Next",
    theme: "light",
    arrow: true,
  })
</script>

<?php } ?>


