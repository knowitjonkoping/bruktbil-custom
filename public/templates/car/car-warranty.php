<?php
/**
 * Car Warranty
 *
 * @link       https://screenpartner.no
 * @since      1.0.7
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/car/car-warranty
 */

// Variables
// Car id
$car_id = get_the_ID();
$condition_report = get_field('condition_report', $car_id);
$warranty_string = get_field('warranty_string', $car_id);
$service_plan_followed = get_field('service_plan_followed', $car_id);
?>

<?php if ($warranty_string || $condition_report || $service_plan_followed) { ?>

  <div class="spb-info-box spb-warranty">

    <?php if ($warranty_string) { ?>
      <p class="spb-icon-line spb-center-icon-text">
        <span class="spb-icon"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . '../images/task-blue.svg'; ?>" alt="<?php echo __('Task Icon', 'sp-bilservice'); ?>"></span>
        <?php echo $warranty_string; ?>
      </p>
    <?php } ?>

    <?php if ($service_plan_followed) { ?>
      <p class="spb-icon-line spb-center-icon-text">
        <span class="spb-icon"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . '../images/history-blue.svg'; ?>" alt="<?php echo __('History Icon', 'sp-bilservice'); ?>"></span>
        <?php echo __('Service program followed', 'sp-bilservice'); ?>
      </p>
    <?php } ?>

    <?php if (($service_plan_followed || $warranty_string) && $condition_report) { ?>
      <div class="spb-break"></div>
    <?php } ?>

    <?php if ($condition_report) { ?>
      <a class="spb-icon-line spb-center-icon-text" href="<?php echo $condition_report['url']; ?>" target="_blank">
        <span class="spb-icon"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . '../images/assignment-blue.svg'; ?>" alt="<?php echo __('History Icon', 'sp-bilservice'); ?>"></span>
        <?php echo __('Approved Condition Report (Download PDF)', 'sp-bilservice'); ?>
      </a>
    <?php } ?>

  </div>


<?php } ?>
