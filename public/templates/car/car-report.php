<?php
/**
 * Car Report
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/car/car-report
 */

$condition_report = get_field('condition_report', $car_id);
$condition_report_text = get_field('tilstandsrapport', 'bilservice_options');
?>

<?php if ($condition_report) { ?>

<div class="spb-info-box the-car-report" id="tilstandsrapport">
   <h3><?php echo __('Tillstandsrapport', 'sp-bilservice'); ?></h3>
   <p><?= $condition_report_text ?></p>
   
   <div class="g-12 reportRow">
      
      <div class="title gs12 gs12-m gs3-l">
         <h3><?php echo __('Nedlastbar PDF', 'sp-bilservice'); ?></h3>
      </div>

      <div class="pdfURL gs12 gs12-m gs9-l">
         <a href="<?php echo $condition_report['url']; ?>" target="_blank" class="reportLink">Tillstandsrapport.pdf</a>
      </div>
   </div>
</div>
<?php } ?>