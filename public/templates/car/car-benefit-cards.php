

<?php 
// Benefit cards for Bruktbiler Single page
$bruktbiler_benefit_cards = get_field('organism_content', 'bilservice_options');
if($bruktbiler_benefit_cards):
	foreach($bruktbiler_benefit_cards as $organism_content):
		if($organism_content['section'] === 'bruktbil'): ?>
			<div class="benefit_card <?= $organism_content['colorscheme']; ?>">
				<div class="g-24 nardo-grid">
					<h2 id="fordeler" class="gs24"><?= $organism_content['section_title']; ?></h2>
					<div class="swiper nardoSwiper g-24 gs24-l go0-l gs23 go1 swiper-initialized swiper-horizontal swiper-pointer-events">
						<div class="swiper-wrapper card_container g-24 gs24 " style="transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
								<?php
								foreach ($organism_content['sections'] as $card) {
								?>
									<div class="card gs6 swiper-slide four-col swiper-slide-active">
										<img src="<?= get_field('icon', $card)['url'];?>" alt="<?= get_field('icon', $card)['alt'];?>">
										<span><?= get_field('text', $card);?></span>
									</div>
								<?php
								}
								?>
						</div>
					</div>
				</div>
			</div>
<?php
		endif;
	endforeach;
endif; ?>