<?php
/**
 * Car Popup
 *
 * @link       https://screenpartner.no
 * @since      1.0.0
 *
 * @package    SP_Bilservice
 * @subpackage SP_Bilservice/public/templates/car/car-popup
 */

// Car id
$carid = $GLOBALS['carid'];
$place = get_the_terms($GLOBALS['carid'], 'place')[0]->name;

$email = sanitize_title(get_the_terms($GLOBALS['carid'], 'place')[0]->name);
$email = get_post_by_name($email, 'avdeling');
$email = get_field('bruktbil_email',$email->ID);
?>

<div class="spb-popup" data-popup-type="video" data-popup-car-id="<?php echo $carid; ?>">
  <div class="spb-popup-container">
    <a href="#" class="spb-close-popup">x - lukk</a>
  </div>
</div>

<div class="spb-popup" data-popup-type="physical" data-popup-car-id="<?php echo $carid; ?>">
  <div class="spb-popup-container">
    <a href="#" class="spb-close-popup">x - lukk</a>
    <div class="spb-popup-scroll">
      <div class="g-12">
      <div class="gs12 gs0-m gs4-l"></div>
        <div class="gs12 gs12-m gs4-l">
          <h3>1. Velg hvor du ønsker å prøvekjøre bil</h3>
          <div class="location">
            <span><?= $place; ?></span>
            <span class="car-name"><?= get_the_title($carid); ?></span>
          </div>
          <!-- Conctact start -->
          <?php
            $testDriveFormID = get_field('choose_form_for_testdrive', 'bilservice_options');
            echo do_shortcode('[wpforms id="'. $testDriveFormID .'" title="false"]'); 
          ?>
          <!-- Contact end -->
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  <?php
  // $placeEmail = brb_get_post_by_name(sanitize_title($place), 'avdeling');
  ?>
  $('.location input').val('<?= get_the_terms( $carid , 'place')[0]->name; ?>');
  $('.contact_person input').val('<?= $email; ?>');
  $('.car_title input').val('<?= get_the_title($carid); ?>');
  $('.car_link input').val('<?= get_permalink($carid); ?>');
  $('.wpforms-submit-container').append( $( "<a href='javascript:' class='avbryt'>avbryt</a>" ) );
</script>
<style>
  .hidden {
    display: none !important;
  }
</style>

<?php
function brb_get_post_by_name(string $name, string $post_type = "post") {
  $query = new WP_Query([
      "post_type" => $post_type,
      "name" => $name
  ]);
  
  $query->have_posts() ? $return = reset($query->posts) : $return = null;
  wp_reset_postdata();
  return $return;
}