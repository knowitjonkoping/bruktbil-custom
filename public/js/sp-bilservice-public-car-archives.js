(function( $ ) {
	'use strict';

	function setCookie(name,value,days) {
	    var expires = "";
	    if (days) {
	        var date = new Date();
	        date.setTime(date.getTime() + (days*24*60*60*1000));
	        expires = "; expires=" + date.toUTCString();
	    }
	    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
	}
	function getCookie(name) {
	    var nameEQ = name + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0;i < ca.length;i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1,c.length);
	        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	    }
	    return null;
	}
	function eraseCookie(name) {
	    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	}

	window.addEventListener('popstate', (event) => {
		// Reload last location
		location.reload();

		// Alternative:
		// check_for_filter_changes( event.state );
		// Crazy amount of work for function above (not finished) ^
		// Recommend sticking with location.reload() using PHP get_query_var()
	});

	/*
	 * Toggle Filters
	 */
	$('.spb-toggle-filters').on('click', function(e) {
		e.preventDefault();
		var filters = $('.spb-filters');
		filters.toggleClass('active');
	});

	/*
	 * Toggle Filter Display
	 */
	$('.spb-toggle-box').on('click', function(e) {
		e.preventDefault();
		var box_content = $(this).parent().next('.spb-filterbox-content');
		var box_image = $(this).parent();
		box_content.toggleClass('filter-terms-hidden');
		box_image.toggleClass('open');
	});

	// A flag to know if a request is sent and the response is not yet received
	let ajax_scroll_request_sent = false;

	//Infinite scroll
		

	/*
	 * Load More
	 */
	$('#spb-loadmore').on('click', function(e) {
			if ( ! ajax_scroll_request_sent && $(window).scrollTop() >= $('.spb-cars').offset().top + $('.spb-cars').outerHeight() - window.innerHeight ) {
				// Set the flag to prevent any concurring request
			ajax_scroll_request_sent = true;
				console.log(car_archives_ajax_object.current_page)
				$.ajax({
					type: 'POST',
					url : car_archives_ajax_object.ajax_url, // AJAX handler
					data : {
					'action': 'sp_bilservice_load_more_cars', // the parameter for admin-ajax.php
					'query': car_archives_ajax_object.posts, // loop parameters passed by wp_localize_script()
					'page' : car_archives_ajax_object.current_page // current page
				},
				type : 'POST',
				beforeSend : function ( xhr ) {
					$('#spb-loadmore').text( car_archives_ajax_object.localized_strings['loading'] + '...' );
					$('#spb-loadmore').addClass("loading");
				},
				success : function( cars ) {
					if ( cars ) {
						$('#spb-loadmore').text( car_archives_ajax_object.localized_strings['load_more'] );
						$('#spb-loadmore').removeClass("loading");
						$('.spb-cars').append( cars ); // insert new posts
						car_archives_ajax_object.current_page++;

						if ( car_archives_ajax_object.current_page == car_archives_ajax_object.max_page ) {
							$('#spb-loadmore').hide(); // if last page, HIDE the loading span
						}
					} else {
						$('#spb-loadmore').hide(); // if no data, HIDE the loading span
					}

					// Unset the flag
          ajax_scroll_request_sent = false;
				}
			});
	  }
	});

	/*
	 * Filter
	 */
	$('#spb-filters :input:visible').on('input', function(e) {
		sp_bilservice_filter_cars();

		// do not submit the form
		return false;
	});

	$('.spb-sortby-select').on('change', function(e) {
		var option_selected = $("option:selected", this);
		var option_value = this.value;

		$('#sortby').val(option_value);
		sp_bilservice_filter_cars();
	});

	$('#spb-toggle-favorites button').on('click', function(e) {
		sp_bilservice_favorites();
		spb_reset_filters();

		// do not submit the form
		return false;
	});

	$('.spb-form-check:not(.spb-second-level) input[type="checkbox"]').change(function() {
		var count = $(this).parent('.spb-form-check').attr('data-brand-count');
		var children = $(this).closest('.spb-filterbox-content').find('.spb-second-level[data-brand-count=' + count + ']');

		if (this.checked) {
			children.addClass('active');
		} else {
			children.removeClass('active');

			var children_input = children.find('input');
			children_input.removeAttr('checked');
			console.log('removing attributes from children');
		}

	});

	function sp_bilservice_filter_cars() {
		$.ajax({
			url : car_archives_ajax_object.ajax_url, // AJAX handler
			data : $('#spb-filters').serialize(), // form data
			dataType : 'JSON', // this data type allows us to receive objects from the server
			type : 'POST',
			beforeSend : function() {
				$('.spb-cars').addClass('loading');
				$('.spb-filterbox').addClass('loading');
			},
			success : function( data ) {

				// when filter applied:
				// set the current page to 1
				car_archives_ajax_object.current_page = 1;

				// set the new query parameters
				car_archives_ajax_object.posts = data.posts;

				// set the new max page parameter
				car_archives_ajax_object.max_page = data.max_page;

        // First pull out the empty strings
				var state = {};
				var brand_states = [];
				var model_states = [];
				var fuel_states = [];
				var transmission_states = [];
				var place_states = [];
				var wheeldrive_states = [];
				var min_price_state = '';
				var max_price_state = '';
				var min_year_state = '';
				var max_year_state = '';
				var min_mileage_state = '';
				var max_mileage_state = '';
				var cash_amount = '';
				var payback_time = '';
        var formData = $('#spb-filters').serializeArray().filter(function (i) {
          if ( i.value != 'spbcarsfilter_ajax_callback' ) {

						console.log(i.name + ': ' + i.value);

						// If data is brand
						if ( i.name.startsWith( 'brand_' ) ) {

							brand_states.push(i.value);
							state['brand'] = brand_states;

						} else if ( i.name.startsWith( 'fuel_' ) ) {

							fuel_states.push(i.value);
							state['fuel'] = fuel_states;

						} else if ( i.name.startsWith( 'model_' ) ) {

							model_states.push(i.value);
							state['model'] = model_states;

						} else if ( i.name.startsWith( 'transmission_' ) ) {

							transmission_states.push(i.value);
							state['transmission'] = transmission_states;

						} else if ( i.name.startsWith( 'place_' ) ) {

							place_states.push(i.value);
							state['place'] = place_states;

						} else if ( i.name.startsWith( 'wheeldrive_' ) ) {

							wheeldrive_states.push(i.value);
							state['wheeldrive'] = wheeldrive_states;

						} else if ( i.name == 'min_price' ) {

							min_price_state = i.value.replace(/\s/g, '');
							state['min_price'] = min_price_state;

						} else if ( i.name == 'max_price' ) {

							max_price_state = i.value.replace(/\s/g, '');
							state['max_price'] = max_price_state;

						} else if ( i.name == 'min_year' ) {

							min_year_state = i.value;
							state['min_year'] = min_year_state;

						} else if ( i.name == 'max_year' ) {

							max_year_state = i.value;
							state['max_year'] = max_year_state;

						} else if ( i.name == 'min_mileage' ) {

							min_mileage_state = i.value;
							state['min_mileage'] = min_mileage_state;

						} else if ( i.name == 'max_mileage' ) {

							max_mileage_state = i.value;
							state['max_mileage'] = max_mileage_state;

						} else if ( i.name == 'show_favorites' ) {

							max_mileage_state = i.value;
							state['max_mileage'] = max_mileage_state;

						} else {

							state[i.name] = i.value;
	            return i.value;

						}
          }
        });

				// Add brands to formdata
				if (brand_states.length > 0) {
					formData.push({name: 'brand', value: brand_states});
				}

				// Add fuels to formdata
				if (fuel_states.length > 0) {
					formData.push({name: 'fuel', value: fuel_states});
				}

				// Add models to formdata
				if (model_states.length > 0) {
					formData.push({name: 'model', value: model_states});
				}

				// Add transmissions to formdata
				if (transmission_states.length > 0) {
					formData.push({name: 'transmission', value: transmission_states});
				}

				// Add place to formdata
				if (place_states.length > 0) {
					formData.push({name: 'place', value: place_states});
				}

				// Add wheeldrive to formdata
				if (wheeldrive_states.length > 0) {
					formData.push({name: 'wheeldrive', value: wheeldrive_states});
				}

				// Add min price to formdata
				if (min_price_state.length > 0) {
					formData.push({name: 'min_price', value: min_price_state});
				}

				// Add max price to formdata
				if (max_price_state.length > 0) {
					formData.push({name: 'max_price', value: max_price_state});
				}

				// Add min year to formdata
				if (min_year_state.length > 0) {
					formData.push({name: 'min_year', value: min_year_state});
				}

				// Add max year to formdata
				if (max_year_state.length > 0) {
					formData.push({name: 'max_year', value: max_year_state});
				}

				// Add min mileage to formdata
				if (min_mileage_state.length > 0) {
					formData.push({name: 'min_mileage', value: min_mileage_state});
				}

				// Add max mileage to formdata
				if (max_mileage_state.length > 0) {
					formData.push({name: 'max_mileage', value: max_mileage_state});
				}

				if ($.param(formData).length == 0) {
					//Now push formData to URL
	        window.history.replaceState({}, '', location.pathname);
				} else {
					//Now push formData to URL
	        window.history.pushState(state, '', '?' + decodeURIComponent($.param(formData)));
				}

				// insert the posts to the container
				$('.spb-cars').removeClass('loading');
				$('.spb-filterbox').removeClass('loading');
				$('.spb-cars').html(data.content);

				// hide load more button, if there are not enough posts for the second page
				if ( data.max_page < 2 ) {
					$('#spb-loadmore').hide();
				} else {
					$('#spb-loadmore').show();
				}

				// Update total cars
				$('.spb-number-of-cars').html(data.found_posts);
			},
			error: function(MLHttpRequest, textStatus, errorThrown) {
				console.log(MLHttpRequest);
				console.log(textStatus);
				console.log(errorThrown);
			},
		});
	}

	function sp_bilservice_favorites() {
		$.ajax({
			url : car_archives_ajax_object.ajax_url, // AJAX handler
			data : $('#spb-toggle-favorites').serialize(), // form data
			dataType : 'JSON', // this data type allows us to receive objects from the server
			type : 'POST',
			beforeSend : function() {
				$('.spb-cars').addClass('loading');
				$('.spb-filterbox').addClass('loading');
			},
			success : function( data ) {

				// when filter applied:
				// set the current page to 1
				car_archives_ajax_object.current_page = 1;

				// set the new query parameters
				car_archives_ajax_object.posts = data.posts;

				// set the new max page parameter
				car_archives_ajax_object.max_page = data.max_page;

				console.log(data.tax_query);

        // First pull out the empty strings
				var state = {};
				var favorites = JSON.parse(getCookie('spb_favorites'));
				var display_favorites = false;
        var formData = $('#spb-toggle-favorites').serializeArray().filter(function (i) {
          if ( i.value != 'spbcarsfavorites_ajax_callback' ) {

						console.log(i.name + ': ' + i.value);

						if ( i.name == 'show_favorites' ) {

							if (JSON.parse(i.value) == false) {
								display_favorites = true;
								$('#show_favorites').val(true);
							} else {
								display_favorites = false;
								$('#show_favorites').val(false);
							}

						}
          }
        });

				if (display_favorites) {
					formData.push({name: 'favorites', value: JSON.stringify(display_favorites)});
				}

				if ($.param(formData).length == 0) {
					//Now push formData to URL
	        window.history.replaceState({}, '', location.pathname);
				} else {
					//Now push formData to URL
	        window.history.pushState(state, '', '?' + decodeURIComponent($.param(formData)));
				}

				// insert the posts to the container
				$('.spb-cars').removeClass('loading');
				$('.spb-filterbox').removeClass('loading');
				$('.spb-cars').html(data.content);

				var data_favorites_btn = $('#spb-toggle-favorites button');
				var data_favorites_active = data_favorites_btn.attr('data-favorites-active');

				if (data_favorites_active == 'true') {
					data_favorites_btn.attr('data-favorites-active', 'false');
				} else {
					data_favorites_btn.attr('data-favorites-active', 'true');
				}

				// hide load more button, if there are not enough posts for the second page
				if ( data.max_page < 2 ) {
					$('#spb-loadmore').hide();
				} else {
					$('#spb-loadmore').show();
				}
			},
			error: function(MLHttpRequest, textStatus, errorThrown) {
				console.log(MLHttpRequest);
				console.log(textStatus);
				console.log(errorThrown);
			},
		});
	}

	function spb_reset_applied_filters() {
		// Do the ajax action
    $.ajax({
      type: 'POST',
			url: car_archives_ajax_object.ajax_url,
      data: {
        "action" : "sp_bilservice_reset_cars_ajax_callback",
      },
			beforeSend : function() {
				$('.spb-cars').addClass('loading');
				$('.spb-filterbox').addClass('loading');
			},
      success: function(data) {
				// First pull out the empty strings
				var state = {};
        window.history.replaceState({}, '', location.pathname);
				location.reload();
      },
			error: function(MLHttpRequest, textStatus, errorThrown) {
				console.log(MLHttpRequest);
				console.log(textStatus);
				console.log(errorThrown);
			}
    });
	}

	function spb_reset_filters() {
		var search = $('input[type="search"]');
		var checkboxes = $('.spb-filterbox input[type="checkbox"]:checked');
		var sortby = $('.spb-sortby-select');

		// Sortby
		sortby.prop('selectedIndex', 0);

		// Search
		search.val('');

		// Checkboxes
		checkboxes.removeAttr('checked');

		// Sliders
		var min_price_val = price_slider.slider("option", "min");
		var max_price_val = price_slider.slider("option", "max");
		var min_year_val = year_slider.slider("option", "min");
		var max_year_val = year_slider.slider("option", "max");
		var min_mileage_val = mileage_slider.slider("option", "min");
		var max_mileage_val = mileage_slider.slider("option", "max");

		price_slider.slider("values", 0, min_price_val);
		price_slider.slider("values", 1, max_price_val);
		$( "#min_price_formatted" ).html(min_price_val.toLocaleString());
		$( "#max_price_formatted" ).html(max_price_val.toLocaleString());

		year_slider.slider("values", 0, min_year_val);
		year_slider.slider("values", 1, max_year_val);
		$( "#min_year_formatted" ).html(min_year_val);
		$( "#max_year_formatted" ).html(max_year_val);

		mileage_slider.slider("values", 0, min_mileage_val);
		mileage_slider.slider("values", 1, max_mileage_val);
		$( "#min_mileage_formatted" ).html(min_mileage_val.toLocaleString());
		$( "#max_mileage_formatted" ).html(max_mileage_val.toLocaleString());


	}

	function check_for_filter_changes( states ) {
		for ( var state in states ) {
			if ( states.hasOwnProperty( state ) ) {
				var name = document.getElementById(state);
				var value = states[state];

				// SEARCH FILTER

				// BRAND FILTER
				if ( state == 'brand' ) {
					var checked_brand = $(name).find('input:radio:checked').val();
					console.log(checked_brand);
					console.log(value);

					if ( checked_brand != value ) {
						// Trigger old checked brand
						var new_radio = $(name).find('input:radio[value=' + value + ']');
						new_radio.click();
					}
				}

				// PRICE FILTER

				// YEAR FILTER

				// MILEAGE FILTER

				// FUEL FILTER

				// TRANSMISSION FILTER

				// WHEEL DRIVE FILTER

				// PLACE FILTER
	    }
		}
	}


	var price_slider = $( "#spb-price-slider" ).slider({
    range: true,
    min: 0,
		step: 1000,
    max: parseInt(car_archives_ajax_object.highest_price),
    values: [ 0, parseInt(car_archives_ajax_object.highest_price) ],
    slide: function( event, ui ) {
      //$( "#min_price" ).val(ui.values[ 0 ].toLocaleString());
      $( "#min_price" ).val(ui.values[ 0 ]);
      $( "#min_price_formatted" ).html(ui.values[ 0 ].toLocaleString() + ' kr');
      $( "#max_price" ).val(ui.values[ 1 ]);
      $( "#max_price_formatted" ).html(ui.values[ 1 ].toLocaleString() + ' kr');
    },
		stop: function( event, ui ) {
			var values = ui.values;
			sp_bilservice_filter_cars();
		},
		create: function( event, ui ) {
			$("#spb-price-slider").slider("values", 0, $('#min_price').val());
			$("#spb-price-slider").slider("values", 1, $('#max_price').val());
		}
  });

	var mileage_slider = $( "#spb-mileage-slider" ).slider({
    range: true,
    min: 0,
		step: 1000,
    max: parseInt(car_archives_ajax_object.highest_mileage),
    values: [ 0, parseInt(car_archives_ajax_object.highest_mileage) ],
    slide: function( event, ui ) {
      $( "#min_mileage" ).val(ui.values[ 0 ]);
      $( "#min_mileage_formatted" ).html(ui.values[ 0 ].toLocaleString());
      $( "#max_mileage" ).val(ui.values[ 1 ]);
      $( "#max_mileage_formatted" ).html(ui.values[ 1 ].toLocaleString());
			console.log(ui);
    },
		stop: function( event, ui ) {
			var values = ui.values;
			sp_bilservice_filter_cars();
		},
		create: function( event, ui ) {
			$("#spb-mileage-slider").slider("values", 0, $('#min_mileage').val());
			$("#spb-mileage-slider").slider("values", 1, $('#max_mileage').val());
		}
  });

	var year_slider = $( "#spb-year-slider" ).slider({
    range: true,
    min: parseInt(car_archives_ajax_object.lowest_year),
    max: parseInt(car_archives_ajax_object.current_year),
    values: [ parseInt(car_archives_ajax_object.lowest_year), parseInt(car_archives_ajax_object.current_year) ],
    slide: function( event, ui ) {
			$( "#min_year" ).val(ui.values[ 0 ]);
      $( "#min_year_formatted" ).html(ui.values[ 0 ]);
      $( "#max_year" ).val(ui.values[ 1 ]);
      $( "#max_year_formatted" ).html(ui.values[ 1 ]);
			var values = ui.values;
			console.log(values);
    },
		stop: function( event, ui ) {
			var values = ui.values;
			console.log(values);
			sp_bilservice_filter_cars();
		},
		create: function( event, ui ) {
			var values = ui.values;
			$("#spb-year-slider").slider("values", 0, $('#min_year').val());
			$("#spb-year-slider").slider("values", 1, $('#max_year').val());
		}
  });

	$( "#spb-cash-amount-slider" ).slider({
    range: false,
    min: 0,
    max: 150000,
		step: 1000,
    value: car_archives_ajax_object.cash_amount,
    slide: function( event, ui ) {
      $( "#cash_amount" ).val(ui.value);
      $( "#cash_amount_formatted" ).html(ui.value.toLocaleString() + ' kr');
    },
		stop: function( event, ui ) {
			var value = ui.value;
			setCookie('spb_cash_amount', value, 7);
			sp_bilservice_filter_cars();
		},
		create: function( event, ui ) {
			$("#spb-cash-amount-slider").slider("value", car_archives_ajax_object.cash_amount);
		}
  });

	$( "#spb-payback-time-slider" ).slider({
    range: false,
    min: 0,
    max: 10,
    value: car_archives_ajax_object.payback_time,
    slide: function( event, ui ) {
      $( "#payback_time" ).val(ui.value);
      $( "#payback_time_formatted" ).html(ui.value + ' ' + car_archives_ajax_object.localized_strings['years']);
    },
		stop: function( event, ui ) {
			var value = ui.value;
			console.log(value);
			setCookie('spb_payback_time', value, 7);
			sp_bilservice_filter_cars();
		},
		create: function( event, ui ) {
			$("#spb-payback-time-slider").slider("value", car_archives_ajax_object.payback_time);
		}
  });

	$('#spb-reset-filters').on('click', function(e) {
		e.preventDefault();
		spb_reset_applied_filters();
	});

	// Hide search icon while typing
	$(document).ready(function() {
		$('.searchInput').keyup(function() {
		  $('.searchInput').val($(this).val());
		  if($(this).length && $(this).val().length){
			 $('.spb-search-filter').addClass('searchInputEmpty')
		  } else {
			  $('.spb-search-filter').removeClass('searchInputEmpty')
		  }
		});
	 });

})( jQuery );
