function setCookie(name,value,days) {
	var expires = "";
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days*24*60*60*1000));
		expires = "; expires=" + date.toUTCString();
	}

	document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}

	return null;
}

function eraseCookie(name) {
	document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

jQuery(document).ready(function($) {

	document.addEventListener('click', function (event) {

		var element = $(event.target);

		// If the clicked element doesn't have the right selector, bail
		if (!event.target.matches('.spb-favorite-car')) return;

		// Don't follow the link
		event.preventDefault();
		var carid = element.attr('data-car-id');
		var favorites = JSON.parse(getCookie('spb_favorites'));
		var favoritescount = $('#spb-toggle-favorites .spb-count');

		if (favorites == null) {
			favorites = [];
		}

		if (favorites.includes(carid)) {
			// remove favorite
			var index = favorites.indexOf(carid);
			favorites.splice(index, 1);
			favoritescount.text(parseInt(favoritescount.text()) - 1);
		} else {
			favorites.push(carid);
			favoritescount.text(parseInt(favoritescount.text()) + 1);
		}

		setCookie('spb_favorites', JSON.stringify(favorites), 7);
		element.toggleClass('active');

	}, false);

});
