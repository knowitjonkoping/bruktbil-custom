(function( $ ) {
	'use strict';

	function spb_update_monthly_price() {
		var monthly_price = $('.spb-monthly-price');
		var cash_percent = $('.spb-cash-percent span.spb-percent-number');
		var aaraabetale = $('.antaar');

		$.ajax({
			type: 'POST',
			url : car_single_ajax_object.ajax_url, // AJAX handler
			data : {
				'action': 'sp_bilservice_get_monthly_price', // the parameter for admin-ajax.php
				'car_id': car_single_ajax_object.car_id,
				'price': car_single_ajax_object.price,
				'payback_time': payback_time_slider.slider("value"),
				'cash_amount': cash_amount_slider.slider("value"),
				'percent': car_single_ajax_object.percent,
				'branch_id': car_single_ajax_object.branch_id
			},
			success : function( data ) {
				if ( data ) {
					monthly_price.html(JSON.parse(data)['monthly_price'] + ' per mnd');
					aaraabetale.html(JSON.parse(data)['payback_time'] + ' år');
					cash_percent.html(JSON.parse(data)['percent']);
					$('#spb-loan-url').prop('href', JSON.parse(data)['loan_url'])
				}
			}
		});
	}

	var cash_amount_slider = $( "#spb-cash-amount-slider-single" ).slider({
    range: false,
    min: 0,
    max: car_single_ajax_object.price,
		step: 1000,
    value: car_single_ajax_object.cash_amount,
    slide: function( event, ui ) {
      $( "#cash_amount" ).val(ui.value);
      $( "#cash_amount_formatted" ).html(ui.value.toLocaleString() + ' kr');
    },
		stop: function( event, ui ) {
			var value = ui.value;
			setCookie('spb_cash_amount', value, 7);
			spb_update_monthly_price();
		},
		create: function( event, ui ) {
			$("#spb-cash-amount-slider").slider("value", car_single_ajax_object.cash_amount);
		}
  });

	var payback_time_slider = $( "#spb-payback-time-slider-single" ).slider({
    range: false,
    min: 0,
    max: 10,
    value: car_single_ajax_object.payback_time,
    slide: function( event, ui ) {
      $( "#payback_time" ).val(ui.value);
      $( "#payback_time_formatted" ).html(ui.value + ' ' + car_single_ajax_object.localized_strings['years']);
    },
		stop: function( event, ui ) {
			var value = ui.value;
			console.log(value);
			setCookie('spb_payback_time', value, 7);
			spb_update_monthly_price();
		},
		create: function( event, ui ) {
			$("#spb-payback-time-slider").slider("value", car_single_ajax_object.payback_time);
		}
  });

   //  var $car_main = $('.carousel-main').flickity({
   //    // options
   //    cellAlign: 'left',
   //    contain: true,
   //    pageDots: false,
   //    prevNextButtons: true,
   //    imagesLoaded: true,
   //    adaptiveHeight: true,
	// 		fullscreen: true
   //  });

   //  $('.carousel-nav').flickity({
   //    // options
   //    asNavFor: ".carousel-main",
   //    pageDots: false,
   //    contain: true,
   //    prevNextButtons: false,
   //    imagesLoaded: true
   //  });

	$('#spb-popup-video, #spb-popup-physical').on('submit', function(e) {
		e.preventDefault();
		$.ajax({
			url : car_single_ajax_object.ajax_url, // AJAX handler
			data : $(this).serialize(), // form data
			dataType : 'JSON', // this data type allows us to receive objects from the server
			type : 'POST',
			success : function( data ) {
				console.log(data);
				if (data.mail_status == true) {
					$('.spb-popup[data-popup-type="' + data.showing_type + '"] form').hide();
					sp_form_message(data.showing_type, '<p>' + car_single_ajax_object.localized_strings['form_success'] + '</p>', 'success');
				} else {
					sp_form_message(data.showing_type, '<p>' + car_single_ajax_object.localized_strings['form_error'] + '</p>', 'error');
				}
			},
			error: function(MLHttpRequest, textStatus, errorThrown) {
				console.log(MLHttpRequest);
				console.log(textStatus);
				console.log(errorThrown);
			},
		});
	})

	$('#spb-schedule-video-showing').on('click', function(e) {
		e.preventDefault();
		var popup = $('.spb-popup[data-popup-type="video"]');
		popup.addClass('active');
		$('body').css('overflow', 'hidden');
	});

	$('#spb-schedule-physical-showing').on('click', function(e) {
		e.preventDefault();
		var popup = $('.spb-popup[data-popup-type="physical"]');
		popup.addClass('active');
		$('body').css('overflow', 'hidden');
	});

	$('#spb-schedule-physical-showing-section').on('click', function(e) {
		e.preventDefault();
		var popup = $('.spb-popup[data-popup-type="physical"]');
		popup.addClass('active');
		$('body').css('overflow', 'hidden');
	});

	$('.spb-close-popup, .avbryt').on('click', function(e) {
		e.preventDefault();
		$('.spb-popup').removeClass('active');
		$('body').css('overflow', 'auto');
	});

	$('.spb-popup').on('click', function(e) {
		var target = e.target;
		if ($(target).hasClass('spb-popup')) {
			$('.spb-popup').removeClass('active');
			$('body').css('overflow', 'auto');
		}
	});

	function sp_form_message(popup_type, $msg, $type) {
		var location = $('.spb-popup[data-popup-type="' + popup_type + '"] .spb-popup-scroll');
		var html = "<div class='spb-notice spb-notice-" + $type + "'>" + $msg + "</div>";
		location.append(html);
	}

})( jQuery );


function sendContact() {
	var valid;	
	valid = validateContact();
	if(valid) {
		jQuery.ajax({
		url: window.location + "../../app/mu-plugins/bilservice/public/templates/car/car-testdrive-mailer.php",
		data:'userName='+$("#userName").val()
		+'&userEmail='+$("#userEmail").val()
		+'&userPhone='+$("#userPhone").val()
		+'&contactEmail='+$("#contactEmail").val()
		+'&contactLocation='+$("#contactLocation").val()
		+'&date='+$("#date").val(),
		type: "POST",
		success:function(data){
		$("#mail-status").html(data);
		},
		error:function (){}
		});
	}
}

$('#date').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'mm/dd/yy',
	beforeShow: function (input, inst) {
		 var rect = input.getBoundingClientRect();
		 setTimeout(function () {
			 inst.dpDiv.css({ top: rect.top + 40, left: rect.left + 0 });
		 }, 0);
	}
});

$('#dateText').click(function() {
	$('#date').datepicker('show');
});

$(document).ready(function(){
   $('#time').timepicker({
      timeFormat: 'HH:mm',
      interval: 60,
      minTime: '10:00',
      maxTime: '18:00',
      defaultTime: '10:00',
      startTime: '10:00',
      dynamic: false,
      dropdown: true,
      scrollbar: true
   });

   $('#timeText').click(function() {
      $('#time').timepicker('show');
   });
});